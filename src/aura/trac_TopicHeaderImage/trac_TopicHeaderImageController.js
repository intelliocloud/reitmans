({
	setImageDisplay : function(component, event, helper) {
		var theTopic = component.get("v.trac_TopicID");
        var helpID = component.get("v.trac_HelpTopicId");
          
        if (theTopic == helpID) {
            component.set("v.trac_imgDisplay", 'block');
        } 
	}
})