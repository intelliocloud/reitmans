({
	doClickTopLevel : function(component, event, helper) {
		helper.doClickTopLevel(component, event);
	},

	doClickUpOneLevel : function(component, event, helper) {
		helper.doClickUpOneLevel(component, event);
	}
})