({
	doClickTopLevel : function(component, event) {
		var topLevelKey = $A.get("$Label.c.Campus_Category_Toplevel_Id");

		var navKey = topLevelKey;
		component.set("v.currentCategory", null);
		component.set("v.navKey", navKey);
	},

	doClickUpOneLevel : function(component, event, helper) {
		var topLevelKey = $A.get("$Label.c.Campus_Category_Toplevel_Id");
		
		var categoryCrumbs = component.get("v.categoryCrumbs");
		var currentCategory = component.get("v.currentCategory");

		if(currentCategory != null && currentCategory != undefined) {
			var newCurrentCategory = null;
			var newNavKey = topLevelKey;
			if(categoryCrumbs.length > 1) {
				newCurrentCategory = categoryCrumbs[categoryCrumbs.length-2];
				newNavKey = newCurrentCategory.id;
			}

			component.set("v.currentCategory", newCurrentCategory);
			component.set("v.navKey", newNavKey);
		}
	}
})