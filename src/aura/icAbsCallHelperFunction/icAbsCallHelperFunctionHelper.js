({
	callSubHelperFunction : function(component, name, params) {
		var concreteComponent = component.getConcreteComponent();
		concreteComponent.callHelperFunction(name, params);
	}
})