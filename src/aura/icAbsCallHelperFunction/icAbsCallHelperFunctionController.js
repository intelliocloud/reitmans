({
	callHelperFunction : function(component,event,helper) {
		var args = event.getParam('arguments');
		var params = args.params ? [component,event,args.params] : [component,event];
		var f = helper[args.name];
		if(f) f.apply(helper,params);
	}
})