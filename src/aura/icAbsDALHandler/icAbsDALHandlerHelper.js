({
	callServerAction : function(component,event,actionName,serverParams,clientParams) {
		//Note : for now, the function is hard coded to only use the first dal component.
		var dal = component.get("v.dal")[0]; 
		dal.callServerAction(actionName,serverParams,clientParams);
	}
})