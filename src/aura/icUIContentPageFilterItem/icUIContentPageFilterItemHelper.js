({
	doClickFilter : function(component, event) {
		var filter = component.get("v.filter");
		component.set("v.currentFilter", filter);
	}
})