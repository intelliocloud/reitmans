({
    checkLicense : function(cmp){
	
    var action = cmp.get("c.hasSalesforceAccess");
    action.setCallback(this, function(response){
        var state = response.getState();
        if (state === "SUCCESS") {
            cmp.set("v.UType", response.getReturnValue());
        
            if (response.getReturnValue()){
    			cmp.set("v.SFL", true);
            }else{
                cmp.set("v.SFL", false);
            } 
        }
      });
       $A.enqueueAction(action);
	},
	backToSF : function(component, event, helper) {
			window.location.href = '/servlet/networks/switch?networkId=000000000000000';
	},
    
    userLang : function(component, event, helper){
    
        var locale = $A.get("$Locale.language");
        if (locale == "en"){
            component.set("v.DisplayText", component.get("v.ButtonTextDisplay") );
        }else{
            component.set("v.DisplayText", component.get("v.ButtonTextDisplayFR") );
        }
    
    }
    
    
})