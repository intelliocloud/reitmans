({
getContents : function(component, event, helper) { 
   var recordId = component.get("v.recordId");
   var action = component.get("c.getUrgentMessage");
   action.setParams({
       "urgentMessageId": recordId 
   });
   action.setCallback(this, function(data) {
    component.set("v.Urgent_Message__c", data.getReturnValue());
  });
   $A.enqueueAction(action);                
  }
})