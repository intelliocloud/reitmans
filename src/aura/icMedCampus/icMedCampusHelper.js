({
	doInit : function(component, event) {
		var topLevelKey = $A.get("$Label.c.Campus_Category_Toplevel_Id");

		var helper = this;
		this.callServerAction(
			component, 
			event, 
			"getNavigationCategories", 
			{},
			{
				onSuccess: function(response) {
					var responseValue = response.getReturnValue();
					var navKey = topLevelKey;
					component.set("v.mapNavCategories", responseValue);
					component.set("v.navCategories", responseValue[navKey]);
					component.set("v.navKey", navKey);
				},
				triggerEvent: "dataProvided",
				storable: true,
				abortable: true
			}
		);
	},

	doNavKeyChange : function(component, event) {
		var topLevelKey = $A.get("$Label.c.Campus_Category_Toplevel_Id");

		var navKey = component.get("v.navKey");
		if(navKey != '' && navKey != undefined && navKey != null) {
			var mapNavCategories = component.get("v.mapNavCategories");
			var currentCategory = component.get("v.currentCategory");
			var currentCategoryHasContent = false;
			var currentCategoryHasWelcome = false;

			component.set("v.mapCategoryContentKeys", []);
			component.set("v.currentContent", null);
			
			if(navKey == topLevelKey) {
				component.set("v.topCategory", null);
				component.set("v.categoryCrumbs", []);
				currentCategoryHasWelcome = true;
			} else {
				if(currentCategory.parent == topLevelKey) {
					component.set("v.topCategory", currentCategory);
					currentCategoryHasWelcome = true;
				}
			}

			var helper = this;
			if(currentCategoryHasWelcome) {
				component.set("v.mapWelcomeContents", null);
				component.set("v.mapWelcomeContentKeys", []);
				this.callServerAction(
					component, 
					event, 
					"getWelcomeContent", 
					{"categoryId":navKey},
					{
						onSuccess: function(response) {
							var responseValue = response.getReturnValue();

							if(currentCategory != null) {
								var categoryCrumbs = component.get("v.categoryCrumbs");
								if(categoryCrumbs.indexOf(currentCategory) < 0) {
									categoryCrumbs.push(currentCategory);
								} else {
									while(categoryCrumbs[categoryCrumbs.length-1].id != currentCategory.id) {
										categoryCrumbs.pop();
									}
								}
								component.set("v.categoryCrumbs", categoryCrumbs);
							}
							component.set("v.currentCategoryHasContent", currentCategoryHasContent);
							component.set("v.mapWelcomeContents", responseValue);
							component.set("v.mapWelcomeContentKeys", Object.keys(responseValue));
							component.set("v.navCategories", mapNavCategories[navKey]);
						},
						triggerEvent: "dataProvided",
						storable: true,
						abortable: true
					}
				);
			} else {
				this.callServerAction(
					component, 
					event, 
					"getCategoryContent", 
					{"categoryId":navKey},
					{
						onSuccess: function(response) {
							var responseValue = response.getReturnValue();
							
							if(Object.keys(responseValue).length > 0) {
								currentCategoryHasContent = true;							
							} else {
								var categoryCrumbs = component.get("v.categoryCrumbs");
								if(categoryCrumbs.indexOf(currentCategory) < 0) {
									categoryCrumbs.push(currentCategory);
								} else {
									while(categoryCrumbs[categoryCrumbs.length-1].id != currentCategory.id) {
										categoryCrumbs.pop();
									}
								}
								component.set("v.categoryCrumbs", categoryCrumbs);
								component.set("v.navCategories", mapNavCategories[navKey]);
							}
							component.set("v.currentCategoryHasContent", currentCategoryHasContent);
							component.set("v.mapCategoryContents", responseValue);
							component.set("v.mapCategoryContentKeys", helper.customOrderKeys(Object.keys(responseValue)));
						},
						triggerEvent: "dataProvided",
						storable: true,
						abortable: true
					}
				);
			}
		}
	},

	//must implement every value in the field Campus_Content__c.Type__c:
	//custom order as defined by client:
	//eLearning - Classroom Program - ...and more!
	customOrderKeys : function(keys) {
		var returnCustomOrderKeys = [];
		var customOrder = ["eLearning","Classroom Programs","...and more!"
			,"Programmes en classe","...et plus!"];
		for (o = 0; o < customOrder.length; o++) {
			var strFind = customOrder[o];
			for (i = 0; i < keys.length; i++) { 
				if(keys[i] == strFind) {
					returnCustomOrderKeys.push(keys[i]);
				}
			}
		}
		return returnCustomOrderKeys;
	}
})