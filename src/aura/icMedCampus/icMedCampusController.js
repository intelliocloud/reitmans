({
	doInit : function(component, event, helper) {
		helper.doInit(component, event);
	},

	doNavKeyChange : function(component, event, helper) {
		helper.doNavKeyChange(component, event);
	}
})