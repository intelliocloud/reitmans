({
	setLanguage : function(component, event, helper) {
		var locale = $A.get("$Locale.language");
        if (locale != 'EN') {
            component.set("v.IFrameURL", component.get("v.ALT_URL") );
        }else{
            component.set("v.IFrameURL", component.get("v.EN_URL") );
        }
	}
})