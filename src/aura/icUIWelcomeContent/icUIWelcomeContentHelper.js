({
	doInit : function(component, event) {
		this.loadContent(component);
	},

	doContentChange : function(component, event) {
		this.loadContent(component);
	},

	loadContent : function(component) {
		var mapWelcomeContents = component.get("v.mapWelcomeContents");
		var mapWelcomeContentKey = component.get("v.mapWelcomeContentKey");

		component.set("v.welcomeContents", mapWelcomeContents[mapWelcomeContentKey]);
	}
})