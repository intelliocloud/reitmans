({
	doInit : function(component, event, helper) {
		helper.doInit(component, event);
	},

	doContentChange : function(component, event, helper) {
		helper.doContentChange(component, event);
	}
})