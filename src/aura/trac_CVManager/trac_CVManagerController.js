({
	setLanguage : function(component, event, helper) {
		var locale = $A.get("$Locale.language");
        if (locale == 'fr') {
            component.set("v.userLanguage", 'f');
        } 
	}
})