({
	doInit : function(component, event, helper) {
		helper.doInit(component, event);
	},

	doFilterChange : function(component, event, helper) {
		helper.doFilterChange(component, event);
	},
	
	doSearchKeyChange : function(component, event, helper) {
		helper.doSearchKeyChange(component, event);
	},

	doClickContent : function(component, event, helper) {
		helper.doClickContent(component, event);
	}
})