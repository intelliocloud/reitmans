({
	doInit : function(component, event) {
		this.validateDisplayContent(component);
	},

	doFilterChange : function(component, event) {
		this.validateDisplayContent(component);
	},

	doSearchKeyChange : function(component, event) {
		this.validateDisplayContent(component);
	},

	validateDisplayContent : function(component) {
		var searchKey = component.get("v.searchKey");
		var displayContent = true;

		if(searchKey != null && searchKey != undefined && searchKey.length > 0) {
			searchKey = searchKey.toLowerCase();
			var content = component.get("v.content");
			var title = content.title.toLowerCase();
			var name = content.name.toLowerCase();
			if(name.search(searchKey) < 0 && title.search(searchKey) < 0) {
				displayContent = false;
			}
		}
		
		var uiContentContainer = component.find('uiContentContainer');
		if(displayContent) {
			$A.util.removeClass(uiContentContainer, "search-filtered");
		} else {
			$A.util.addClass(uiContentContainer, "search-filtered");
		}
	},

	doClickContent : function(component, event) {
		var content = component.get("v.content");
		component.set("v.currentContent", content);
	}
})