({
	doInit : function(component, event, helper) {
		helper.doInit(component, event);
	},

	doClickAllCategory : function(component, event, helper) {
		helper.doClickAllCategory(component, event);
	},

	doFilterChange : function(component, event, helper) {
		helper.doFilterChange(component, event);
	}
})