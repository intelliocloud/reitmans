({
	doInit : function(component, event) {
		var mapCategoryContents = component.get("v.mapCategoryContents");
		var mapCategoryContentKey = component.get("v.mapCategoryContentKey");
		var currentFilter = component.get("v.currentFilter");

		component.set("v.categoryContents", mapCategoryContents[mapCategoryContentKey]);
		this.validateDisplayContent(component, currentFilter, mapCategoryContentKey);
	},

	doClickAllCategory : function(component, event) {
		var mapCategoryContentKey = component.get("v.mapCategoryContentKey");		
		component.set("v.currentFilter", mapCategoryContentKey);		
	},

	doFilterChange : function(component, event) {
		var mapCategoryContentKey = component.get("v.mapCategoryContentKey");
		var currentFilter = component.get("v.currentFilter");

		this.validateDisplayContent(component, currentFilter, mapCategoryContentKey);
	},

	validateDisplayContent : function(component, currentFilter, mapCategoryContentKey) {
		var uiContentListContainer = component.find('uiContentListContainer');
		if(currentFilter == "All" || currentFilter == mapCategoryContentKey) {
			$A.util.removeClass(uiContentListContainer, "filter-filtered");
		} else {
			$A.util.addClass(uiContentListContainer, "filter-filtered");
		}
	}
})