({
	doClick : function(component, event, helper) {
		var lastCrumb = component.get("v.lastCrumb");
		if(!lastCrumb) {
			var navCategory = component.get("v.navCategory");

			component.set("v.currentCategory", navCategory);
			component.set("v.navKey", navCategory.id);
		}
	}
})