({
    // Handle component initialization 
    doInit : function(component, event, helper) {
        var type = component.get("v.type");
        var subjectIdEN = component.get("v.subjectIdEN");
        var subjectIdFR = component.get("v.subjectIdFR");
		var locale = $A.get("$Locale.language");
        var feedDesignVal = helper.feedDisplay(component);
        
        if(locale =="en" ){
            var subjectId = subjectIdEN;
        }else if( locale == "fr" ){
            var subjectId = subjectIdFR;
        }
        
        // Dynamically create the feed with the specified type
        $A.createComponent("forceChatter:feed", {"type": type, "subjectId" : subjectId, "feedDesign": feedDesignVal}, function(feed) {
            var feedContainer = component.find("feedContainer");
            feedContainer.set("v.body", feed);	
        });
    }
})