({
	doClick : function(component, event) {
		var currentCategory = component.get("v.currentCategory");
		var navCategory = component.get("v.navCategory");

		component.set("v.currentCategory", navCategory);
		component.set("v.navKey", null);
		component.set("v.navKey", navCategory.id);
	}
})