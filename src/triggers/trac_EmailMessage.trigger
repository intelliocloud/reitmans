/**
 *  @description Trigger for EmailMessage
 *  @author 	 Graham Barnard, Traction on Demand.
 *  @date        2016-02-10
 */
trigger trac_EmailMessage on EmailMessage (after insert, before insert) {

	if (Trigger.isInsert && Trigger.isBefore){
		trac_ClosedCaseEmail.onInsert(Trigger.new);
	}	

 	if (Trigger.isInsert && Trigger.isAfter) {
 		trac_EmailToCase emailToCase = new trac_EmailToCase(Trigger.new);
 		emailToCase.associateBannerToCases();
	}
}