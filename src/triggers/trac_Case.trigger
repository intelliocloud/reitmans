/**
 *  @description Case trigger
 *  @author 	 Tanminder Rai, Traction on Demand.
 *  @date        2016-02-03
 */
trigger trac_Case on Case (before insert, after insert, before update, after update) {

 	if(Trigger.isInsert && Trigger.isBefore) {
 		trac_EmailToCaseLoopPrevent.checkEmailToCaseLoop(Trigger.new);

 		trac_Entitlement entitlement = new trac_Entitlement(Trigger.new);
   		entitlement.associateEntitlementToCase();
 	}
 	else if (Trigger.isInsert && Trigger.isAfter ) {
		trac_AssignContactToCase.assignContact(Trigger.new);
		trac_CaseCoupon.couponIsUsed(Trigger.new);
	}
	else if (Trigger.isUpdate && Trigger.isBefore) {
 		trac_Entitlement entitlement = new trac_Entitlement(Trigger.new);
   		entitlement.associateEntitlementToCase();
	}
	else if (Trigger.isUpdate && Trigger.isAfter) {
		trac_AssignContactToCase.onUpdate(Trigger.newMap, Trigger.oldMap );
		trac_AssignContactToCase.runAssignmentRules(Trigger.new);
		trac_CaseCoupon.couponIsUsed(Trigger.new, Trigger.oldMap);
	}
}