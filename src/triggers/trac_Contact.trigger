/**
 *  @description Contact trigger
 *  @author 	 Tanminder Rai, Traction on Demand.
 *  @date        2016-02-03
 */
trigger trac_Contact on Contact (before insert, before update, after insert, after update) {
	if(Trigger.isBefore && Trigger.isInsert) {
		trac_Account.syncContactsByEmail(Trigger.new);
	}
	else if(Trigger.isBefore && Trigger.isUpdate) {
		trac_Account.syncContactsByEmail(Trigger.new, Trigger.oldMap);
	}
	else if(Trigger.isAfter && Trigger.isInsert) {
		trac_Contact.saveNewContacts(Trigger.new);
		trac_Contact.updateMembership(Trigger.new);
	}
	else if(Trigger.isAfter && Trigger.isUpdate) {
		trac_Contact.saveExistingContacts(Trigger.new, Trigger.oldMap);
		trac_Contact.updateMembership(Trigger.new, Trigger.oldMap);
	}

}