/**
 *  @description Trigger on Request__c object
 *  @author 	 Marco Martel, Traction on Demand.
 *  @date        2016-02-03
 */

trigger trac_Request on Request__c (after insert, after update) {

	if((Trigger.isUpdate || Trigger.isInsert) && Trigger.isAfter){
		trac_CaseRequest.CaseUpdate(Trigger.new, Trigger.oldMap);

	}
}