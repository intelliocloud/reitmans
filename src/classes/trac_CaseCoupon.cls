/**
 *  @description Update Coupon object if it is used by a Case
 *  @author 	 Marco Martel, Traction on Demand.
 *  @date        2016-02-11
 */

public without sharing class trac_CaseCoupon {

	public static String USED_STATUS = 'Used';
	public static void couponIsUsed(List<Case> newCases) {

		List<Coupon__c> couponsToBeUpdated = new List<Coupon__c>();

		Map<Id, Case> casesWithCoupons1 = new Map<Id, Case>([
			SELECT Coupon_Code_1__r.Is_Coupon_Used__c, Coupon_Code_1__r.Case__c, Id
			FROM Case
			WHERE Coupon_Code_1__c != null AND Id IN :newCases]);

		Map<Id, Case> casesWithCoupons2 = new Map<Id, Case>([
			SELECT Coupon_Code_2__r.Is_Coupon_Used__c, Coupon_Code_2__r.Case__c, Id
			FROM Case
			WHERE Coupon_Code_2__c != null AND Id IN :newCases]);

		for(Case triggerCase:casesWithCoupons1.values()){
			if(!triggerCase.Coupon_Code_1__r.Is_Coupon_Used__c && triggerCase.Coupon_Code_1__r.Case__c == null){
				triggerCase.Coupon_Code_1__r.Is_Coupon_Used__c = true;
				triggerCase.Coupon_Code_1__r.Case__c = triggerCase.id;
				triggerCase.Coupon_Code_1__r.Status__c = USED_STATUS;
				couponsToBeUpdated.add(triggerCase.Coupon_Code_1__r);
			}
		}

		for(Case triggerCase:casesWithCoupons2.values()){
			if(!triggerCase.Coupon_Code_2__r.Is_Coupon_Used__c && triggerCase.Coupon_Code_2__r.Case__c == null){
				triggerCase.Coupon_Code_2__r.Is_Coupon_Used__c = true;
				triggerCase.Coupon_Code_2__r.Case__c = triggerCase.id;
				triggerCase.Coupon_Code_2__r.Status__c = USED_STATUS;
				couponsToBeUpdated.add(triggerCase.Coupon_Code_2__r);
			}
		}

		if(couponsToBeUpdated.size()>0){
			update couponsToBeUpdated;
		}

	}

	public static void couponIsUsed(List<Case> triggerNew, Map<Id, Case> triggerOld){

		List<Case> casesWithCouponChanges = new List<Case>();
		for(Case triggerCase:triggerNew){
			if((triggerCase.Coupon_Code_1__c != triggerOld.get(triggerCase.Id).Coupon_Code_1__c) || (triggerCase.Coupon_Code_2__c != triggerOld.get(triggerCase.Id).Coupon_Code_2__c)){
				casesWithCouponChanges.add(triggerCase);
			}
		}

		if(casesWithCouponChanges.size()>0){
			couponIsUsed(casesWithCouponChanges);
		}
	} 
}