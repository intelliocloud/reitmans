//Generated by FuseIT WSDL2Apex (http://www.fuseit.com/Solutions/SFDC-Explorer/Help-WSDL-Parser.aspx)
/**
 *  @description  Wsdl2Apex Mock class from https://webservice.s6.exacttarget.com/etframework.wsdl
 *  @author      Zi Zhang, Traction on Demand.
 *  @date        2016-02-24
 */
@isTest
public class exacttargetComWsdlPartnerapiMockImpl implements WebServiceMock {
	public static  Boolean isSuccess = false;
	public static  Boolean noResults = false;

	public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType) {
       
		System.debug(LoggingLevel.INFO, 'exacttargetComWsdlPartnerapiMockImpl.doInvoke() - ' +
			'\n request: ' + request +
			'\n response: ' + response +
			'\n endpoint: ' + endpoint +
			'\n soapAction: ' + soapAction +
			'\n requestName: ' + requestName +
			'\n responseNS: ' + responseNS +
			'\n responseName: ' + responseName +
			'\n responseType: ' + responseType);

		if(request instanceOf exacttargetComWsdlPartnerapi.ConfigureRequestMsg_element) {
			response.put( 'response_x', new exacttargetComWsdlPartnerapi.ConfigureResponseMsg_element());
		}
		else if(request instanceOf exacttargetComWsdlPartnerapi.CreateRequest_element) {
			response.put( 'response_x', new exacttargetComWsdlPartnerapi.CreateResponse_element());
		}
		else if(request instanceOf exacttargetComWsdlPartnerapi.DeleteRequest_element) {
			response.put( 'response_x', new exacttargetComWsdlPartnerapi.DeleteResponse_element());
		}
		else if(request instanceOf exacttargetComWsdlPartnerapi.DefinitionRequestMsg_element) {
			response.put( 'response_x', new exacttargetComWsdlPartnerapi.DefinitionResponseMsg_element());
		}
		else if(request instanceOf exacttargetComWsdlPartnerapi.ExecuteRequestMsg_element) {
			response.put( 'response_x', new exacttargetComWsdlPartnerapi.ExecuteResponseMsg_element());
		}
		else if(request instanceOf exacttargetComWsdlPartnerapi.ExtractRequestMsg_element) {
			response.put( 'response_x', new exacttargetComWsdlPartnerapi.ExtractResponseMsg_element());
		}
		else if(request instanceOf exacttargetComWsdlPartnerapi.SystemStatusRequestMsg_element) {
			response.put( 'response_x', new exacttargetComWsdlPartnerapi.SystemStatusResponseMsg_element());
		}
		else if(request instanceOf exacttargetComWsdlPartnerapi.PerformRequestMsg_element) {
			response.put( 'response_x', new exacttargetComWsdlPartnerapi.PerformResponseMsg_element());
		}
		else if(request instanceOf exacttargetComWsdlPartnerapi.QueryRequestMsg_element) {
			response.put( 'response_x', new exacttargetComWsdlPartnerapi.QueryResponseMsg_element());
		}
		else if(request instanceOf exacttargetComWsdlPartnerapi.RetrieveRequestMsg_element) {
			exacttargetComWsdlPartnerapi.RetrieveResponseMsg_element rrm = new exacttargetComWsdlPartnerapi.RetrieveResponseMsg_element();
			if(isSuccess) {
				rrm.OverallStatus = 'OK';
				exacttargetComWsdlPartnerapi.APIObject obj = new exacttargetComWsdlPartnerapi.APIObject();
				obj.ID = '001';
				obj.Status = 'Active';

				if(!noResults) {
					exacttargetComWsdlPartnerapi.APIProperty emailAddressProp = new exacttargetComWsdlPartnerapi.APIProperty();
					emailAddressProp.Name = 'emailaddr';
					emailAddressProp.Value = 'test@test.com';
					exacttargetComWsdlPartnerapi.APIProperty emailNameProp = new exacttargetComWsdlPartnerapi.APIProperty();
					emailNameProp.Name = 'emailname_';
					emailNameProp.Value = 'testEmailName';
					exacttargetComWsdlPartnerapi.APIProperty emailSendDateProp = new exacttargetComWsdlPartnerapi.APIProperty();
					emailSendDateProp.Name = 'SendDate';
					emailSendDateProp.Value = '' + Datetime.now();
					exacttargetComWsdlPartnerapi.APIProperty emailJobProp = new exacttargetComWsdlPartnerapi.APIProperty();
					emailJobProp.Name = 'JobID';
					emailJobProp.Value = '123';
					exacttargetComWsdlPartnerapi.APIProperty emailUrlProp = new exacttargetComWsdlPartnerapi.APIProperty();
					emailUrlProp.Name = 'view_email_url';
					emailUrlProp.Value = 'www.test.com';
					exacttargetComWsdlPartnerapi.APIProperty emailIdProp = new exacttargetComWsdlPartnerapi.APIProperty();
					emailIdProp.Name = 'emailid';
					emailIdProp.Value = '1010';
					exacttargetComWsdlPartnerapi.APIProperty subIdProp = new exacttargetComWsdlPartnerapi.APIProperty();
					subIdProp.Name = 'SubID';
					subIdProp.Value = '123456';
					exacttargetComWsdlPartnerapi.Property props = new exacttargetComWsdlPartnerapi.Property();
					props.Property = new List<exacttargetComWsdlPartnerapi.APIProperty>{emailAddressProp, 
																						emailNameProp, 
																						emailSendDateProp, 
																						emailJobProp, 
																						emailUrlProp, 
																						emailIdProp, 
																						subIdProp};
					obj.Properties = props;

					rrm.Results = new List<exacttargetComWsdlPartnerapi.APIObject>{obj};
				} else {
					rrm.Results = null;
				}

				
			}
			
			response.put( 'response_x', rrm);
		}
		else if(request instanceOf exacttargetComWsdlPartnerapi.ScheduleRequestMsg_element) {
			response.put( 'response_x', new exacttargetComWsdlPartnerapi.ScheduleResponseMsg_element());
		}
		else if(request instanceOf exacttargetComWsdlPartnerapi.UpdateRequest_element) {
			response.put( 'response_x', new exacttargetComWsdlPartnerapi.UpdateResponse_element());
		}
		else if(request instanceOf exacttargetComWsdlPartnerapi.VersionInfoRequestMsg_element) {
			response.put( 'response_x', new exacttargetComWsdlPartnerapi.VersionInfoResponseMsg_element());
		}
	}
}