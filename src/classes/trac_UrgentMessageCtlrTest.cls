/**
 *  @description Test Class for trac_UrgentMessageCtlr
 *  @author 	 Jeremy Horan, Traction on Demand.
 *  @date        2016-10-18
 */
@isTest
private class trac_UrgentMessageCtlrTest {
	
	@isTest static void test_controller() {
		Urgent_Message__c urgentMessage = new Urgent_Message__c(
			Banner__c = 'Reitmans',
			Activation_Date__c = Date.today(),
			Body__c = 'TEST',
			Expiration_Date__c = Date.today().addDays(3)
		);
		insert urgentMessage;

		Profile currentUserProfile = trac_ProfileSelector.findById(UserInfo.getProfileId());
		Profile_Banner_Community_Settings__c profileBannerCommunitySetting = new Profile_Banner_Community_Settings__c(
			Name = currentUserProfile.Name,
			Banner__c = 'Reitmans',
			Profile__c = currentUserProfile.Name
		);
		insert profileBannerCommunitySetting;

		trac_UrgentMessageCtlr ctlr = new trac_UrgentMessageCtlr();
		System.assertEquals(1, trac_UrgentMessageCtlr.getUrgentMessages().size());
		System.assertNotEquals(null,trac_UrgentMessageCtlr.getUrgentMessage(urgentMessage.Id));
	}	
}