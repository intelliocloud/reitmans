public class icBusinessLogicCampusCategory implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Campus_Category__c> getAllActiveCampusCategory();
		Map<String, List<icDTOCategory>> getMapCampusCategoriesFromList(List<Campus_Category__c> categories);
	}

	public class Impl implements IClass {

		icRepositoryCampusCategory.IClass repoCategory = (icRepositoryCampusCategory.IClass) icObjectFactory.GetSingletonInstance('icRepositoryCampusCategory');
		
		public List<Campus_Category__c> getAllActiveCampusCategory() {
			return repoCategory.getAllActiveCampusCategory();
		}

		public Map<String, List<icDTOCategory>> getMapCampusCategoriesFromList(List<Campus_Category__c> categories) {
			Map<String, List<icDTOCategory>> returnMap = new Map<String, List<icDTOCategory>>();

			String language = UserInfo.getLanguage();
			String mapKey;
			for(Campus_Category__c category : categories) {
				mapKey = category.Parent_Category__c;
				if(mapKey != null) {
					List<icDTOCategory> thisKeyCategories = returnMap.get(mapKey);
					if(thisKeyCategories == null) {
						thisKeyCategories = new List<icDTOCategory>();
					}
					thisKeyCategories.add(mapSFToDTO(category, mapKey, language));
					returnMap.put(mapKey, thisKeyCategories);
				}
			}

			return returnMap;
		}

		private icDTOCategory mapSFToDTO(Campus_Category__c category, String parent, String language) {
			icDTOCategory returnDTO = new icDTOCategory();
			returnDTO.id = category.Id;
			returnDTO.parent = parent;
			returnDTO.name = category.Name;
			returnDTO.label = category.Label_EN__c;
			if(language == 'FR') {
				returnDTO.label = category.Label_FR__c;
			}
			return returnDTO;
		}
	}
}