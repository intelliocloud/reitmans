/**
 *  @description Controller for trac_InstagramCandidCarousel VF page
 *  @author    Brian Williams, Traction on Demand.
 *  @date        2016-08-31
 */
public with sharing class trac_TwitterFeedPageCtrl {
  
  public String widgetId{get;set;}
  public String TweetUser{get;set;}
  public String TweetNumber{get;set;}

  public trac_TwitterFeedPageCtrl() {
    widgetId = ApexPages.currentPage().getParameters().get('widgetId');
    TweetUser = ApexPages.currentPage().getParameters().get('TweetUser');
    TweetNumber = ApexPages.currentPage().getParameters().get('TweetNumber');
  }
}