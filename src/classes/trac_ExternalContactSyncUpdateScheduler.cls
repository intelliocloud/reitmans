/**
 *  @description Implements trac_ExternalContactSyncBatch and trac_ExternalContactUpdateBatch
 *  @author 	 Jeremy Horan, Traction on Demand.
 *  @date        2016-03-10
 */
global class trac_ExternalContactSyncUpdateScheduler implements Schedulable {
	public static String CRON_EXP = '0 0 5 * * ?'; // Runs everyday at 5 am
   
  global static String scheduleDaily(String batchName) {
		trac_ExternalContactSyncUpdateScheduler externalContactSyncUpdateScheduler = new trac_ExternalContactSyncUpdateScheduler();
       	return System.schedule(batchName, CRON_EXP, externalContactSyncUpdateScheduler);
  }

	global void execute(SchedulableContext sc) {
		trac_ExternalContactSyncBatch externalContactSyncBatch = new trac_ExternalContactSyncBatch();
        Database.executeBatch(externalContactSyncBatch, 50);
	}
}