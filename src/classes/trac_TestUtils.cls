/**
 *  @description Setup for test objects and test scenarios reuable throughout all tests
 *  @author 	 Graham Barnard, Traction on Demand.
 *  @date        2015-12-10
 */


public with sharing class trac_TestUtils {

	private static Id contactCustomerServiceRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Service RCL').getRecordTypeId();
	private static Id accountCustomerServiceRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Service RCL').getRecordTypeId();

	/**
	 *  @description Setup a test account
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2015-12-10
	 */
	public static Account setupAccount() {
		Account account = new Account();
		account.Name = 'Test Account';
		account.RecordTypeId = accountCustomerServiceRecordTypeId;
		return account;
	}

	/**
	 *  @description Setup a test contact
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2015-12-10
	 */
	public static Contact setupContact(Id accountId) {
		Contact contact = new Contact();
		contact.FirstName = 'Contact First Name';
		contact.LastName = 'Contact Last Name';
		contact.Email = 'test@email.com';
		contact.AccountId = accountId;
		contact.RecordTypeId = contactCustomerServiceRecordTypeId;
		return contact;
	}

	/**
	 *  @description Setup a test contact with no Account
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-03-23
	 */
	public static Contact setupContact() {
		Contact contact = new Contact();
		contact.FirstName = 'Contact First Name';
		contact.LastName = 'Contact Last Name';
		contact.Email = 'test@email.com';
		contact.RecordTypeId = contactCustomerServiceRecordTypeId;
		return contact;
	}

	/**
	 *  @description Setup a test external customer
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2015-12-10
	 */
	public static Ext_Contact__x setupCustomer() {
		Ext_Contact__x customer = new Ext_Contact__x(
			brand_prefix__c = 'RE',
			customer_no__c = 1234567890,
			email_address__c = 'test@email.com',
			first_name__c = 'test first name',
			last_name__c = 'test last name',
			customer_id__c = 123456789,
			telephone_no__c = '514-111-1111',
			mobile_phone_no__c = '514-222-2222',
			gender__c = 'F',
			language_code__c = 'eng',
			birth_date__c = DateTime.now(),
			membership_type_code__c = 'abcd',
			email_opt_in__c = 1,
			email_opt_in_date__c = DateTime.now(),
			create_source__c = 'source',
			segmentation_flag_a__c = true,
			segmentation_flag_b__c = true,
			segmentation_flag_c__c = true,
			segmentation_flag_d__c = true,
			segmentation_flag_e__c = true,
			segmentation_flag_f__c = true
		);
		return customer;
	}

	/**
	 *  @description Setup a test case
	 *  @author 	 Marco Martel, Traction on Demand.
	 *  @date        2016-02-09
	 */
	public static Case setupCase(Id contactId){
		Case newCase = new Case();
		newCase.ContactId = contactId;
		newCase.Status = 'New';
		newCase.Origin = 'Web';
		newCase.Category__c = 'Gift Card';
		newCase.Sub_Category__c = 'Activation';
		return newCase;
	}

	public static List<Case> createCases(Integer num){	
		List<Case> newList = new List<Case>();

		for(Integer i =0; i < num; i++){
			newList.add( new Case() );
		}
		return newList;
	}

	public static List<EmailMessage> createEmailMessages(Integer num){
		List<EmailMessage> newList = new List<EmailMessage>();

		for(Integer i =0; i < num; i++){
			newList.add( new EmailMessage() );
		}
		return newList;		
	}

	/**
	 *  @description Setup a test email message
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-09
	 */
	public static EmailMessage setupEmailMessage(Id parentId){
		EmailMessage emailMessage = new EmailMessage();
		emailMessage.ParentId = parentId;
		emailMessage.ToAddress = 'test@email.com';
		return emailMessage;
	}

	/**
	 *  @description Setup a test Coupon
	 *  @author 	 Marco Martel, Traction on Demand.
	 *  @date        2016-02-12
	 */
	public static Coupon__c setupCoupon(){
		Coupon__c newCoupon = new Coupon__c();
		newCoupon.Name = 'Test Coupon';
		return newCoupon;
	}

	/**
	 *  @description Setup a test Customer Service User
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-06-09
	 */
	public static User setupCustomerServiceUser() {
		Profile profile = [SELECT Id FROM Profile WHERE Name='Customer Service'];
		User user = new User(
		 	Alias = generateRandomString(5), 
		 	Email = generateRandomString(8) + '@testorg.com', 
            EmailEncodingKey ='UTF-8', 
            LastName ='Testing', 
            LanguageLocaleKey ='en_US', 
            LocaleSidKey ='en_US', 
            ProfileId = profile.Id, 
            TimeZoneSidKey ='America/Los_Angeles', 
            UserName = generateRandomString(8) + '@testorg.com'
        );
		insert user;
        return user;
	}

	/**
	 *  @description Sets up a list of External Contact Xrefs updated today
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-03-11
	 */
	public static List<sObject> setupMockExternalContactXrefs(){
		Ext_Contact_Xref__x externalContactXref = new Ext_Contact_Xref__x(
			brand_prefix__c = 'RE',
			old_customer_no__c = 1345678901,
			new_customer_no__c = 1345678902
		);

		List<sObject> mockExternalContactXrefs = new List<sObject>{externalContactXref};

		return mockExternalContactXrefs;
	}

	/**
	 *  @description Sets up a list of External Contacts updated today
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-03-11
	 */
	public static List<sObject> setupMockExternalContacts(){
		Ext_Contact__x externalContact = new Ext_Contact__x(
			brand_prefix__c = 'RE',
			customer_no__c = 1345678901,
			email_address__c = 'test@email.com',
			first_name__c = 'test first name',
			last_name__c = 'test last name',
			customer_id__c = 134567891,
			telephone_no__c = '514-111-1111',
			mobile_phone_no__c = '514-222-2222',
			gender__c = 'F',
			language_code__c = 'eng',
			birth_date__c = DateTime.now(),
			membership_type_code__c = 'abcd',
			email_opt_in__c = 1,
			email_opt_in_date__c = DateTime.now(),
			create_source__c = 'source',
			segmentation_flag_a__c = true,
			segmentation_flag_b__c = true,
			segmentation_flag_c__c = true,
			segmentation_flag_d__c = true,
			segmentation_flag_e__c = true,
			segmentation_flag_f__c = true,
			last_updated__c = Datetime.now()
		);

		Ext_Contact__x externalContact2 = new Ext_Contact__x(
			brand_prefix__c = 'RE',
			customer_no__c = 1345678902,
			email_address__c = 'test@email.com',
			first_name__c = 'test first name',
			last_name__c = 'test last name',
			customer_id__c = 134567892,
			telephone_no__c = '514-111-1111',
			mobile_phone_no__c = '514-222-2222',
			gender__c = 'F',
			language_code__c = 'eng',
			birth_date__c = DateTime.now(),
			membership_type_code__c = 'abcd',
			email_opt_in__c = 1,
			email_opt_in_date__c = DateTime.now(),
			create_source__c = 'source',
			segmentation_flag_a__c = true,
			segmentation_flag_b__c = true,
			segmentation_flag_c__c = true,
			segmentation_flag_d__c = true,
			segmentation_flag_e__c = true,
			segmentation_flag_f__c = true,
			last_updated__c = Datetime.now()
		);

		List<sObject> mockExternalContacts = new List<sObject>{externalContact, externalContact2};

		return mockExternalContacts;
	}

	/**
	 *  @description Generates random alphanumeric string of given length
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-03-11
	 */
	public static String generateRandomString(Integer len) {
	    final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
	    String randStr = '';
	    while (randStr.length() < len) {
	      	Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
	      	randStr += chars.substring(idx, idx + 1);
	    }
	    return randStr;
	}
}