/**
 *  @description Base Model that all SObjects will inherent
 *  @author 	 Graham Barnard, Traction on Demand.
 *  @date        2016-02-05
 */
public with sharing class trac_SObjectModel {

	/**
	 *  @description Return the list of ids of a given list of sObjects
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-05
	 */
	
	public static Set<Id> getIds(List<sObject> sObjects) {
		if(sObjects == null) {
			return new Set<Id>();
		}
	   return (new Map<Id, sObject>(sObjects)).keySet();
	}

	/**
	 *  @description Is this a case record
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-10
	 */
	public static Boolean isCase(Id whatId)	{
		return (whatId.getSObjectType() == Schema.Case.getSObjectType());
	}
}