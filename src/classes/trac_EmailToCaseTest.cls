/**
 *  @description Tests for trac_EmailToCase
 *  @author      Graham Barnard, Traction on Demand.
 *  @date        2016-02-09
 */
@isTest
private class trac_EmailToCaseTest {

    /**
     *  @description Test with no email banner
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-09
     */
    @isTest static void testWithNoEmailBanner() {

        Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        insert contact;

        Case testCase = trac_TestUtils.setupCase(contact.Id);
        testCase.Banner__c = null;
        testCase.Origin = 'Email';
        insert testCase;

        Test.startTest();
        EmailMessage emailMessage = trac_TestUtils.setupEmailMessage(testCase.Id);
        emailMessage.ToAddress = 'valid@email.com';
        insert emailMessage;
        Test.stopTest();

        List<Case> cases = [SELECT Id, Banner__c, Language__c FROM Case WHERE Id =: testCase.Id];
        System.assertEquals(1, cases.size(), 'Should return 1 case record');

        System.assertEquals(null, cases[0].Banner__c, 'Banner should not be set');
        System.assertEquals(null, cases[0].Language__c, 'Language should not be set');
    }

    /**
     *  @description Test with no email banner
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-09
     */
    @isTest static void testWithInvalidEmailBanner() {
        Banner_Email__c bannerEmail = new Banner_Email__c();
        bannerEmail.Name = 'invalid';
        bannerEmail.Email__c = 'invalid@email.com';
        bannerEmail.Banner__c = 'REIT';
        insert bannerEmail;

        Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        insert contact;

        Case testCase = trac_TestUtils.setupCase(contact.Id);
        testCase.Banner__c = null;
        testCase.Origin = 'Email';
        insert testCase;

        Test.startTest();
        EmailMessage emailMessage = trac_TestUtils.setupEmailMessage(testCase.Id);
        emailMessage.ToAddress = 'valid@email.com';
        insert emailMessage;
        Test.stopTest();

        List<Case> cases = [SELECT Id, Banner__c, Origin, Language__c FROM Case WHERE Id =: testCase.Id];
        System.assertEquals(1, cases.size(), 'Should return 1 case record');

        System.assertEquals(null, cases[0].Banner__c, 'Banner should not be set');
        System.assertEquals(null, cases[0].Language__c, 'Language should not be set');
    }

    /**
     *  @description Test with no email banner
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-09
     */
    @isTest static void testWithValidEmailBanner() {
        Banner_Email__c bannerEmail = new Banner_Email__c();
        bannerEmail.Name = 'valid';
        bannerEmail.Email__c = 'valid@email.com';
        bannerEmail.Banner__c = 'REIT';
        bannerEmail.Language__c = 'EN';
        insert bannerEmail;

        Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        insert contact;

        Case testCase = trac_TestUtils.setupCase(contact.Id);
        testCase.Banner__c = null;
        testCase.Origin = 'Email';
        insert testCase;


        Test.startTest();
        EmailMessage emailMessage = trac_TestUtils.setupEmailMessage(testCase.Id);
        emailMessage.ToAddress = 'valid@email.com';
        insert emailMessage;
        Test.stopTest();

        List<Case> cases = [SELECT Id, Banner__c, Language__c FROM Case WHERE Id =: testCase.Id];
        System.assertEquals(1, cases.size(), 'Should return 1 case record');

        System.assertEquals('REIT', cases[0].Banner__c, 'Banner should be set to REIT');
        System.assertEquals('EN', cases[0].Language__c, 'Language should be set to EN');
    }

    /**
     *  @description Test with no email banner
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-09
     */
    @isTest static void testMultipleValidEmailBanners() {
        Banner_Email__c bannerEmail1 = new Banner_Email__c();
        bannerEmail1.Name = 'valid1';
        bannerEmail1.Email__c = 'valid1@email.com';
        bannerEmail1.Banner__c = 'REIT';
        bannerEmail1.Language__c = 'EN';

        Banner_Email__c bannerEmail2 = new Banner_Email__c();
        bannerEmail2.Name = 'valid2';
        bannerEmail2.Email__c = 'valid2@email.com';
        bannerEmail2.Banner__c = 'RWCO';
        bannerEmail2.Language__c = 'FR';
        insert new List<Banner_Email__c>{bannerEmail1, bannerEmail2};

        Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        insert contact;

        Case testCase1 = trac_TestUtils.setupCase(contact.Id);
        testCase1.Banner__c = null;
        testCase1.Origin = 'Email';

        Case testCase2 = trac_TestUtils.setupCase(contact.Id);
        testCase2.Banner__c = null;
        testCase2.Origin = 'Email';

        List<Case> cases = new List<Case>{testCase1, testCase2};
        insert cases;

        Test.startTest();
        EmailMessage emailMessage1 = trac_TestUtils.setupEmailMessage(testCase1.Id);
        emailMessage1.ToAddress = 'valid1@email.com';

        EmailMessage emailMessage2 = trac_TestUtils.setupEmailMessage(testCase2.Id);
        emailMessage2.ToAddress = 'valid2@email.com';
        insert new List<EmailMessage>{emailMessage1, emailMessage2};
        Test.stopTest();

        cases = [SELECT Id, Banner__c, Language__c FROM Case WHERE Id =: testCase1.Id];
        testCase1 = cases[0];

        cases = [SELECT Id, Banner__c, Language__c FROM Case WHERE Id =: testCase2.Id];
        testCase2 = cases[0];

        System.assertEquals('REIT', testCase1.Banner__c, 'Banner should be set to REIT');
        System.assertEquals('EN', testCase1.Language__c, 'Language should be set to EN');

        //System.assertEquals('RWCO', testCase2.Banner__c, 'Banner should be set to REIT');
        //System.assertEquals('FR', testCase2.Language__c, 'Language should be set to FR');
    }
}