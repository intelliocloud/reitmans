global class trac_ExternalContactUpdateScheduler implements Schedulable {
	public static String CRON_EXP = '0 0 * * * ?'; // Runs every hour
	private static Integer MILLISECONDS_TO_HOURS = 3600000;
   
	global static String scheduleHourly(String jobName) {
		trac_ExternalContactUpdateScheduler externalContactUpdateScheduler = new trac_ExternalContactUpdateScheduler();
		return System.schedule(jobName, CRON_EXP, externalContactUpdateScheduler);
	}

	global void execute(SchedulableContext sc) {
		String dateFormat = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';

		DateTime endDate = DateTime.now();
		DateTime startDate = endDate.addHours(-1).addMinutes(-10);
		String endDateString = endDate.format(dateFormat);
		String startDateString = startDate.format(dateFormat);

		trac_ExternalContactUpdate.runContactUpdate(startDateString, endDateString);
	}
}