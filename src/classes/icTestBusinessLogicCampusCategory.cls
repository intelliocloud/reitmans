@isTest
public with sharing class icTestBusinessLogicCampusCategory {

	static void initTest() {
		icTestCampusObjectData.createCampusDataSet();
	}

	@future
	static void futureInitTest()
	{
		icTestCampusObjectData.createCampusDataSet();
	}

	static testMethod void test_getAllActiveCampusCategory() {
		icBusinessLogicCampusCategory.IClass repo = (icBusinessLogicCampusCategory.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicCampusCategory');
		List<Campus_Category__c> result = repo.getAllActiveCampusCategory();		
	}

	static testMethod void test_getMapCampusCategoriesFromList_FR() {
		User newUser = icTestCampusObjectData.buildUser();
		newUser.LocaleSidKey = 'fr_CA';
		newUser.LanguageLocaleKey = 'FR';
		insert newUser;

		System.runAs(newUser) {
			Test.startTest();
			System.debug('test_getMapCampusCategoriesFromList_FR getLanguage() = ' + UserInfo.getLanguage());
			futureInitTest();
			Test.stopTest();
			icBusinessLogicCampusCategory.IClass repo = (icBusinessLogicCampusCategory.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicCampusCategory');
			Map<String, List<icDTOCategory>> result = repo.getMapCampusCategoriesFromList(repo.getAllActiveCampusCategory());			
		}
	}

	static testMethod void test_getMapCampusCategoriesFromList_EN() {
		User newUser = icTestCampusObjectData.buildUser();
		newUser.LocaleSidKey = 'fr_CA';
		newUser.LanguageLocaleKey = 'FR';
		insert newUser;

		System.runAs(newUser) {
			Test.startTest();
			System.debug('test_getMapCampusCategoriesFromList_FR getLanguage() = ' + UserInfo.getLanguage());
			futureInitTest();
			Test.stopTest();
			icBusinessLogicCampusCategory.IClass repo = (icBusinessLogicCampusCategory.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicCampusCategory');
			Map<String, List<icDTOCategory>> result = repo.getMapCampusCategoriesFromList(repo.getAllActiveCampusCategory());			
		}
	}
}