/**
 *  @description Test for trac_BackToSalesforceCTRL
 *  @author      Brian Williams, Traction on Demand.
 *  @date        2016-10-04
 */

@isTest
private class trac_BackToSalesforceCTRLTest {
    
    @isTest static void test_BackToSalesforce() {
        System.assertEquals(true, trac_BackToSalesforceCTRL.hasSalesforceAccess());
    }

    @isTest static void test_BackToSalesforceNoFlag() {
    	Profile profile = [SELECT Id FROM Profile WHERE Name = 'RCL Communities User'];

    	User user = new User(
			Alias = trac_TestUtils.generateRandomString(5),
			Email = trac_TestUtils.generateRandomString(10) + '@' + trac_TestUtils.generateRandomString(5) + '.com',
			EmailEncodingKey = 'UTF-8',
			LastName = trac_TestUtils.generateRandomString(10),
			LanguageLocaleKey = 'en_US',
			LocaleSidKey = 'en_US',
			TimeZoneSidKey = 'America/Los_Angeles',
			ProfileId = profile.Id,
			UserName = trac_TestUtils.generateRandomString(10) + '@' + trac_TestUtils.generateRandomString(5) + '.com'
        );
        System.runAs(user) {
        	System.assertEquals(false, trac_BackToSalesforceCTRL.hasSalesforceAccess());
        }        
    }

    @isTest static void test_BackToSalesforceFlag() {
    	Profile profile = [SELECT Id FROM Profile WHERE Name = 'RCL Communities User'];

    	User user = new User(
			Alias = trac_TestUtils.generateRandomString(5),
			Email = trac_TestUtils.generateRandomString(10) + '@' + trac_TestUtils.generateRandomString(5) + '.com',
			EmailEncodingKey = 'UTF-8',
			LastName = trac_TestUtils.generateRandomString(10),
			LanguageLocaleKey = 'en_US',
			LocaleSidKey = 'en_US',
			TimeZoneSidKey = 'America/Los_Angeles',
			ProfileId = profile.Id,
			UserName = trac_TestUtils.generateRandomString(10) + '@' + trac_TestUtils.generateRandomString(5) + '.com',
			Allowed_back_to_Salesforce__c = true
        );
        System.runAs(user) {
        	System.assertEquals(true, trac_BackToSalesforceCTRL.hasSalesforceAccess());
        }        
    }
}