/**
 *  @description  Email Send History Controller: Get email logs from exacttarget.
 *  @author      Zi Zhang, Traction on Demand.
 *  @date        2016-01-26
 */
public with sharing class trac_EmailSendHistoryCtlr {
    public static final String EMAIL_ID             = 'emailid';
    public static final String EMAIL_ADDRESS        = 'emailaddr';
    public static final String EMAIL_SUBJECT        = 'Subject';
    public static final String EMAIL_NAME           = 'emailname_';
    public static final String EMAIL_SEND_DATE      = 'SendDate';
    public static final String EMAIL_JOB_ID         = 'JobID';
    public static final String EMAIL_SUB_ID         = 'SubID';
    public static final String EMAIL_URL            = 'view_email_url';
    public static final String ID                   = 'ID';
    public static final String SUBSCRIBER_KEY       = 'SubscriberKey';
    public static final String SUBSCRIBER_STATUS    = 'Status';
    public static final String DE_SEND_LOG          = 'DataExtensionObject[SendLog]';
    public static final String DE_SEND_LOG_EMAIL_ID = 'DataExtensionObject[SendLog_EmailID]';
    public static final String WHERE_IN             = 'IN';
    public static final String WHERE_EQUALS         = 'equals';
    public static final String SIMPLE_FILTER_PART   = 'SimpleFilterPart';
    public static final String OVERALL_STATUS_GOOD  = 'OK';
    public static final String EMAIL_OBJECT         = 'Email';
    public static final String SUBSCRIBER_OBJECT    = 'Subscriber';


    private exacttargetComWsdlPartnerapi.Soap stub;
    public Map<String, EmailLog> emailLogsByUrl {get; set;}
    private Map<String, String> emailIdsByEmailUrl;
    private Map<String, String> emailSubjectsByEmailId;
    private Map<String, String> subscriberKeyBySubId;
    private Map<String, String> subStatusBySubId;
    private Map<String, ET_SOAP_API_Login__c> soapLoginByBusinessUnit;
    public String emailAddress {get; set;}
    public String banner {get; set;}

    public trac_EmailSendHistoryCtlr(ApexPages.StandardController stdController) {
        Id recId = stdController.getRecord().Id;
        List<Case> cases;
        List<Contact> contacts;

        // record ID param may be from contact or case page
        if(String.isNotEmpty(recId)) {
        	if(recId.getSObjectType() == Case.sObjectType) {
        		cases = [SELECT Id, Contact.Email, Contact.Banner__c FROM Case WHERE Id =: recId];
        	}else if(recId.getSObjectType() == Contact.sObjectType) {
        		contacts = [SELECT Id, Email, Banner__c FROM Contact WHERE Id =: recId];
        	}
        }

        if(cases != null && cases.size() == 1) {
        	banner = cases[0].Contact.Banner__c;
            emailAddress = cases[0].Contact.Email;
        } else if(contacts != null && contacts.size() == 1) {
        	banner = contacts[0].Banner__c;
            emailAddress = contacts[0].Email;
        }

        if(String.isNotEmpty(banner) && String.isNotEmpty(emailAddress)) {
            // map of email urls to email log wrapper class
            emailLogsByUrl = new Map<String, EmailLog>();

            stub = new exacttargetComWsdlPartnerapi.Soap();
            stub.Header = new exacttargetComWsdlPartnerapi.Security();
            stub.Header.usernameToken = new exacttargetComWsdlPartnerapi.UsernameToken();

            soapLoginByBusinessUnit = ET_SOAP_API_Login__c.getAll();

            if(soapLoginByBusinessUnit.get(banner) != null) {
            	stub.Header.usernameToken.username = soapLoginByBusinessUnit.get(banner).Username__c;
            	stub.Header.usernameToken.password = soapLoginByBusinessUnit.get(banner).Password__c;
            } 

            stub.inputHttpHeaders_x = new Map<String, String>();
            stub.timeout_x = 60000;
        }
        
    }

    // Soap call to SendLog DE to filter by Case Contact's Email address
    public PageReference getEmailSendLogs() {
        if(String.isEmpty(emailAddress)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ' Email Address not found.'));
            return null;
        }

        if(String.isEmpty(banner)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ' Banner is not specified on contact.'));
            return null;
        }

        if(soapLoginByBusinessUnit == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ' Please make sure Banner and Email fields are not empty.'));
            return null;
        }

        if(soapLoginByBusinessUnit.get(banner) == null || String.isEmpty(banner)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ' This business unit is not supported.'));
            return null;
        }

        List<String> emailUrls = new List<String>();
        List<String> emailIds = new List<String>();
        List<String> subscriberIds = new List<String>();

        List<String> properties = new List<String>();
        properties.add(EMAIL_ADDRESS);
        properties.add(EMAIL_NAME);
        properties.add(EMAIL_SEND_DATE);
        properties.add(EMAIL_JOB_ID);
        properties.add(EMAIL_SUB_ID);
        properties.add(EMAIL_URL);
        properties.add(EMAIL_ID);

        exacttargetComWsdlPartnerapi.RetrieveRequest retrieveRequest = new exacttargetComWsdlPartnerapi.RetrieveRequest();
        retrieveRequest.ObjectType = DE_SEND_LOG;
        retrieveRequest.Properties = properties;
        retrieveRequest.Filter = new exacttargetComWsdlPartnerapi.FilterPart();
        retrieveRequest.Filter.type = SIMPLE_FILTER_PART;
        retrieveRequest.Filter.Property = EMAIL_ADDRESS;
        retrieveRequest.Filter.SimpleOperator = WHERE_EQUALS;
        retrieveRequest.Filter.Value = new List<String>{emailAddress};

        try {
            exacttargetComWsdlPartnerapi.RetrieveResponseMsg_element response = stub.Retrieve_x(retrieveRequest);

            if(response.OverallStatus != OVERALL_STATUS_GOOD) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ' Webservice is unavailable. Please try again later.'));
                return null;
            }

            if(response.Results == null || response.Results.size() == 0) {
                return null;
            }

            // If the api response changes, this block will need refactoring
            // Iterate through <Result> tags and get child tagds for processing
            for(exacttargetComWsdlPartnerapi.APIObject result : response.Results) {
                EmailLog anEmailLog = new EmailLog();

                for(exacttargetComWsdlPartnerapi.APIProperty prop : result.Properties.Property) {
                    if(prop.Name == EMAIL_ADDRESS) {
                        anEmailLog.emailAddr = prop.Value;
                        emailAddress = prop.Value;
                    } else if(prop.Name == EMAIL_NAME) {
                        anEmailLog.emailName = prop.Value;
                    } else if(prop.Name == EMAIL_SEND_DATE) {
                        anEmailLog.sendDate = prop.Value;
                    } else if(prop.Name == EMAIL_JOB_ID) {
                        anEmailLog.jobID = prop.Value;
                    } else if(prop.Name == EMAIL_URL) {
                        anEmailLog.emailURL = prop.Value;
                        emailUrls.add(prop.Value);
                    } else if(prop.Name == Email_ID) {
                        anEmailLog.emailId = prop.Value;
                        emailIds.add(prop.Value);
                    } else if(prop.Name == EMAIL_SUB_ID) {
                        anEmailLog.subID = prop.Value;
                        subscriberIds.add(prop.Value);
                    }
                }

                if(String.isNotEmpty(anEmailLog.emailAddr) && String.isNotEmpty(anEmailLog.sendDate)) {
                    emailLogsByUrl.put(anEmailLog.emailURL, anEmailLog);
                }
            }

        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ' Webservice is unavailable. Please try again later.'));
            return null;
        }

        if(subscriberIds.size() > 0) {
            getSubscriberInfo(subscriberIds);

            // Populate the EmailLog wrapper objects
            for(EmailLog aLog : emailLogsByUrl.values()) {
                if(String.isNotEmpty(aLog.subID)) {
                    if(subscriberKeyBySubId.get(aLog.subID) != null) {
                        aLog.subKey = subscriberKeyBySubId.get(aLog.subID);
                    }

                    if(subStatusBySubId.get(aLog.subID) != null) {
                        aLog.subStatus = subStatusBySubId.get(aLog.subID);
                    }
                }
            }
        }

        if(emailIds.size() > 0) {
            getEmailSubject(emailIds);

            // Populate the EmailLog wrapper objects with email subjects
            for(EmailLog aLog : emailLogsByUrl.values()) {
                if(String.isNotEmpty(aLog.emailId) && emailSubjectsByEmailId.get(aLog.emailId) != null) {
                    aLog.emailSubject = emailSubjectsByEmailId.get(aLog.emailId);
                }
            }
        }

        return null;
    }

    // Soap call to Subscriber object to find SubID
    public void getSubscriberInfo(List<String> subIds) {
        subscriberKeyBySubId = new Map<String, String>();
        subStatusBySubId = new Map<String, String>();

        List<String> properties = new List<String>();
        properties.add(ID);
        properties.add(SUBSCRIBER_KEY);
        properties.add(SUBSCRIBER_STATUS);
        exacttargetComWsdlPartnerapi.RetrieveRequest retrieveRequest = new exacttargetComWsdlPartnerapi.RetrieveRequest();
        retrieveRequest.ObjectType = SUBSCRIBER_OBJECT;
        retrieveRequest.Properties = properties;
        retrieveRequest.Filter = new exacttargetComWsdlPartnerapi.FilterPart();
        retrieveRequest.Filter.type = SIMPLE_FILTER_PART;
        retrieveRequest.Filter.Property = ID;
        retrieveRequest.Filter.SimpleOperator = WHERE_EQUALS;
        if(subIds.size() > 1) {
            retrieveRequest.Filter.SimpleOperator = WHERE_IN;
        } else {
            retrieveRequest.Filter.SimpleOperator = WHERE_EQUALS;
        }
        retrieveRequest.Filter.Value = subIds;
        
        exacttargetComWsdlPartnerapi.RetrieveResponseMsg_element response = stub.Retrieve_x(retrieveRequest);

        if(response.OverallStatus != OVERALL_STATUS_GOOD) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ' Webservice is unavailable. Please try again later.'));
            return;
        }

        // No emails retrieved
        if(response == null || response.Results == null) {
            return;
        }

        for(exacttargetComWsdlPartnerapi.APIObject result : response.Results) {
            if(String.isNotEmpty(result.ID)) {
                if(String.isNotEmpty(result.SubscriberKey)) {
                    subscriberKeyBySubId.put(result.ID, result.SubscriberKey);
                }

                if(String.isNotEmpty(result.Status)) {
                    subStatusBySubId.put(result.ID, result.Status);
                }
            }
        }
    }

    // Soap call to Email object to find EmailSubject based on emailId
    @TestVisible
    private void getEmailSubject(List<String> emailIds) {
        emailSubjectsByEmailId = new Map<String, String>();

        List<String> properties = new List<String>();
        properties.add(ID);
        properties.add(EMAIL_SUBJECT);

        exacttargetComWsdlPartnerapi.RetrieveRequest retrieveRequest = new exacttargetComWsdlPartnerapi.RetrieveRequest();
        retrieveRequest.ObjectType = EMAIL_OBJECT;
        retrieveRequest.Properties = properties;
        retrieveRequest.Filter = new exacttargetComWsdlPartnerapi.FilterPart();
        retrieveRequest.Filter.type = SIMPLE_FILTER_PART;
        retrieveRequest.Filter.Property = ID;
        if(emailIds.size() > 1) {
            retrieveRequest.Filter.SimpleOperator = WHERE_IN;
        } else {
            retrieveRequest.Filter.SimpleOperator = WHERE_EQUALS;
        }
        retrieveRequest.Filter.Value = emailIds;
        
        exacttargetComWsdlPartnerapi.RetrieveResponseMsg_element response = stub.Retrieve_x(retrieveRequest);

        if(response == null || response.Results == null || response.OverallStatus != OVERALL_STATUS_GOOD) {
            return;
        }

        for(exacttargetComWsdlPartnerapi.APIObject result : response.Results) {
            String eId = '';
            String eSubject = '';

            if(String.isNotEmpty(result.ID) && String.isNotEmpty(result.Subject)) {
                emailSubjectsByEmailId.put(result.ID, result.Subject);
            }
        }
    }

    public class EmailLog {
        public String emailAddr {get; set;}
        public String emailName {get; set;}
        public String sendDate {get; set;}
        public String jobID {get; set;}
        public String subID {get; set;}
        public String emailURL {get; set;}
        public String emailId {get; set;}
        public String emailSubject {get; set;}
        public String subKey {get; set;}
        public String subStatus {get; set;}

        public EmailLog() {}
    }

    public class CommonException extends Exception {}
}