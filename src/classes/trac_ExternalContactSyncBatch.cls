/**
 *  @description Merges old Salesforce contacts with new contacts using the External Contact Xref object
 *               Updates existing Cases corresponding contact to new contact
 *  @author 	 Graham Barnard, Jeremy Horan Traction on Demand.
 *  @date        2016-02-18
 */
global class trac_ExternalContactSyncBatch implements Database.Batchable<sObject>, Database.AllowsCallouts  {
	
	global String query;
	
	global trac_ExternalContactSyncBatch() {
		query = 'SELECT brand_prefix__c, new_customer_no__c, old_customer_no__c FROM Ext_contact_xref__x WHERE last_update_date__c >= LAST_N_DAYS:1';
	}

	global trac_ExternalContactSyncBatch(Integer numberOfDays) {
		query = 'SELECT brand_prefix__c, new_customer_no__c, old_customer_no__c FROM Ext_contact_xref__x WHERE last_update_date__c >= LAST_N_DAYS:'+String.valueOf(numberOfDays);
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		if (Test.isRunningTest()) {
   			List<sObject> scope = trac_TestUtils.setupMockExternalContactXrefs();
   			execute(BC, scope);
   		}

		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {

   		trac_QueryBuilder queryBuilder = new trac_QueryBuilder();
        queryBuilder.setObject('Contact');
        queryBuilder.setFields(new List<String>{'Banner__c', 'Customer_Number__c'});
        queryBuilder.setLogicalOperator(trac_QueryBuilder.OR_OPERATOR);

		Map<String, String> newUniqueKeyByOldUniqueKey = new Map<String, String>();
		List<Long> newCustomerNumbers = new List<Long>();

		//Create map of keys to connect new and old contacts by brand and customer number
		//Create dynamic query to get contacts to be merged
		for(SObject s : scope) {
			Ext_Contact_Xref__x extContactXref = (Ext_Contact_Xref__x) s;
			
			String oldUniqueKey = createUniqueKey(trac_SyncFromExternalObject.BRAND_MAP.get(extContactXref.brand_prefix__c), extContactXref.old_customer_no__c);
			String newUniqueKey = createUniqueKey(trac_SyncFromExternalObject.BRAND_MAP.get(extContactXref.brand_prefix__c), extContactXref.new_customer_no__c);

			newUniqueKeyByOldUniqueKey.put(oldUniqueKey, newUniqueKey);
			newCustomerNumbers.add(extContactXref.new_customer_no__c.longValue());

			trac_QueryBuilder.Operator operator = new trac_QueryBuilder.Operator();
	        operator.setLogicalOperator(trac_QueryBuilder.AND_OPERATOR);
	        operator.addCondition(new trac_QueryBuilder.Condition('Banner__c', trac_QueryBuilder.EQUALS_OPERATOR, trac_SyncFromExternalObject.BRAND_MAP.get(extContactXref.brand_prefix__c)));
	        operator.addCondition(new trac_QueryBuilder.Condition('Customer_Number__c', trac_QueryBuilder.EQUALS_OPERATOR, String.valueOf(extContactXref.old_customer_no__c)));
	        queryBuilder.baseOperator.addOperator(operator);
		}

		String queryString = queryBuilder.build();
		List<Contact> oldContacts = Database.query(queryString);

		List<Ext_Contact__x> externalContacts = trac_ExternalContactModel.findAllByCustomerNumbers(newCustomerNumbers);

		List<Ext_Contact__x> filteredExternalContacts = new List<Ext_Contact__x>();
		for (Ext_Contact__x externalContact : externalContacts) {
			if (externalContact.email_address__c != null && externalContact.brand_prefix__c != null) {
				filteredExternalContacts.add(externalContact);
			}
		}

		List<String> customerNumbers = longValuesToString(newCustomerNumbers);
		List<Contact> contacts = trac_ContactModel.findAllByCustomerNumbers(customerNumbers);

        Map<String, Contact> newContactByUniqueKey = new Map<String, Contact>();  
        for (Contact contact : contacts) {
        	if (contact.Banner__c != null && contact.Customer_Number__c != null) {
        		newContactByUniqueKey.put(createUniqueKey(contact.Banner__c, contact.Customer_Number__c), contact);
        	}
        }

        List<Contact> contactsToUpdate = new List<Contact>();
        
        //Get unique key from external object and get the new contact associated to it to sync with external objects
        for (Ext_Contact__x filteredExternalContact : filteredExternalContacts) {
        	String uniqueKey = createUniqueKey(trac_SyncFromExternalObject.BRAND_MAP.get(filteredExternalContact.brand_prefix__c), filteredExternalContact.customer_no__c);
        	if (newContactByUniqueKey.containsKey(uniqueKey)) {
        		Contact contact = newContactByUniqueKey.get(uniqueKey);
        		Id accountId = (contact.AccountId != null) ? contact.AccountId : findAccountId(contact, contacts);
        		contactsToUpdate.add(trac_SyncFromExternalObject.mapContact(contact, filteredExternalContact, accountId, false));
        	}
		}

		Map<Id, Contact> newContactsByOldContactId = new Map<Id, Contact>();
		Set<Id> oldContactIds = new Set<Id>();
		List<Contact> contactsToDelete = new List<Contact>();
		
		//Store new contacts against the old contacts Id to be used to update Contact lookup on existing Cases with old Contacts
		//Populate list of contacts that will be deleted after update
		for (Contact oldContact : oldContacts) {
			oldContactIds.add(oldContact.Id);
			String uniqueKey = createUniqueKey(oldContact.Banner__c, oldContact.Customer_Number__c);

			if (newUniqueKeyByOldUniqueKey.containsKey(uniqueKey)) {
				Contact newContact = newContactByUniqueKey.get(newUniqueKeyByOldUniqueKey.get(uniqueKey));
				if (newContact != null) {
					newContactsByOldContactId.put(oldContact.Id, newContact);
					contactsToDelete.add(oldContact);
				}
			}
		}

		List<Case> casesToMerge = trac_CaseModel.findCasesByContactIds(oldContactIds);

		List<Case> casesToUpdate = new List<Case>();
		if (casesToMerge.size() > 0) {
			for (Case c : casesToMerge) {
				if (newContactsByOldContactId.containsKey(c.ContactId)) {
					Contact newContact = newContactsByOldContactId.get(c.ContactId);
					c.ContactId = newContact.Id;
					casesToUpdate.add(c);
				}
			}
		}

		if (casesToUpdate.size() > 0) {
			update casesToUpdate;
		}

		if (contactsToDelete.size() > 0) {
			delete contactsToDelete;
		}

		if (contactsToUpdate.size() > 0) {
			update contactsToUpdate;
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	/**
	 *  @description Create a new unique key from the external contact xref record
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-18
	 */
	public static String createUniqueKey(String brandPrefix, Decimal customerNo) {
		return brandPrefix + '_' + customerNo.longValue();
	}

	/**
	 *  @description Create a new unique key from the contact record
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-03-03
	 */
	public static String createUniqueKey(String brandPrefix, String customerNo) {
		return brandPrefix + '_' + customerNo;
	}

	/**
	 *  @description Finds contact with same email and gets associated account Id
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-03-03
	 */
	public static Id findAccountId(Contact unlinkedContact, List<Contact> contacts) {
        Id accountId;
        for(Contact contact : contacts) {
            if(contact.Email != null && unlinkedContact.Email == contact.Email && contact.AccountId != null) {
                accountId = contact.AccountId;
                break;
            }
        }
        return accountId;
    }

    private List<String> longValuesToString(List<Long> longValues) {
    	List<String> stringValues = new List<String>();
    	for(Long longValue : longValues) {
    		stringValues.add(String.valueOf(longValue));
    	}

    	return stringValues;
    }
}