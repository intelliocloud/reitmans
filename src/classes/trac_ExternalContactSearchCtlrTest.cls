/**
 *  @description Unit tests for trac_ExternalContactSearchCtlr
 *  @author 	 Graham Barnard, Traction on Demand.
 *  @date        2016-02-08
 */
@isTest
private class trac_ExternalContactSearchCtlrTest {

	/**
	 *  @description Test searching with no records
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-08
	 */
	@isTest static void testWithNoRecords() {
		trac_ExternalContactSearchCtlr controller = new trac_ExternalContactSearchCtlr();
		System.assertEquals(null,controller.searchString, 'Search string is null at this point');

		Test.startTest();
		controller.searchString = 'test@email.com';
		controller.searchAll();
		Test.stopTest();

		System.assertEquals(0, controller.cases.size(), 'Cases should have no records');
		System.assertEquals(0, controller.leads.size(), 'Leads should have no records');
		System.assertEquals(0, controller.accounts.size(), 'Accounts should have no records');
		System.assertEquals(0, controller.contacts.size(), 'Contacts should have no records');
		System.assertEquals(0, controller.externalContacts.size(), 'External Contacts should have no records');
	}

	/**
	 *  @description Test searching with salesforce records
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-08
	 */
	@isTest static void testAccountSearching() {
		Account account = trac_TestUtils.setupAccount();
		account.Email_Address__c = 'test@email.com';
		insert account;

		trac_ExternalContactSearchCtlr controller = new trac_ExternalContactSearchCtlr();

		Test.startTest();

		List<Id> fixedSearchResults= new List<Id>();
		fixedSearchResults.add(account.Id);
       	Test.setFixedSearchResults(fixedSearchResults);

		controller.searchString = 'test@email.com';
		controller.searchAll();
		Test.stopTest();

		System.assertEquals(0, controller.cases.size(), 'Cases should have no records');
		System.assertEquals(0, controller.leads.size(), 'Leads should have no records');
		System.assertEquals(1, controller.accounts.size(), 'Accounts should have 1 record');
		System.assertEquals(0, controller.contacts.size(), 'Contacts should have no records');
		System.assertEquals(0, controller.externalContacts.size(), 'External Contacts should have no records');
	}

	/**
	 *  @description Test searching with salesforce records
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-08
	 */
	@isTest static void testContactSearching() {
		Account account = trac_TestUtils.setupAccount();
		account.Email_Address__c = 'test@email.com';
		insert account;

		Contact contact = trac_TestUtils.setupContact(account.Id);
		contact.Email = 'test@email.com';
		insert contact;

		trac_ExternalContactSearchCtlr controller = new trac_ExternalContactSearchCtlr();

		Test.startTest();

		List<Id> fixedSearchResults= new List<Id>();
		fixedSearchResults.add(account.Id);
		fixedSearchResults.add(contact.Id);
       	Test.setFixedSearchResults(fixedSearchResults);

		controller.searchString = 'test@email.com';
		controller.searchAll();
		Test.stopTest();

		System.assertEquals(0, controller.cases.size(), 'Cases should have no records');
		System.assertEquals(0, controller.leads.size(), 'Leads should have no records');
		System.assertEquals(1, controller.accounts.size(), 'Accounts should have 1 record');
		System.assertEquals(1, controller.contacts.size(), 'Contacts should have 1 record');
		System.assertEquals(0, controller.externalContacts.size(), 'External Contacts should have no records');
	}

	/**
	 *  @description Test searching with salesforce records
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-08
	 */
	@isTest static void testCaseSearching() {
		Account account = trac_TestUtils.setupAccount();
		account.Email_Address__c = 'test@email.com';
		insert account;

		Contact contact = trac_TestUtils.setupContact(account.Id);
		contact.Email = 'test@email.com';
		insert contact;

		Case c = trac_TestUtils.setupCase(contact.Id);
		insert c;

		trac_ExternalContactSearchCtlr controller = new trac_ExternalContactSearchCtlr();

		Test.startTest();

		List<Id> fixedSearchResults= new List<Id>();
		fixedSearchResults.add(account.Id);
		fixedSearchResults.add(contact.Id);
		fixedSearchResults.add(c.Id);
       	Test.setFixedSearchResults(fixedSearchResults);

		controller.searchString = 'test@email.com';
		controller.searchAll();
		Test.stopTest();

		System.assertEquals(1, controller.cases.size(), 'Cases should have 1 record');
		System.assertEquals(0, controller.leads.size(), 'Leads should have no records');
		System.assertEquals(1, controller.accounts.size(), 'Accounts should have 1 record');
		System.assertEquals(1, controller.contacts.size(), 'Contacts should have 1 record');
		System.assertEquals(0, controller.externalContacts.size(), 'External Contacts should have no records');
	}

	/**
	 *  @description Test no records are returned when search term is too small
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-08
	 */
	@isTest static void testInvalidSearchTerm() {
		Account account = trac_TestUtils.setupAccount();
		account.Email_Address__c = 'test@email.com';
		insert account;

		Contact contact = trac_TestUtils.setupContact(account.Id);
		contact.Email = 'test@email.com';
		insert contact;

		trac_ExternalContactSearchCtlr controller = new trac_ExternalContactSearchCtlr();

		Test.startTest();

		List<Id> fixedSearchResults= new List<Id>();
		fixedSearchResults.add(account.Id);
		fixedSearchResults.add(contact.Id);
       	Test.setFixedSearchResults(fixedSearchResults);

		controller.searchString = 'te';
		controller.searchAll();
		Test.stopTest();

		System.assertEquals(0, controller.cases.size(), 'Cases should have no records');
		System.assertEquals(0, controller.leads.size(), 'Leads should have no records');
		System.assertEquals(0, controller.accounts.size(), 'Accounts should have no records');
		System.assertEquals(0, controller.contacts.size(), 'Contacts should have no records');
		System.assertEquals(0, controller.externalContacts.size(), 'External Contacts should have no records');	}


	/**
	 *  @description Test searching with mocked external records by email
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-08
	 */
	@isTest static void testExternalContactSearchingByEmail() {
		trac_ExternalContactSearchCtlr controller = new trac_ExternalContactSearchCtlr();

		Test.startTest();
		Ext_Contact__x externalContact = trac_TestUtils.setupCustomer();

		trac_ExternalContactModel.mockExternalContacts.add(externalContact);
		controller.searchString = 'test@email.com';
		controller.searchAll();
		Test.stopTest();

		System.assertEquals(0, controller.cases.size(), 'Cases should have no records');
		System.assertEquals(0, controller.leads.size(), 'Leads should have no records');
		System.assertEquals(0, controller.accounts.size(), 'Accounts should have no records');
		System.assertEquals(0, controller.contacts.size(), 'Contacts should have no records');
		System.assertEquals(1, controller.externalContacts.size(), 'External Contacts should have 1 record');
	}

	/**
	 *  @description Test searching with mocked external records by phone
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-08
	 */
	@isTest static void testExternalContactSearchingByPhone() {
		trac_ExternalContactSearchCtlr controller = new trac_ExternalContactSearchCtlr();

		Test.startTest();
		Ext_Contact__x externalContact = trac_TestUtils.setupCustomer();

		trac_ExternalContactModel.mockExternalContacts.add(externalContact);
		controller.searchString = '514-445-4454';
		controller.searchAll();
		Test.stopTest();

		System.assertEquals(0, controller.cases.size(), 'Cases should have no records');
		System.assertEquals(0, controller.leads.size(), 'Leads should have no records');
		System.assertEquals(0, controller.accounts.size(), 'Accounts should have no records');
		System.assertEquals(0, controller.contacts.size(), 'Contacts should have no records');
		System.assertEquals(1, controller.externalContacts.size(), 'External Contacts should have 1 record');
	}

	/**
	 *  @description Test searching with mocked external records by name
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-08
	 */
	@isTest static void testExternalContactSearchingByName() {
		trac_ExternalContactSearchCtlr controller = new trac_ExternalContactSearchCtlr();

		Test.startTest();
		Ext_Contact__x externalContact = trac_TestUtils.setupCustomer();

		trac_ExternalContactModel.mockExternalContacts.add(externalContact);
		controller.searchString = 'test name';
		controller.searchAll();
		Test.stopTest();

		System.assertEquals(0, controller.cases.size(), 'Cases should have no records');
		System.assertEquals(0, controller.leads.size(), 'Leads should have no records');
		System.assertEquals(0, controller.accounts.size(), 'Accounts should have no records');
		System.assertEquals(0, controller.contacts.size(), 'Contacts should have no records');
		System.assertEquals(1, controller.externalContacts.size(), 'External Contacts should have no records');
	}
}