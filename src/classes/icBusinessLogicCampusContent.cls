public class icBusinessLogicCampusContent implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Junction_Content_Category__c> getCampusContentJunctionByCategory(String categoryId);
		List<Junction_Content_Category__c> getCampusWelcomeContentJunctionByCategory(String categoryId);
		Map<String, List<icDTOContent>> getMapCampusContentFromList(List<Junction_Content_Category__c> junctions);
		Map<String, List<icDTOContent>> getMapCampusWelcomeContentFromList(List<Junction_Content_Category__c> junctions);
	}

	public class Impl implements IClass {

		icRepositoryCampusContent.IClass repoContent = (icRepositoryCampusContent.IClass) icObjectFactory.GetSingletonInstance('icRepositoryCampusContent');
		
		public List<Junction_Content_Category__c> getCampusContentJunctionByCategory(String categoryId) {
			String strLanguage = 'English';
			if(UserInfo.getLanguage() == 'FR') {
				strLanguage = 'French';
			}					
			return repoContent.getCampusContentJunctionByCategoryAndLanguage(categoryId, strLanguage);
		}

		public List<Junction_Content_Category__c> getCampusWelcomeContentJunctionByCategory(String categoryId) {
			System.debug('getCampusWelcomeContentJunctionByCategory for : ' + categoryId);			
			return repoContent.getCampusWelcomeContentJunctionByCategory(categoryId);
		}

		public Map<String, List<icDTOContent>> getMapCampusContentFromList(List<Junction_Content_Category__c> junctions) {
			Map<String, List<icDTOContent>> returnMap = new Map<String, List<icDTOContent>>();

			String language = UserInfo.getLanguage();
			String mapKey;
			for(Junction_Content_Category__c junction : junctions) {
				String thisRecordTypeName = junction.Campus_Content__r.RecordType.Name;
				List<icDTOContent> thisTypeContentList = returnMap.get(junction.Campus_Content__r.Type__c);
				if(thisTypeContentList == null) {
					thisTypeContentList = new List<icDTOContent>();
				}
				thisTypeContentList.add(mapSFToDTO(junction, language, thisRecordTypeName));
				returnMap.put(junction.Campus_Content__r.Type__c, thisTypeContentList);
			}
			
			return returnMap;
		}

		public Map<String, List<icDTOContent>> getMapCampusWelcomeContentFromList(List<Junction_Content_Category__c> junctions) {
			Map<String, List<icDTOContent>> returnMap = new Map<String, List<icDTOContent>>();

			String language = UserInfo.getLanguage();
			String mapKey;
			for(Junction_Content_Category__c junction : junctions) {
				String thisRecordTypeName = junction.Campus_Content__r.RecordType.Name;
				String thisSortOrder = junction.Campus_Content__r.Sort_Order__c;

				List<icDTOContent> thisTypeContentList = returnMap.get(thisSortOrder);
				if(thisTypeContentList == null) {
					thisTypeContentList = new List<icDTOContent>();
				}
				thisTypeContentList.add(mapSFToDTO(junction, language, thisRecordTypeName));
				returnMap.put(thisSortOrder, thisTypeContentList);
			}
			
			return returnMap;
		}

		private icDTOContent mapSFToDTO(Junction_Content_Category__c junction, String language, String thisRecordTypeName) {
			icDTOContent returnDTO = new icDTOContent();
			returnDTO.id = junction.Campus_Content__c;
			returnDTO.recordType = thisRecordTypeName;
			returnDTO.name = junction.Campus_Content__r.Name;
			returnDTO.imageLocation = junction.Campus_Content__r.Image_Location__c;
			returnDTO.title = junction.Campus_Content__r.Title__c;
			returnDTO.contentType = junction.Campus_Content__r.Type__c;
			returnDTO.shortDescription = junction.Campus_Content__r.Short_Description__c;
			returnDTO.description = junction.Campus_Content__r.Description__c;
			returnDTO.cost = junction.Campus_Content__r.Cost__c;
			returnDTO.eligibility = junction.Campus_Content__r.Eligibility__c;
			returnDTO.howToRegister = junction.Campus_Content__r.How_to_Register__c;
			returnDTO.location = junction.Campus_Content__r.Location__c;
			returnDTO.partners = junction.Campus_Content__r.Partners__c;
			returnDTO.syllabus = junction.Campus_Content__r.Syllabus__c;
			returnDTO.targetAudience = junction.Campus_Content__r.Target_Audience__c;
			returnDTO.timeCommitment = junction.Campus_Content__r.Time_Commitment__c;			
			returnDTO.video = junction.Campus_Content__r.Video__c;
			returnDTO.article = junction.Campus_Content__r.Article__c;

			if(thisRecordTypeName == 'Welcome') {
				if(language == 'FR') {
					returnDTO.title = junction.Campus_Content__r.Content_Heading_FR__c;
					returnDTO.subTitle = junction.Campus_Content__r.Content_Sub_Heading_FR__c;
					returnDTO.description = junction.Campus_Content__r.Content_Details_FR__c;
				} else {
					returnDTO.title = junction.Campus_Content__r.Content_Heading_EN__c;
					returnDTO.subTitle = junction.Campus_Content__r.Content_Sub_Heading_EN__c;
					returnDTO.description = junction.Campus_Content__r.Content_Details_EN__c;
				}
			}
			if(thisRecordTypeName == 'Faculty') {
				returnDTO.name = junction.Campus_Content__r.Faculty_Name__c;
				if(language == 'FR') {
					returnDTO.title = junction.Campus_Content__r.Faculty_Title_FR__c;
					returnDTO.description = junction.Campus_Content__r.Faculty_Detail_FR__c;
				} else {
					returnDTO.title = junction.Campus_Content__r.Faculty_Title_EN__c;
					returnDTO.description = junction.Campus_Content__r.Faculty_Detail_EN__c;
				}
			}

			return returnDTO;
		}
	}
}