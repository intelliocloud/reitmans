/**
 *  @description Tests for trac_InstagramCandidCarouselCtrl
 *  @author 	 Jeremy Horan, Traction on Demand.
 *  @date        2016-08-04
 */
@isTest
private class trac_InstagramCandidCarouselCtrlTest {
	
	@isTest static void test_CandidIdSet() {
		PageReference pageRef = Page.trac_InstagramCandidCarousel;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('candidId', 'abc12345');
        ApexPages.currentPage().getParameters().put('candidTag', 'abc12345');

		trac_InstagramCandidCarouselCtrl controller = new trac_InstagramCandidCarouselCtrl();
		System.assertEquals('abc12345',controller.candidId);
		System.assertEquals('abc12345',controller.candidTag);
	}
}