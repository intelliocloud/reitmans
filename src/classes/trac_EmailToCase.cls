/**
 *  @description Additional methods needed for Email to Case
 *  @author      Graham Barnard, Traction on Demand.
 *  @date        2016-02-09
 */
public with sharing class trac_EmailToCase {
    private final static String EMAIL_ORIGIN_STATUS = 'Email';
    private final static String EMAIL_DELIMITER = ';';

    private List<Case> cases;
    private List<EmailMessage> emailMessages;

    /**
     *  @description Constructor for Trigger new
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-09
     */
    public trac_EmailToCase(List<EmailMessage> unfilteredEmailMessages) {
        Set<Id> caseIds = new Set<Id>();

        emailMessages = new List<EmailMessage>();
        for(EmailMessage emailMessage : unfilteredEmailMessages) {
            if(trac_SObjectModel.isCase(emailMessage.ParentId)) {
                caseIds.add(emailMessage.ParentId);
                emailMessages.add(emailMessage);
            }
        }

        if(caseIds.size() == 0) return;

        cases = filterCases(trac_CaseModel.findAllByIds(caseIds));
    }

    /**
     *  @description Filtered the cases based on case type
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-09
     */
    private List<Case> filterCases(List<Case> unfilteredCases) {
        List<Case> filteredCases = new List<Case>();
        for(Case unfilteredCase : unfilteredCases) {
            if(unfilteredCase.Origin == EMAIL_ORIGIN_STATUS && unfilteredCase.Banner__c == null) {
                filteredCases.add(unfilteredCase);
            }
        }

        return filteredCases;
    }

    /**
     *  @description Update the banner based on the email message to field 
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-09
     */
    public List<Case> associateBannerToCases() {
        List<Case> casesToUpdate = new List<Case>();

        if(cases.size() == 0) return casesToUpdate; 

        Set<Id> caseIds = trac_SObjectModel.getIds(cases);
        Map<Id, String> caseToEmailAddress = new Map<Id, String>();

        for(EmailMessage emailMessage : emailMessages) {
            if(!caseToEmailAddress.containsKey(emailMessage.ParentId)) {
                caseToEmailAddress.put(emailMessage.ParentId, emailMessage.ToAddress);
            }
        }

        List<Banner_Email__c> bannerEmails = Banner_Email__c.getAll().values();
        Map<String, Banner_Email__c> emailToBanner = new Map<String, Banner_Email__c>();
        for(Banner_Email__c bannerEmail : bannerEmails) {
            emailToBanner.put(bannerEmail.Email__c, bannerEmail);
        }

        for(Case c : cases) {
            String emailAddressString = caseToEmailAddress.get(c.Id);
            emailAddressString = emailAddressString.replaceAll('\n', '');
            List<String> emailAddresses = emailAddressString.split(EMAIL_DELIMITER);
            for (String emailAddress : emailAddresses) {
                if(emailToBanner.containsKey(emailAddress)) {
                    Banner_Email__c bannerEmail = emailToBanner.get(emailAddress);
                    c.Banner__c = bannerEmail.Banner__c;
                    c.Language__c = bannerEmail.Language__c;
                    casesToUpdate.add(c);
                    break;
                }
            }
        }

        if(!casesToUpdate.isEmpty()) {
            update casesToUpdate;
        }

        return cases;
    }
}