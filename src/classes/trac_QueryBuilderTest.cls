/**
 *  @description Unit Tests for trac_QueryBuilder
 *  @author      Graham Barnard, Traction on Demand.
 *  @date        2016-02-18
 */
@isTest
private class trac_QueryBuilderTest {
    /**
     *  @description Test when the object name is not set
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-18
     */
    @isTest static void testEmptyObjectName() {
        Boolean exceptionThrown = false;

        trac_QueryBuilder queryBuilder = new trac_QueryBuilder();
        try {
            String resultString = queryBuilder.build();
        }
        catch(Exception e) {
            exceptionThrown = true;
        }

        System.assertEquals(true, exceptionThrown);
    }

    /**
     *  @description Test when the object name is set and nothing else
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-18
     */
    @isTest static void testValidBasicQuery() {
        trac_QueryBuilder queryBuilder = new trac_QueryBuilder();
        queryBuilder.setObject('Account');

        String resultString = queryBuilder.build();
        System.assertEquals('SELECT Id FROM Account', resultString);
    }

    /**
     *  @description Test when the object name is set
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-18
     */
    @isTest static void testValidSingleField() {
        trac_QueryBuilder queryBuilder = new trac_QueryBuilder();
        queryBuilder.setObject('Account');
        queryBuilder.setFields(new List<String>{'Name'});

        String resultString = queryBuilder.build();
        System.assertEquals('SELECT Name FROM Account', resultString);
    }

    /**
     *  @description Test when the object name is set
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-18
     */
    @isTest static void testValidMultipleFields() {
        trac_QueryBuilder queryBuilder = new trac_QueryBuilder();
        queryBuilder.setObject('Account');
        queryBuilder.setFields(new List<String>{'Id, Name'});

        String resultString = queryBuilder.build();
        System.assertEquals('SELECT Id, Name FROM Account', resultString);
    }

    /**
     *  @description Test when the object name is set
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-18
     */
    @isTest static void testValidLimit() {
        trac_QueryBuilder queryBuilder = new trac_QueryBuilder();
        queryBuilder.setObject('Account');
        queryBuilder.setLimit(100);

        String resultString = queryBuilder.build();
        System.assertEquals('SELECT Id FROM Account LIMIT 100', resultString);
    }

    /**
     *  @description Test when the object name is set
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-18
     */
    @isTest static void testSingleCondition() {
        trac_QueryBuilder queryBuilder = new trac_QueryBuilder();
        queryBuilder.setObject('Account');
        queryBuilder.baseOperator.addCondition(new trac_QueryBuilder.Condition('Name', trac_QueryBuilder.EQUALS_OPERATOR, 'Test'));

        String resultString = queryBuilder.build();
        System.assertEquals('SELECT Id FROM Account WHERE Name = \'Test\'', resultString);
    }

    /**
     *  @description Test when the object name is set
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-18
     */
    @isTest static void testInvalidMultipleCondition() {
        Boolean exceptionThrown = false;
        trac_QueryBuilder queryBuilder = new trac_QueryBuilder();
        queryBuilder.setObject('Account');
        queryBuilder.baseOperator.addCondition(new trac_QueryBuilder.Condition('Name', trac_QueryBuilder.EQUALS_OPERATOR, 'Test1'));
        queryBuilder.baseOperator.addCondition(new trac_QueryBuilder.Condition('Type', trac_QueryBuilder.NOT_EQUALS_OPERATOR, 'Test2'));
        queryBuilder.baseOperator.addCondition(new trac_QueryBuilder.Condition('Web', trac_QueryBuilder.LIKE_OPERATOR, 'Test3'));

        try {
            String resultString = queryBuilder.build();
        }
        catch(Exception e) {
            exceptionThrown = true;
        }

         System.assertEquals(true, exceptionThrown);
   }

    /**
     *  @description Test when the object name is set
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-18
     */
    @isTest static void testValidAndMultipleCondition() {
        trac_QueryBuilder queryBuilder = new trac_QueryBuilder();
        queryBuilder.setObject('Account');
        queryBuilder.setLogicalOperator(trac_QueryBuilder.AND_OPERATOR);
        queryBuilder.baseOperator.addCondition(new trac_QueryBuilder.Condition('Name', trac_QueryBuilder.EQUALS_OPERATOR, 'Test1'));
        queryBuilder.baseOperator.addCondition(new trac_QueryBuilder.Condition('Type', trac_QueryBuilder.NOT_EQUALS_OPERATOR, 'Test2'));
        queryBuilder.baseOperator.addCondition(new trac_QueryBuilder.Condition('Web', trac_QueryBuilder.EQUALS_OPERATOR, 'Test3'));

        String resultString = queryBuilder.build();
        System.assertEquals('SELECT Id FROM Account WHERE Name = \'Test1\' AND Type != \'Test2\' AND Web = \'Test3\'', resultString);
   }

    /**
     *  @description Test when the object name is set
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-18
     */
    @isTest static void testValidOrMultipleCondition() {
        trac_QueryBuilder queryBuilder = new trac_QueryBuilder();
        queryBuilder.setObject('Contact');
        queryBuilder.setLogicalOperator(trac_QueryBuilder.OR_OPERATOR);
        queryBuilder.baseOperator.addCondition(new trac_QueryBuilder.Condition('FirstName', trac_QueryBuilder.EQUALS_OPERATOR, 'First Name'));
        queryBuilder.baseOperator.addCondition(new trac_QueryBuilder.Condition('LastName', trac_QueryBuilder.NOT_EQUALS_OPERATOR, 'Last Name'));
        queryBuilder.baseOperator.addCondition(new trac_QueryBuilder.Condition('Email', trac_QueryBuilder.EQUALS_OPERATOR, 'email@test.com'));

        String resultString = queryBuilder.build();
        System.assertEquals('SELECT Id FROM Contact WHERE FirstName = \'First Name\' OR LastName != \'Last Name\' OR Email = \'email@test.com\'', resultString);
   }      

    /**
     *  @description Test when the object name is set
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-18
     */
    @isTest static void testLayeredCondition() {
        trac_QueryBuilder queryBuilder = new trac_QueryBuilder();
        queryBuilder.setObject('Contact');
        queryBuilder.setLogicalOperator(trac_QueryBuilder.OR_OPERATOR);

        trac_QueryBuilder.AndOperator operator1 = new trac_QueryBuilder.AndOperator();
        operator1.addCondition(new trac_QueryBuilder.Condition('FirstName', trac_QueryBuilder.EQUALS_OPERATOR, 'First Name 1'));
        operator1.addCondition(new trac_QueryBuilder.Condition('LastName', trac_QueryBuilder.EQUALS_OPERATOR, 'Last Name 1'));

        trac_QueryBuilder.AndOperator operator2 = new trac_QueryBuilder.AndOperator();
        operator2.addConditions(new List<trac_QueryBuilder.Condition>{new trac_QueryBuilder.Condition('FirstName', trac_QueryBuilder.EQUALS_OPERATOR, 'First Name 2')});
        operator2.addConditions(new List<trac_QueryBuilder.Condition>{new trac_QueryBuilder.Condition('LastName', trac_QueryBuilder.EQUALS_OPERATOR, 'Last Name 2')});

        queryBuilder.baseOperator.addOperator(operator1);
        queryBuilder.baseOperator.addOperators(new List<trac_QueryBuilder.Operator>{operator2});

        String resultString = queryBuilder.build();
        System.assertEquals('SELECT Id FROM Contact WHERE (FirstName = \'First Name 1\' AND LastName = \'Last Name 1\') OR (FirstName = \'First Name 2\' AND LastName = \'Last Name 2\')', resultString);
   }      
}