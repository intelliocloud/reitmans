/**
 *  @description Creates an account based on contact's email
 *  @author      Tanminder Rai, Traction on Demand.
 *  @date        2016-02-03
 */
public with sharing class trac_Account {

    private static Id accountCustomerServiceRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Service RCL').getRecordTypeId();
    private static Id contactCustomerServiceRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Service RCL').getRecordTypeId();

    /**
     *  @description Sync Contacts with Account on insert trigger
     *  @author      Tanminder Rai, Traction on Demand.
     *  @date        2016-02-03
     */    
    public static void syncContactsByEmail(List<Contact> newContacts) {
        List<Contact> filteredContacts = new List<Contact>();
        for(Contact contact: newContacts) {
            if(String.isBlank(contact.AccountId) && contact.RecordTypeId == contactCustomerServiceRecordTypeId) {
                filteredContacts.add(contact);
            }
        }
        syncAccounts(filteredContacts);
    }

    /**
     *  @description Sync Contacts with Account on update trigger
     *  @author      Tanminder Rai, Traction on Demand.
     *  @date        2016-02-25
     */
    public static void syncContactsByEmail(List<Contact> newContacts, Map<Id, Contact> oldMap) {
        List<Contact> filteredContacts = new List<Contact>();
        for(Contact contact : newContacts) {
            if(contact.Email != oldMap.get(contact.Id).Email && contact.RecordTypeId == contactCustomerServiceRecordTypeId) {
                if (!(contact.Email != null && oldMap.get(contact.Id).Email != null && contact.Email.toLowerCase() == oldMap.get(contact.Id).Email.toLowerCase())) {
                    filteredContacts.add(contact);
                }
            }
        }

        syncAccounts(filteredContacts);
    }

    /**
     *  @description Find or Create Account
     *  @author      Tanminder Rai, Traction on Demand.
     *  @date        2016-02-25
     */
    private static void syncAccounts(List<Contact> contacts) {
        if(contacts.isEmpty()) return;

        Set<String> contactEmails = new Set<String>();
        for(Contact contact : contacts) {
            if(contact.Email != null) {
                contactEmails.add(contact.Email);
            }
        }

        List<Account> accounts = trac_AccountModel.findByEmailAddresses(contactEmails);
        Map<String, Account> emailToAccountMap = getEmailToAccountMap(accounts);
        Map<String, Account> emailToNewAccountMap = new Map<String, Account>();
        List<Account> accountsToInsert = new List<Account>();
        List<Contact> contactsToUpdateAccId = new List<Contact>();

        for (Contact contact : contacts) {
            if(emailToAccountMap.containsKey(contact.Email) && contact.Email != null && !String.isBlank(contact.Email)) {
                contact.AccountId = emailToAccountMap.get(contact.Email).Id;
            }
            else {
                Account newAccount = createAccountFromContact(contact);
                accountsToInsert.add(newAccount);
                emailToNewAccountMap.put(contact.Email, newAccount);
                contactsToUpdateAccId.add(contact);
            }
        }

        if(!accountsToInsert.isEmpty()) {
            insert accountsToInsert;
        }

        for(Contact contact :contactsToUpdateAccId) {
            contact.AccountId = emailToNewAccountMap.get(contact.Email).Id;
        }
    }



    /**
     *  @description Creates a new account from contact
     *  @author      Tanminder Rai, Traction on Demand.
     *  @date        2016-02-03
     */
    private static Account createAccountFromContact(Contact contact) {

        String firstName =  String.isNotBlank(contact.FirstName) ? contact.FirstName : '';
        String lastName =  String.isNotBlank(contact.LastName) ? contact.LastName : '';

        return new Account(
            Name             = firstName + ' ' + lastName,
            First_Name__c    = contact.FirstName,
            Last_Name__c     = contact.LastName,
            Email_Address__c = contact.Email,
            Phone            = contact.Phone,
            RecordTypeId     = accountCustomerServiceRecordTypeId
        );
    }

    /**
     *  @description Creates an email to account map
     *  @author      Tanminder Rai, Traction on Demand.
     *  @date        2016-02-03
     */
    private static Map<String, Account> getEmailToAccountMap(List<Account> accounts) {
        Map<String, Account> emailToAcc = new Map<String, Account>();
        for(Account a :accounts) {
            emailToAcc.put(a.Email_Address__c, a);
        }
        return emailToAcc;
    }
}