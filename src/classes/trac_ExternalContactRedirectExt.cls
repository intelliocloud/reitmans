/**
 *  @description Controller to override the external object details page
 *  @author      Graham Barnard, Traction on Demand.
 *  @date        2015-11-30
 */
public with sharing class trac_ExternalContactRedirectExt {
    public Ext_Contact__x externalContact { get; set; }


    private final static Map<String, String> GENDER_MAP = new Map<String, String>{
        'M' => 'Male',
        'F' => 'Female',
        'U' => 'Unknown'
    };

    private final static String DEFAULT_GENDER = 'Unknown';
    private final static Integer EMAIL_OPT_IN_VALUE = 1;

    /**
     *  @description Constructor for Standard Controller
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-11-30
     */
    public trac_ExternalContactRedirectExt(ApexPages.StandardController stdController) {
        externalContact = (Ext_Contact__x) stdController.getRecord();
        
        Ext_Contact__x searchedExternalContact = trac_ExternalContactModel.findById(externalContact.Id);
        if(searchedExternalContact != null) {
            externalContact = searchedExternalContact;
        }
    }

    /**
     *  @description Constructor for Standard Controller
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-11-30
     */
    public trac_ExternalContactRedirectExt() {
        String customerNoParam = ApexPages.currentPage().getParameters().get('customer_no');
        Long customerNo = (customerNoParam != null) ? Long.valueOf(customerNoParam) : null;
        
        String brandPrefix = ApexPages.currentPage().getParameters().get('brand_prefix');

        if(customerNo != null && String.isNotBlank(brandPrefix)) {
            externalContact = trac_ExternalContactModel.findByCustomerNoAndBrand(customerNo, brandPrefix);
        }
    }

    /**
     *  @description Convert the External Object to native Contact and Account Records
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-11-30
     */
    public PageReference convertExternalObject() {
        if(externalContact == null) return null;

        Contact brandContact;
        Id accountId;
        
        if (!String.isBlank(externalContact.email_address__c)) {
            List<Contact> contacts = trac_ContactModel.findAllByEmailAddress(externalContact.email_address__c);
            brandContact = getBrandContactByBanner(contacts, externalContact.brand_prefix__c);
            accountId = (brandContact.AccountId != null) ? brandContact.AccountId : findAccountId(contacts);
        } else {
            if (trac_SyncFromExternalObject.BRAND_MAP.containsKey(externalContact.brand_prefix__c)) {
                String banner = trac_SyncFromExternalObject.BRAND_MAP.get(externalContact.brand_prefix__c);
                brandContact = trac_ContactModel.findByCustomerNumberAndBanner(String.valueOf(externalContact.customer_no__c), banner);
                if (brandContact != null) {
                    accountId = (brandContact.AccountId != null) ? brandContact.AccountId : null;
                }
            }
            if (brandContact == null) {
                brandContact = new Contact();
            }
        }

        trac_SyncFromExternalObject.mapContact(brandContact, externalContact, accountId, true);

        return new PageReference('/' + brandContact.Id);
    }

    /**
     *  @description Find if any of the contacts have an account
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-17
     */
     private Id findAccountId(List<Contact> contacts) {
        Id accountId;
        for(Contact contact : contacts) {
            if(contact.AccountId != null) {
                accountId = contact.AccountId;
                break;
            }
        }
        return accountId;
     }

    /**
     *  @description Find the Contact with matching banner
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-10
     */
    private Contact getBrandContactByBanner(List<Contact> contacts, String banner) {
        banner = trac_SyncFromExternalObject.BRAND_MAP.get(banner);

        Contact brandContact = new Contact();
        for(Contact contact : contacts) {
            if(contact.Banner__c == banner) {
                brandContact = contact;
                break;
            }
        }

        return brandContact;
    }
}