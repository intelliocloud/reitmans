public class icUtilRecordType {
	
	public static Map<String, Map<String, RecordType>> RECORDTYPESBYOBJECT {
		get {
			if(RECORDTYPESBYOBJECT == null) {
				RECORDTYPESBYOBJECT = new Map<String, Map<String, RecordType>>();
			}
			return RECORDTYPESBYOBJECT;
		}

		set;
	}

	public static List<RecordType> getRecordTypeByObject(String objectName) {
		Map<String, RecordType> mapRecordTypesByName = RECORDTYPESBYOBJECT.get(objectName); 
		
		if(mapRecordTypesByName == null) {
			mapRecordTypesByName = new Map<String, RecordType>();
			List<RecordType> thisObjectsRecordType = [SELECT Id, Name, SobjectType FROM RecordType WHERE SObjectType = :objectName ORDER BY Name];
			for(RecordType thisRecordType : thisObjectsRecordType) {
				mapRecordTypesByName.put(thisRecordType.Name, thisRecordType);
			}
			RECORDTYPESBYOBJECT.put(objectName, mapRecordTypesByName);
		}

		return mapRecordTypesByName.values();
	}

	public static RecordType getRecordTypeByObjectAndId(String objectName, String recordTypeId) {
		RecordType returtnRecordType;

		Boolean queryRecordType = false;
		Map<String, RecordType> mapRecordTypesById = RECORDTYPESBYOBJECT.get(objectName);
		if(mapRecordTypesById == null) {
			mapRecordTypesById = new Map<String, RecordType>();
			queryRecordType = true;
		} else {
			returtnRecordType = mapRecordTypesById.get(recordTypeId);
			if(returtnRecordType == null) {
				queryRecordType = true;
			}
		}

		if(queryRecordType) {
			List<RecordType> thisObjectsRecordType = [SELECT Id, Name, SobjectType FROM RecordType WHERE SObjectType = :objectName AND Id = :recordTypeId];
			for(RecordType thisRecordType : thisObjectsRecordType) {
				returtnRecordType = thisRecordType;
				mapRecordTypesById.put(thisRecordType.Id, thisRecordType);
			}
			RECORDTYPESBYOBJECT.put(objectName, mapRecordTypesById);
		}
		
		return returtnRecordType;
	}

	public static RecordType getRecordTypeByObjectAndName(String objectName, String recordTypeName) {
		RecordType returtnRecordType;

		Boolean queryRecordType = false;
		Map<String, RecordType> mapRecordTypesByName = RECORDTYPESBYOBJECT.get(objectName);
		if(mapRecordTypesByName == null) {
			mapRecordTypesByName = new Map<String, RecordType>();
			queryRecordType = true;
		} else {
			returtnRecordType = mapRecordTypesByName.get(recordTypeName);
			if(returtnRecordType == null) {
				queryRecordType = true;
			}
		}

		if(queryRecordType) {
			List<RecordType> thisObjectsRecordType = [SELECT Id, Name, SobjectType FROM RecordType WHERE SObjectType = :objectName AND Name = :recordTypeName];
			for(RecordType thisRecordType : thisObjectsRecordType) {
				returtnRecordType = thisRecordType;
				mapRecordTypesByName.put(thisRecordType.Name, thisRecordType);
			}
			RECORDTYPESBYOBJECT.put(objectName, mapRecordTypesByName);
		}
		
		return returtnRecordType;
	}
}