@isTest
private class trac_ExternalContactUpdateTest {
	
	private static Account account;

	static {
		account = trac_TestUtils.setupAccount();
		insert account;		
	}

	@isTest static void test_ContactUpdateBatch() {

		Ext_Contact__x externalContact = new Ext_Contact__x(
			brand_prefix__c = 'RE',
			customer_no__c = 1345678901,
			email_address__c = 'test@email.com',
			first_name__c = 'test first name',
			last_name__c = 'test last name',
			customer_id__c = 134567891,
			telephone_no__c = '514-111-1111',
			mobile_phone_no__c = '514-222-2222',
			gender__c = 'F',
			language_code__c = 'eng',
			birth_date__c = DateTime.now(),
			membership_type_code__c = 'abcd',
			email_opt_in__c = 1,
			email_opt_in_date__c = DateTime.now(),
			create_source__c = 'source',
			segmentation_flag_a__c = true,
			segmentation_flag_b__c = true,
			segmentation_flag_c__c = true,
			segmentation_flag_d__c = true,
			segmentation_flag_e__c = true,
			segmentation_flag_f__c = true,
			last_updated__c = Datetime.now().addMinutes(-30)
		);

		Contact contact = trac_TestUtils.setupContact(account.Id);
		contact.Customer_Number__c = String.valueOf(externalContact.customer_no__c);
		contact.Customer_Id__c = 134567892;
		contact.Banner__c = trac_SyncFromExternalObject.BRAND_MAP.get(externalContact.brand_prefix__c);
		contact.LastName = 'test last name';
		insert contact;

		Test.startTest();
		String dateFormat = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
		DateTime endDate = DateTime.now();
		DateTime startDate = endDate.addHours(-1).addMinutes(-10);
		String endDateString = endDate.format(dateFormat);
		String startDateString = startDate.format(dateFormat);

		trac_ExternalContactUpdate.mockExternalContacts.add(externalContact);
		trac_ExternalContactUpdate.runContactUpdate(startDateString, endDateString);
		Test.stopTest();

		Contact queriedcontact = [SELECT Id, FirstName FROM Contact WHERE Id =: contact.Id];
		System.assertEquals(externalContact.first_name__c.toUpperCase(), queriedContact.FirstName.toUpperCase());
	}

	@isTest static void test_ManualScheduling() {
		System.assertNotEquals(null, trac_ExternalContactUpdateScheduler.scheduleHourly(trac_TestUtils.generateRandomString(10)));
	}	
}