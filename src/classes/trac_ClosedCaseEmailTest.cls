/**
 * trac_CaseEmailMessage Test
 * @Date: 5/19/2016
 * @auther: Tanminder Rai
 */
@isTest
private class trac_ClosedCaseEmailTest {
    
    /**
     * Test that when a email message is sent to a closed case
     * a new case is a created and email message is re-parented and an email notifcation is sent out
     */
    @isTest static void test_closed_case() {

        Case testCase = trac_TestUtils.createCases(1)[0];
        testCase.status = 'Closed';
        testCase.Sub_Category__c = 'Accessories';
        insert testCase;

        Contact cont = new Contact(LastName='TestLastName', Email='testEmail@tod.com');
        insert cont;

        List<EmailMessage> testMsg = trac_TestUtils.createEmailMessages(1);
        testMsg[0].ParentId = testCase.Id;

        Test.startTest();
        Integer emailbefore = Limits.getEmailInvocations();
        insert testMsg;
        System.assertNotEquals(emailbefore,Limits.getEmailInvocations(),'should have decreased');
        Test.stopTest();

        List<Case> resultCase = [SELECT Id FROM Case];
        List<EmailMessage> resultMsg= [SELECT Id, ParentId FROM EmailMessage WHERE Id =:testMsg[0].Id];

        System.assertEquals(2, resultCase.size(), 'A new case should be created.');
        System.assertNotEquals(resultMsg[0].ParentId, testCase.Id, 'The Email message should be re-parented to the new case');

        List<Case> newResultCase = [SELECT Id, ParentId, Sub_Category__c FROM Case WHERE Status='Assigned'];
        System.assertEquals(testCase.Id, newResultCase[0].ParentId);        
        System.assertNotEquals(null, newResultCase[0].Sub_Category__c);        

    }

    /**
     * Test that when a email message is sent to a new case
     * no new case is created and no email notification is sent out
     */
    @isTest static void test_new_case() {

        Case testCase = trac_TestUtils.createCases(1)[0];
        testCase.status = 'New';
        insert testCase;

        List<EmailMessage> testMsg = trac_TestUtils.createEmailMessages(1);
        testMsg[0].ParentId = testCase.Id;

        Test.startTest();
        insert testMsg;
        Test.stopTest();

        List<Case> resultCase = [SELECT Id FROM Case];
        List<EmailMessage> resultMsg= [SELECT Id, ParentId FROM EmailMessage WHERE Id =:testMsg[0].Id];

        System.assertEquals(resultCase.size(), 1, 'A new case should not be created.');
        System.assertEquals(resultMsg[0].ParentId, testCase.Id, 'The Email message should have the same parent');
    }
}