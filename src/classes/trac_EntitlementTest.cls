/**
 *  @description Unit tests for trac_Entitlement
 *  @author 	 Graham Barnard, Traction on Demand.
 *  @date        2016-02-08
 */
@isTest
private class trac_EntitlementTest {
    /**
     *  @description Test Converting an External Object with no data
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-10
     */
    @isTest static void testInvalidAccountEntitlementProcess() {
    	trac_Entitlement.ENTITLEMENT_NAME = 'invalid';

    	Account account = trac_TestUtils.setupAccount();
    	account.Name = 'Test';
    	insert account;

    	List<Entitlement> entitlements = [SELECT Id, Name FROM Entitlement];
    	System.assertEquals(0, entitlements.size(), 'No Entitlements should be created');
    }

    /**
     *  @description Test Converting an External Object with no data
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-10
     */
    @isTest static void testValidAccountEntitlementProcess() {
    	Account account = trac_TestUtils.setupAccount();
    	account.Name = 'Test';
    	insert account;

    	List<Entitlement> entitlements = [SELECT Id, Name FROM Entitlement];
    	System.assertEquals(1, entitlements.size(), 'An entitlement should be created');

    	System.assertEquals('Test Default' ,entitlements[0].Name, 'Name is equal to Account Name + Entitlement Name');
    }

    /**
     *  @description Test Converting an External Object with no data
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-10
     */
    @isTest static void testMissingAccountForCaseEntitlementProcess() {
        Case testCase = trac_TestUtils.setupCase(null);
        insert testCase;

        List<Case> cases = [SELECT Id, EntitlementId FROM Case];
        System.assertEquals(1, cases.size());
        System.assertEquals(null, cases[0].EntitlementId);
    }

    /**
     *  @description Test Converting an External Object with no data
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-10
     */
    @isTest static void testValidCaseEntitlementProcess() {
        Account account = trac_TestUtils.setupAccount();
        account.Name = 'Test';
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        insert contact;

        Test.startTest();
        Case testCase = trac_TestUtils.setupCase(contact.Id);
        insert testCase;
        Test.stopTest();

        List<Case> cases = [SELECT Id, EntitlementId FROM Case];
        System.assertEquals(1, cases.size());

        System.assertNotEquals(null, case.EntitlementId);
    }
}