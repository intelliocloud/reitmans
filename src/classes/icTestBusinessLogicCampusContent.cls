@isTest
public with sharing class icTestBusinessLogicCampusContent {

	static void initTest() {
		icTestCampusObjectData.createCampusDataSet();
	}

	@future
	static void futureInitTest()
	{
		icTestCampusObjectData.createCampusDataSet();
	}

	static testMethod void test_getCampusContentJunctionByCategory_FR() {
		User newUser = icTestCampusObjectData.buildUser();
		newUser.LocaleSidKey = 'fr_CA';
		newUser.LanguageLocaleKey = 'FR';
		insert newUser;

		System.runAs(newUser) {
			Test.startTest();
			System.debug('test_getCampusContentJunctionByCategory_FR getLanguage() = ' + UserInfo.getLanguage());
			futureInitTest();
			Test.stopTest();
			icBusinessLogicCampusContent.IClass repo = (icBusinessLogicCampusContent.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicCampusContent');
			List<Junction_Content_Category__c> result = repo.getCampusContentJunctionByCategory(icTestCampusObjectData.contentCategory);			
		}
	}

	static testMethod void test_getCampusContentJunctionByCategory_EN() {
		User newUser = icTestCampusObjectData.buildUser();
		newUser.LocaleSidKey = 'en_US';
		newUser.LanguageLocaleKey = 'en_US';
		insert newUser;

		System.runAs(newUser) {
			Test.startTest();
			System.debug('test_getCampusContentJunctionByCategory_EN getLanguage() = ' + UserInfo.getLanguage());
			futureInitTest();
			Test.stopTest();
			icBusinessLogicCampusContent.IClass repo = (icBusinessLogicCampusContent.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicCampusContent');
			List<Junction_Content_Category__c> result = repo.getCampusContentJunctionByCategory(icTestCampusObjectData.contentCategory);			
		}
	}

	static testMethod void test_getCampusWelcomeContentJunctionByCategory() {
		initTest();

		icBusinessLogicCampusContent.IClass repo = (icBusinessLogicCampusContent.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicCampusContent');
		List<Junction_Content_Category__c> result = repo.getCampusWelcomeContentJunctionByCategory(icTestCampusObjectData.contentCategory);
	}

	static testMethod void test_getCampusWelcomeContentJunctionByCategory_TopLevel() {
		initTest();

		icBusinessLogicCampusContent.IClass repo = (icBusinessLogicCampusContent.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicCampusContent');
		List<Junction_Content_Category__c> result = repo.getCampusWelcomeContentJunctionByCategory('TOPLEVEL');
	}

	static testMethod void test_getMapCampusContentFromList() {
		initTest();

		icBusinessLogicCampusContent.IClass repo = (icBusinessLogicCampusContent.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicCampusContent');
		Map<String, List<icDTOContent>> result = repo.getMapCampusContentFromList(repo.getCampusContentJunctionByCategory(icTestCampusObjectData.contentCategory));
	}

	static testMethod void test_getMapCampusWelcomeContentFromList() {
		initTest();

		icBusinessLogicCampusContent.IClass repo = (icBusinessLogicCampusContent.IClass) icObjectFactory.GetSingletonInstance('icBusinessLogicCampusContent');
		Map<String, List<icDTOContent>> result = repo.getMapCampusWelcomeContentFromList(repo.getCampusWelcomeContentJunctionByCategory(icTestCampusObjectData.contentCategory));
	}
}