public class trac_CRM_SoapWebServicesMockGeneric implements WebServiceMock {
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        Object responseElement;

        response.put('response_x', responseElement); 
   }
}