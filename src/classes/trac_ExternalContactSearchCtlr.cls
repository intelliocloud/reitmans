/**
 *  @description Controller to search Account, Contacts, Leads, Opportunities and External Contacts 
 *  @author      Graham Barnard, Traction on Demand.
 *  @date        2015-12-21
 */
public without sharing class trac_ExternalContactSearchCtlr {

    public List<Case> cases { get;set; }
    public List<Lead> leads { get;set; }
    public List<Account> accounts { get;set; }
    public List<Contact> contacts { get;set; }
    public List<Ext_Contact__x> externalContacts { get;set; }

    public String searchString { get;set; }

    private static String ERROR_FAILED_CONNECTING_TO_EXTERNAL_SYSTEM = Label.External_Contact_Search_Error;

    private static Integer MINIMUM_SEARCH_LENGTH = 3;

    /**
     *  @description Constructor
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-23
     */
    public trac_ExternalContactSearchCtlr() {
        searchString = ApexPages.currentPage().getParameters().get('s');
        searchAll();
    }

    /**
     *  @description Search Salesforce Native and External Objects
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-23
     */
    public PageReference searchAll() {
        clearResults();
        if(String.isNotBlank(searchString) && searchString.length() >= MINIMUM_SEARCH_LENGTH) {
            searchString = searchString.toLowerCase();
            searchSOSL();
            searchExternalObjects();
        }
        return null;
    }

    /**
     *  @description Search in the external contact by email address
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-29
     */
     private void searchExternalObjects() {
        externalContacts = new List<Ext_Contact__x>();
        try {
            externalContacts = trac_ExternalContactModel.findAllByPhoneOrEmail(searchString);
        }
        catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ERROR_FAILED_CONNECTING_TO_EXTERNAL_SYSTEM));
        }
    }

    /**
     *  @description Search by SOSL
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-23
     */
    private void searchSOSL() {
        String soslSearchString = 'FIND \'' + searchString + '\' IN ALL FIELDS RETURNING Account (Id, Name, Phone, Email_Address__c), Contact(Name, Phone, FirstName, LastName, Email, Banner__c), Case(Id, Status, Origin), Lead(Id, Name)';

        System.debug('soslSearchString: ' + soslSearchString);
        List<List<SObject>> searchResults = Search.query(soslSearchString);

        System.debug('searchResults: ' + searchResults);
        for (List<SObject> searchResultList : searchResults) {
            if(searchResultList.getSObjectType() == Case.sObjectType) {
                cases = (List<Case>) searchResultList;
            }
            else if(searchResultList.getSObjectType() == Lead.sObjectType) {
                leads = (List<Lead>) searchResultList;
            }
            else if(searchResultList.getSObjectType() == Account.sObjectType) {
                accounts = (List<Account>) searchResultList;
            }
            else if(searchResultList.getSObjectType() == Contact.sObjectType) {
                contacts = (List<Contact>) searchResultList;
            }
        }
    }

    /**
     *  @description Clear all current results
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-02-01
     */
    private void clearResults() {
        cases = new List<Case>();
        leads = new List<Lead>();
        accounts = new List<Account>();
        contacts = new List<Contact>();
        externalContacts = new List<Ext_Contact__x>();
    }
}