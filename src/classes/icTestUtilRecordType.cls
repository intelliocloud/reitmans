@isTest
public with sharing class icTestUtilRecordType {

	static testMethod void test_getRecordTypeByObject() {
		List<RecordType> result = icUtilRecordType.getRecordTypeByObject('Campus_Content__c');
	}

	static testMethod void test_getRecordTypeByObjectAndName_NoResult() {
		RecordType result = icUtilRecordType.getRecordTypeByObjectAndName('Campus_Content__c', 'Name');
	}

	static testMethod void test_getRecordTypeByObjectAndName_Result() {
		RecordType result = icUtilRecordType.getRecordTypeByObjectAndName('Campus_Content__c', 'Welcome');
	}

	static testMethod void test_getRecordTypeByObjectAndId_NoResult() {
		RecordType result = icUtilRecordType.getRecordTypeByObjectAndId('Campus_Content__c', 'fakeId');
	}

	static testMethod void test_getRecordTypeByObjectAndId_Result() {
		RecordType recordType = icUtilRecordType.getRecordTypeByObjectAndName('Campus_Content__c', 'Welcome');
		RecordType result = icUtilRecordType.getRecordTypeByObjectAndId('Campus_Content__c', recordType.Id);
	}

	static testMethod void test_getRecordTypeByObjectAndName_ResultFromMap() {
		RecordType result = icUtilRecordType.getRecordTypeByObjectAndName('Campus_Content__c', 'Welcome');
		result = icUtilRecordType.getRecordTypeByObjectAndName('Campus_Content__c', 'Welcome');
	}

	static testMethod void test_getRecordTypeByObjectAndName_ResultFromMap_ReQuery() {
		RecordType result = icUtilRecordType.getRecordTypeByObjectAndName('Campus_Content__c', 'Welcome');
		result = icUtilRecordType.getRecordTypeByObjectAndName('Campus_Content__c', 'External');
	}
}