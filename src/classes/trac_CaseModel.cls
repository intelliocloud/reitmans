/**
 *  @description All SOQL calls for Case Object
 *  @author 	 Tanminder Rai, Traction on Demand.
 *  @date        2016-01-14
 */
public with sharing class trac_CaseModel {
	/**
	 *  @description Return a single case by id
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-01-04
	 */
	public static List<Case> findAllByIds(Set<Id> caseIds) {
		List<Case> cases = [
            SELECT Id, Banner__c, OwnerId, Store__c, Category__c, ParentId, Parent_Case__c, Order_Number__c, Subject, Description, 
            	Incident_Date__c, Web_Category__c, Sub_Category__c, Sub_Sub_Category__c, Product_Number_SKU__c, Case_Reassigned__c, 
            	ContactId, Language__c, Origin, Priority, Status, SuppliedEmail, SuppliedName, Contact.Email
            FROM Case
            WHERE Id IN : caseIds
        ];

		return cases;
	}

	/**
	 *  @description Gets a list of Cases given a set of Contact Ids
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-03-07
	 */
	public static List<Case> findCasesByContactIds(Set<Id> contactIds) {
		List<Case> cases = [
            SELECT Id, Banner__c, Case_Reassigned__c, ContactId, Language__c, Origin, Priority, Status, SuppliedEmail, SuppliedName
            FROM Case
            WHERE ContactId IN : contactIds
        ];

		return cases;
	}
}