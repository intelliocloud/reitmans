public with sharing class trac_ExternalContactUpdate {

	@TestVisible private static List<Ext_Contact__x> mockExternalContacts = new List<Ext_Contact__x>();

	public static void runContactUpdate(String startDateTime, String endDateTime) {
		String query;

		String queryLimit = String.valueOf(Limits.getLimitQueryRows() /2);

		query = 'SELECT Id, address_id__c, address_modified__c, address_type_code__c, apartment__c, birth_date__c,';
		query +=	' brand_prefix__c, city__c, country_code__c, create_date__c, create_source__c, customer_id__c,';
		query +=	' customer_no__c, due_date__c, email_address__c, email_indicator__c, email_opt_in__c, email_opt_in_date__c,';
		query +=	' first_name__c, gender__c, language_code__c, last_name__c, last_updated__c, mail_opt_in_flag__c,';
		query +=	' membership_date__c, membership_type_code__c, mobile_phone_no__c, phone_opt_in_flag__c, post_code__c,';
		query +=	' province__c, segmentation_flag_a__c, segmentation_flag_b__c, segmentation_flag_c__c, segmentation_flag_d__c,';
		query +=	' segmentation_flag_e__c, segmentation_flag_f__c, store_no__c, street_no__c, telephone_no__c';
		query +=	' FROM Ext_contact__x WHERE last_updated__c >= ' + startDateTime + ' AND last_updated__c <= ' + endDateTime;
		query +=    ' LIMIT ' + queryLimit;

		trac_QueryBuilder queryBuilder = new trac_QueryBuilder();
        queryBuilder.setObject('Contact');
        queryBuilder.setFields(new List<String>{
        	'Id', 'AccountId', 'Birthdate', 'Email', 'FirstName', 'LastName', 'Phone', 'MobilePhone', 'Banner__c', 'Blacklist__c', 'Created_Source__c',
			'Customer_Id__c', 'Customer_Number__c', 'External_ID__c', 'Gender__c', 'Membership_Status__c',
			'Opt_In__c', 'Opt_In_Date__c', 'Preferred_Contact_Number__c', 'Preferred_Language__c', 'Preferred_Store__c', 'Synced__c',
			'MailingStreet', 'MailingCity', 'MailingState', 'MailingCountry', 'MailingPostalCode',
			'Thyme_Segmentation_Flag__c', 'Nestle_Segmentation_Flag__c', 'Huggies_Segmentation_Flag__c',
			'CST_Segmentation_Flag__c', 'Other_Segmentation_Flag__c', 'BMO_Segmentation_Flag__c'
		});
        queryBuilder.setLogicalOperator(trac_QueryBuilder.OR_OPERATOR);

   		List<Ext_Contact__x> filteredExternalContacts = new List<Ext_Contact__x>();
   		List<sObject> externalContacts = Database.query(query);
   		if (Test.isRunningTest()) {
   			externalContacts = mockExternalContacts;
   		}
   		for(SObject s : externalContacts) {
			Ext_Contact__x externalContact = (Ext_Contact__x) s;

			if (externalContact.brand_prefix__c != null && externalContact.customer_no__c != null) {
				trac_QueryBuilder.Operator operator = new trac_QueryBuilder.Operator();
		        operator.setLogicalOperator(trac_QueryBuilder.AND_OPERATOR);
		        operator.addCondition(new trac_QueryBuilder.Condition('Banner__c', trac_QueryBuilder.EQUALS_OPERATOR, trac_SyncFromExternalObject.BRAND_MAP.get(externalContact.brand_prefix__c)));
		        operator.addCondition(new trac_QueryBuilder.Condition('Customer_Number__c', trac_QueryBuilder.EQUALS_OPERATOR, String.valueOf(externalContact.customer_no__c)));
		        queryBuilder.baseOperator.addOperator(operator);
					        
				filteredExternalContacts.add(externalContact);
			}
		}

		String queryString = queryBuilder.build();
		List<Contact> contacts = Database.query(queryString);

        Map<String, Contact> contactByUniqueKey = new Map<String, Contact>();  
        for (Contact contact : contacts) {
        	if (contact.Banner__c != null && contact.Customer_Number__c != null) {
        		contactByUniqueKey.put(trac_ExternalContactSyncBatch.createUniqueKey(contact.Banner__c, contact.Customer_Number__c), contact);
        	}
        }

		List<Contact> contactsToUpdate = new List<Contact>();
        
        //Get unique key from external object and get the new contact associated to it to sync with external objects
        for (Ext_Contact__x filteredExternalContact : filteredExternalContacts) {
        	String uniqueKey = trac_ExternalContactSyncBatch.createUniqueKey(trac_SyncFromExternalObject.BRAND_MAP.get(filteredExternalContact.brand_prefix__c), filteredExternalContact.customer_no__c);
        	if (contactByUniqueKey.containsKey(uniqueKey) && contactsToUpdate.size() <= Limits.getLimitDMLRows()) {
        		Contact contact = contactByUniqueKey.get(uniqueKey);
        		Id accountId = (contact.AccountId != null) ? contact.AccountId : trac_ExternalContactSyncBatch.findAccountId(contact, contacts);
        		contactsToUpdate.add(trac_SyncFromExternalObject.mapContact(contact, filteredExternalContact, accountId, false));
        	}
		}
		if (contactsToUpdate.size() > 0) {
			update contactsToUpdate;
		}
	}
}