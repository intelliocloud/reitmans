public class trac_CRM_SoapWebServicesMock implements WebServiceMock {
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        trac_CRM_SoapWebServices.SaveNewCustomerResponse_element responseElement = new trac_CRM_SoapWebServices.SaveNewCustomerResponse_element();
        responseElement.parameters = new trac_CRM_SoapWebServices.InitializeParameters();
        responseElement.parameters.CustomerNumber = 123456789;

        response.put('response_x', responseElement); 
   }
}