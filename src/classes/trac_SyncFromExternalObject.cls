/**
 *  @description Sync the external object data to the contact
 *  @author      Tanminder Rai, Traction on Demand.
 *  @date        2016-02-19
 */
public with sharing class trac_SyncFromExternalObject {

    private static Id customerServiceRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Service RCL').getRecordTypeId();

    private final static String THYME_MEMBERSHIP     = 'Thyme ID';
    private final static String DEFAULT_NAME         = 'Name Not Provided';
    private final static String DEFAULT_GENDER       = 'Unknown';
    private final static Integer OPT_IN_VALUE  = 1;
    private final static Integer OPT_OUT_VALUE = 2;
    private final static Map<String, String> GENDER_MAP = new Map<String, String>{
        'M' => 'Male',
        'F' => 'Female',
        'U' => 'Unknown'
    };

    public final static Map<String, String> BRAND_MAP = new Map<String, String>{
        'AE' => 'Addition Elle',
        'HY' => 'Hyba',
        'PE' => 'Penningtons',
        'RE' => 'Reitmans',
        'RW' => 'RW&Co',
        'SS' => 'Smart Set',
        'TH' => 'Thyme'
    };

    public final static Map<String, String> COUNTRY_CODE_MAP = new Map<String, String>{
        'CA'  => 'CAN',
        'US'  => 'USA',
        'CAF' => 'CAN',
        'CAN' => 'CAN',
        'USA' => 'USA'
    };

    private final static Set<String> SEGMENTATION_MEMBERSHIP_BANNERS = new Set<String>{'Thyme'};

    public final static String CANADA_POSTAL_CODE_REGEX = '[A-Z][0-9][A-Z] [0-9][A-Z][0-9]';
    public final static String USA_POSTAL_CODE_REGEX = '[0-9]{5}';

    /**
     *  @description Update the contact information
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-10
     */
     public static Contact mapContact(Contact contact, Ext_Contact__x externalContact, Id accountId, Boolean upsertRecord ) {
        Boolean isDirty = (contact.Id == null);

        System.debug('externalContact: ' + externalContact);

        String externalId = externalContact.brand_prefix__c + '-' + externalContact.customer_id__c;

        if(contact.External_ID__c != externalId) {
           contact.External_ID__c = externalId;
           isDirty = true;
        }

        if(contact.AccountId != accountId) {
           contact.AccountId = accountId;
           isDirty = true;
        }

        if(contact.FirstName != externalContact.first_name__c) {
           contact.FirstName = externalContact.first_name__c != null ? externalContact.first_name__c : DEFAULT_NAME;
           isDirty = true;
        }


        if(contact.LastName != externalContact.last_name__c) {
           contact.LastName = externalContact.last_name__c != null ? externalContact.last_name__c : DEFAULT_NAME;
           isDirty = true;
        }

        if(contact.Email != externalContact.email_address__c) {
           contact.Email = externalContact.email_address__c;
           isDirty = true;
        }

        if(contact.Customer_Id__c != externalContact.customer_id__c) {
           contact.Customer_Id__c = externalContact.customer_id__c;
           isDirty = true;
        }

        String customerNumber = String.valueOf(externalContact.customer_no__c);
        if(contact.Customer_Number__c != customerNumber) {
           contact.Customer_Number__c = customerNumber;
           isDirty = true;
        }

        String banner = BRAND_MAP.get(externalContact.brand_prefix__c);
        if(contact.Banner__c != banner) {
           contact.Banner__c = banner;
           isDirty = true;
        }

        if(contact.Phone != externalContact.telephone_no__c) {
           contact.Phone = externalContact.telephone_no__c;
           isDirty = true;
        }

       if(contact.MobilePhone != externalContact.mobile_phone_no__c) {
           contact.MobilePhone = externalContact.mobile_phone_no__c;
           isDirty = true;
        }

        String gender = GENDER_MAP.containsKey(externalContact.gender__c) ? GENDER_MAP.get(externalContact.gender__c) : DEFAULT_GENDER;
        if(contact.Gender__c != gender) {
            contact.Gender__c = gender;
            isDirty = true;
        }

        if(contact.Preferred_Language__c != externalContact.language_code__c) {
            contact.Preferred_Language__c = externalContact.language_code__c;
            isDirty = true;
        }

        Date birthdate = Date.valueOf(externalContact.birth_date__c);
        if(contact.Birthdate != birthdate) {
            contact.Birthdate = birthdate;
            isDirty = true;
        }

        if(SEGMENTATION_MEMBERSHIP_BANNERS.contains(contact.Banner__c)) {
            if (externalContact.segmentation_flag_a__c && contact.Membership_Status__c != THYME_MEMBERSHIP) {
                contact.Membership_Status__c = THYME_MEMBERSHIP;
                isDirty = true;
            } else if (!externalContact.segmentation_flag_a__c && contact.Membership_Status__c == THYME_MEMBERSHIP) {
                contact.Membership_Status__c = null;
                isDirty = true;
            }
        }

        Boolean emailOptIn = (externalContact.email_opt_in__c != OPT_OUT_VALUE);
        if(contact.Opt_In__c != emailOptIn) {
            contact.Opt_In__c = emailOptIn;
            isDirty = true;
        }

        Boolean phoneOptIn = (externalContact.phone_opt_in_flag__c != OPT_OUT_VALUE);
        if(contact.Phone_Opt_In__c != phoneOptIn) {
            contact.Phone_Opt_In__c = phoneOptIn;
            isDirty = true;
        }

        Boolean mailOptIn = (externalContact.mail_opt_in_flag__c != OPT_OUT_VALUE);
        if(contact.Mail_Opt_In__c != mailOptIn) {
            contact.Mail_Opt_In__c = mailOptIn;
            isDirty = true;
        }

        Date emailOptInDate = Date.valueOf(externalContact.email_opt_in_date__c);
        if(contact.Opt_In_Date__c != emailOptInDate) {
            contact.Opt_In_Date__c = emailOptInDate;
            isDirty = true;
        }

        if(contact.Created_Source__c != externalContact.create_source__c) {
            contact.Created_Source__c = externalContact.create_source__c;
            isDirty = true;
        }

        if(contact.Thyme_Segmentation_Flag__c != externalContact.segmentation_flag_a__c) {
            contact.Thyme_Segmentation_Flag__c = externalContact.segmentation_flag_a__c;
            isDirty = true;
        }

        if(contact.Nestle_Segmentation_Flag__c != externalContact.segmentation_flag_b__c) {
            contact.Nestle_Segmentation_Flag__c = externalContact.segmentation_flag_b__c;
            isDirty = true;
        }

        if(contact.Huggies_Segmentation_Flag__c != externalContact.segmentation_flag_c__c) {
            contact.Huggies_Segmentation_Flag__c = externalContact.segmentation_flag_c__c;
            isDirty = true;
        }

        if(contact.CST_Segmentation_Flag__c != externalContact.segmentation_flag_d__c) {
            contact.CST_Segmentation_Flag__c = externalContact.segmentation_flag_d__c;
            isDirty = true;
        }

        if(contact.Other_Segmentation_Flag__c != externalContact.segmentation_flag_e__c) {
            contact.Other_Segmentation_Flag__c = externalContact.segmentation_flag_e__c;
            isDirty = true;
        }

        if(contact.BMO_Segmentation_Flag__c != externalContact.segmentation_flag_f__c) {
           contact.BMO_Segmentation_Flag__c = externalContact.segmentation_flag_f__c;
            isDirty = true;
        }

        String street = externalContact.street_no__c != null ? externalContact.street_no__c : '';
        String appartment = externalContact.apartment__c != null ? externalContact.apartment__c : '';
        if(contact.MailingStreet != externalContact.street_no__c && contact.MailingStreet != (street + ' ' + appartment)) {
            contact.MailingStreet = street + ' ' + appartment;
            isDirty = true;
        }

        if(COUNTRY_CODE_MAP.containsKey(externalContact.country_code__c) && contact.MailingCountry != COUNTRY_CODE_MAP.get(externalContact.country_code__c)
        && externalContact.province__c != null) {
            contact.MailingCountry = COUNTRY_CODE_MAP.get(externalContact.country_code__c);
            isDirty = true;
        }

        if(!String.isBlank(contact.MailingCountry) && contact.MailingState != externalContact.province__c) {
            contact.MailingState = externalContact.province__c;
            isDirty = true;
        }

        if(contact.MailingCity != externalContact.city__c) {
            contact.MailingCity = externalContact.city__c;
            isDirty = true;
        }

        if(contact.MailingPostalCode != externalContact.post_code__c && externalContact.post_code__c != null) {
            if ((contact.MailingCountry == 'CAN' && Pattern.matches(CANADA_POSTAL_CODE_REGEX, externalContact.post_code__c)) || 
              (contact.MailingCountry == 'USA' && Pattern.matches(USA_POSTAL_CODE_REGEX, externalContact.post_code__c))) {
                contact.MailingPostalCode = externalContact.post_code__c;
                isDirty = true;
            }            
        }

        if(contact.Synced__c != true) {
            contact.Synced__c = true;
            isDirty = true;
        }

        if(isDirty && upsertRecord) {
            contact.RecordTypeId = customerServiceRecordTypeId;
            try {
                upsert contact;
            }
            catch (Exception e) {
                contact.MailingStreet = null;
                contact.MailingCity = null;
                contact.MailingPostalCode = null;
                contact.MailingState = null;
                contact.MailingCountry = null;

                try {
                    upsert contact;
                }
                catch (Exception ex) {

                }
            }
        }

        return contact;
     }
}