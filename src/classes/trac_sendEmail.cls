/**
 * Send email  
 * @description Sends an email
 * @author Tanminder Rai, Traction on Demand
 * @date 2016-25-7
 */

public class trac_sendEmail {

	private Messaging.SingleEmailMessage emailToSend;

	public trac_sendEmail(String recipient, Id caseId, String templateId){
		List<String> recipients = new List<String>();
		recipients.add(recipient);

		 Contact c = [select id, Email from Contact where email <> null limit 1];

		Messaging.SingleEmailMessage tmpEmail = new Messaging.SingleEmailMessage();
		tmpEmail.setTemplateId(templateId);
		tmpEmail.setWhatId(caseId);
		tmpEmail.setTargetObjectId(c.Id);
		tmpEmail.setToAddresses(recipients); 
		tmpEmail.setSaveAsActivity(false);

		Savepoint sp = Database.setSavepoint();
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] {tmpEmail}); // Dummy email send
		Database.rollback(sp); // Email will not send as it is rolled Back

		emailToSend = new Messaging.SingleEmailMessage();
		emailToSend.setToAddresses(tmpEmail.getToAddresses());
		emailToSend.setPlainTextBody(tmpEmail.getPlainTextBody());
		emailToSend.setHTMLBody(tmpEmail.getHTMLBody());
		emailToSend.setSubject(tmpEmail.getSubject());
		emailToSend.setSaveAsActivity(false);

	}

	public void sendEmail(){
		try {
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailToSend});
	        return;
	    }
    	catch (EmailException e) {
    		throw new UtilException('[U-02] sendTemplatedEmail error. ' + e.getMessage());
    	}		
	}

	public class UtilException extends Exception {} 
}