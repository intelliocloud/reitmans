/**
 * If the case for the incoming email message is already closed,
 * 		clones the case and reparent's the message to the new case
 * @Date: 5/19/2016
 * @auther: Tanminder Rai
 */

public with sharing class trac_ClosedCaseEmail {

	public static final String templateApiName = 'New_Email_form_Closed_Case';

	public static void onInsert(List<EmailMessage> emailMessages) {

		Map<Id, EmailMessage> caseToEmailMsgMap = getCaseToEmailMsgMap(emailMessages);
		List<Case> parentCases 					= trac_CaseModel.findAllByIds( caseToEmailMsgMap.keySet() );

		//If the case is already closed, clone the case
		List<Case> casesToInsert = new List<Case>();
		Set<Id> caseOwnerIds = new Set<Id>();
		for(Case pCase : parentCases){
			if ( pCase.status == 'Closed' ){
				Case newCase = pCase.clone(false, true);
				newCase.Status = 'Assigned';
				newCase.Parent_Case__c = pCase.Id;
				newCase.ParentId = pCase.Id;
				casesToInsert.add( newCase );
				caseOwnerIds.add( pCase.OwnerId );
			}
		}
		insert casesToInsert;

		//Re-parent the email message to the new case
		Map<Id, User> caseOwnersMap = getOwnerEmailAddress(caseOwnerIds);
		for(Case newCase : casesToInsert){
			EmailMessage em = caseToEmailMsgMap.get( newCase.ParentId );
			em.ParentId = newCase.Id;
			String ownerEmail = caseOwnersMap.get( newCase.OwnerId ).Email;
			String templateId = getEmailTemplateId();
			if(templateId != null && ownerEmail != null){
				trac_SendEmail email = new trac_SendEmail( ownerEmail, newCase.Id, templateId);
				email.sendEmail();
			}
		}

	}

	/**
	 * Get a Map of case Ids to email messages
	 * @param  emailMessages 
	 * @return case to email message map
	 */
	private static Map<Id, EmailMessage> getCaseToEmailMsgMap(List<EmailMessage> emailMessages){
		Map<Id, EmailMessage> caseToEmailMsgMap = new Map<Id, EmailMessage>();
		for(EmailMessage em : emailMessages){
			if (String.isNotBlank(em.ParentId) ){
				caseToEmailMsgMap.put( em.ParentId, em );
			}
		}
		return caseToEmailMsgMap;
	}

	/**
	 * Query the template id
	 * @return Template id
	 */
	private static String getEmailTemplateId(){
		EmailTemplate et = [SELECT Id, Name FROM EmailTemplate WHERE developername = : templateApiName];
		return et == null? null : et.Id;
	}

	private static Map<Id, User> getOwnerEmailAddress(Set<Id> caseOwnerIds){
		return new Map<Id, User>([SELECT Email FROM User WHERE Id=:caseOwnerIds]);
	}
}