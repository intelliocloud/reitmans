/**
 *  @description Tests for trac_ExternalContactSyncBatch, trac_ExternalContactUpdateBatch and trac_ExternalContactSyncUpdateScheduler
 *  @author 	 Jeremy Horan, Traction on Demand.
 *  @date        2016-03-11
 */
@isTest
private class trac_ExternalContactSyncUpdateTest {

	private static Account account;

	static {
		account = trac_TestUtils.setupAccount();
		insert account;		
	}
	
	@isTest static void test_ContactSyncBatch() {

		List<sObject> externalContactXrefs = trac_TestUtils.setupMockExternalContactXrefs();

		System.assert(externalContactXrefs.size() > 0);

		Ext_Contact_Xref__x externalContactXref = (Ext_Contact_Xref__x) externalContactXrefs[0];

		Contact oldContact = trac_TestUtils.setupContact(account.Id);
		oldContact.Customer_Number__c = String.valueOf(externalContactXref.old_customer_no__c.longValue());
		oldContact.Customer_Id__c = 134567891;
		oldContact.Banner__c = trac_SyncFromExternalObject.BRAND_MAP.get(externalContactXref.brand_prefix__c);
		oldContact.LastName = 'test last name';


		Contact newContact = trac_TestUtils.setupContact(account.Id);
		newContact.Customer_Number__c = String.valueOf(externalContactXref.new_customer_no__c.longValue());
		newContact.Customer_Id__c = 134567892;
		newContact.Banner__c = trac_SyncFromExternalObject.BRAND_MAP.get(externalContactXref.brand_prefix__c);
		newContact.LastName = 'test last name';

		List<Contact> contacts = new List<Contact>{oldContact, newContact};
		insert contacts;

		Case c = trac_TestUtils.setupCase(oldContact.Id);
		insert c;

		Ext_Contact__x newExternalContact = new Ext_Contact__x(
			brand_prefix__c = externalContactXref.brand_prefix__c,
			customer_no__c = externalContactXref.new_customer_no__c,
			email_address__c = 'test@email.com',
			first_name__c = 'test first name'+trac_TestUtils.generateRandomString(5),
			last_name__c = 'test last name',
			customer_id__c = 134567892,
			telephone_no__c = '514-111-1111',
			mobile_phone_no__c = '514-222-2222',
			gender__c = 'F',
			language_code__c = 'eng',
			birth_date__c = DateTime.now(),
			membership_type_code__c = 'abcd',
			email_opt_in__c = 1,
			email_opt_in_date__c = DateTime.now(),
			create_source__c = 'source',
			segmentation_flag_a__c = true,
			segmentation_flag_b__c = true,
			segmentation_flag_c__c = true,
			segmentation_flag_d__c = true,
			segmentation_flag_e__c = true,
			segmentation_flag_f__c = true
		);
		trac_ExternalContactModel.mockExternalContacts.add(newExternalContact);

		Test.startTest();
		trac_ExternalContactSyncBatch externalContactSyncBatch = new trac_ExternalContactSyncBatch();
		Database.executebatch(externalContactSyncBatch, 50);
		Test.stopTest();

		Case queriedCase = [SELECT Id, ContactId FROM Case WHERE Id =: c.Id];
		System.assertEquals(newContact.Id, queriedCase.ContactId);

		Contact queriedNewContact = [SELECT Id, FirstName FROM Contact WHERE Id =: newContact.Id];
		System.assertEquals(newExternalContact.first_name__c.toUpperCase(), queriedNewContact.FirstName.toUpperCase());

		List<Contact> queriedOldContacts = [SELECT Id FROM Contact WHERE Id =: oldContact.Id];
		System.assertEquals(0, queriedOldContacts.size());
	}


	@isTest static void test_ContactUpdateBatch() {

		List<sObject> externalContacts = trac_TestUtils.setupMockExternalContacts();

		System.assert(externalContacts.size() > 0);

		Ext_Contact__x externalContact = (Ext_Contact__x) externalContacts[0];

		Contact contact = trac_TestUtils.setupContact(account.Id);
		contact.Customer_Number__c = String.valueOf(externalContact.customer_no__c);
		contact.Customer_Id__c = 134567892;
		contact.Banner__c = trac_SyncFromExternalObject.BRAND_MAP.get(externalContact.brand_prefix__c);
		contact.LastName = 'test last name';
		insert contact;

		Test.startTest();
		trac_ExternalContactUpdateBatch externalContactUpdateBatch = new trac_ExternalContactUpdateBatch();
		Database.executebatch(externalContactUpdateBatch, 50);
		Test.stopTest();

		Contact queriedcontact = [SELECT Id, FirstName FROM Contact WHERE Id =: contact.Id];
		System.assertEquals(externalContact.first_name__c.toUpperCase(), queriedContact.FirstName.toUpperCase());
	}

	@isTest static void test_ManualScheduling() {
		System.assertNotEquals(null, trac_ExternalContactSyncUpdateScheduler.scheduleDaily(trac_TestUtils.generateRandomString(10)));
	}

	@isTest static void test_FindingAccountId() {
		Integer count = 3;
		Contact contact = new Contact(LastName = 'test', Email = String.valueOf(count) + 'test@email.com');
		List<Contact> contacts = new List<Contact>();
		List<Account> accounts = new List<Account>();
		for(Integer i = 0; i < 5; i++) {
			Account tempAccount = trac_TestUtils.setupAccount();
			insert tempAccount;
			accounts.add(tempAccount);
			contacts.add(new Contact(AccountId = tempAccount.Id, LastName = 'test', Email = String.valueOf(i) + 'test@email.com'));
		}
		System.assertEquals(accounts[count].Id, trac_ExternalContactSyncBatch.findAccountId(contact, contacts));
	}
	
}