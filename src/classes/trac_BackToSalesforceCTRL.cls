/**
 *  @description trac_BackToSalesforceCTRL
 *  @author      Brian Williams, Traction on Demand.
 *  @date        2016-10-04
 */
public class trac_BackToSalesforceCTRL {

	private static final Set<String> SALESFORCE_LICENCES = new Set<String>{'Salesforce'};

	/**
	 *  @description Returns True if User should have access to Salesforce from the community
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-11-08
	 */
	@AuraEnabled
	public static Boolean hasSalesforceAccess() {
	    
		String profileLicenceId;
		if (UserInfo.getProfileid() != null) {
			Profile profile = [SELECT UserLicenseId FROM Profile WHERE Id =: UserInfo.getProfileid()];
			profileLicenceId = profile.UserLicenseId;
		}

		if (profileLicenceId != null) {
			UserLicense userLicense = [SELECT Name FROM UserLicense WHERE Id =: profileLicenceId];
			if (SALESFORCE_LICENCES.contains(userLicense.Name)) {
				return true;
			}
		}
		User user = [SELECT Id, Allowed_back_to_Salesforce__c From User WHERE Id =: UserInfo.getUserId()];
		if (user.Allowed_back_to_Salesforce__c) {
			return true;
		}
		return false;
	}
    
}