/**
 *  @description Test for trac_CouponIsUsed
 *  @author 	 Marco Martel, Traction on Demand.
 *  @date        2016-02-12
 */

@isTest
private class trac_CouponIsUsedTest {

	public static String ACTIVE_STATUS = 'Active';
	public static String USED_STATUS = 'Used';

	@isTest static void twoCouponInsert(){
		Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        contact.Banner__c = 'REIT';
        contact.FirstName = 'First';
        contact.LastName = 'Last';
        insert contact;
		
		Coupon__c coupon = trac_TestUtils.setupCoupon();
		coupon.Name = '1';
		insert coupon;



		List<Coupon__c> coupons = [
			SELECT Id, Is_Coupon_Used__c, Case__c
			FROM Coupon__c
			WHERE Id = :coupon.Id
			];

		System.assert(!coupons[0].Is_Coupon_Used__c);

		Case case1 = trac_TestUtils.setupCase(contact.Id);
		insert case1;

		Coupon__c coupon2 = trac_TestUtils.setupCoupon();
		coupon2.Name = '2';
		insert coupon2;

		case1.Coupon_Code_1__c = coupon.Id;
		case1.Coupon_Code_2__c = coupon2.Id;
		update case1;

		coupons = [
			SELECT Id, Is_Coupon_Used__c, Case__c
			FROM Coupon__c
			WHERE Id = :coupon.Id OR Id = :coupon2.Id
			ORDER BY NAME ASC
			];

		System.assert(coupons[0].Is_Coupon_Used__c);
		System.assert(coupons[0].Case__c==case1.Id);
		System.assert(coupons[1].Is_Coupon_Used__c);
		System.assert(coupons[1].Case__c==case1.Id);

	}

	@isTest static void bulkCouponInsert(){

		Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        contact.Banner__c = 'REIT';
        contact.FirstName = 'First';
        contact.LastName = 'Last';
        insert contact;

        List<Coupon__c> coupons = new List<Coupon__c>();
        for(integer i=0; i>20; i++){
        	Coupon__c coupon = trac_TestUtils.setupCoupon();
        	coupon.Status__c = ACTIVE_STATUS;
        	coupons.add(coupon);
        }
		insert coupons;

		List<Coupon__c> couponsToTest = ([
			SELECT Id, Status__c, Is_Coupon_Used__c, Case__c
			FROM Coupon__c
			WHERE Id IN :coupons]);

		Boolean isCorrect = true;
		for(Coupon__c couponToTest:couponsToTest){
			if(couponToTest.Status__c != ACTIVE_STATUS || couponToTest.Is_Coupon_Used__c || couponToTest.Case__c != null){
				isCorrect = false;
			}
		}

		System.assert(isCorrect);

		List<Case> cases = new List<Case>();
		for(integer i=0; i>10; i++){
			Case case1 = trac_TestUtils.setupCase(contact.Id);
			case1.Coupon_Code_1__c = couponsToTest[i].Id;
			cases.add(case1);
		}
		insert cases;

		couponsToTest = ([
			SELECT Id, Status__c, Is_Coupon_Used__c, Case__c
			FROM Coupon__c
			WHERE Id IN :coupons]);

		isCorrect = true;
		for(Coupon__c couponToTest:couponsToTest){
			if(couponToTest.Status__c != USED_STATUS || !couponToTest.Is_Coupon_Used__c || couponToTest.Case__c == null){
				isCorrect = false;
			}
		}

		System.assert(isCorrect);	
	}

	@isTest static void sameCouponInsert(){

		Boolean doesTestBlowUp = false;
		Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        contact.Banner__c = 'REIT';
        contact.FirstName = 'First';
        contact.LastName = 'Last';
        insert contact;
		
		Coupon__c coupon = trac_TestUtils.setupCoupon();
		insert coupon;

		List<Coupon__c> coupons = [
			SELECT Id, Is_Coupon_Used__c, Case__c
			FROM Coupon__c
			WHERE Id = :coupon.Id
			];

		System.assert(!coupons[0].Is_Coupon_Used__c);

		Case case1 = trac_TestUtils.setupCase(contact.Id);
		insert case1;

		case1.Coupon_Code_1__c = coupon.Id;
		update case1;

		Case case2 = trac_TestUtils.setupCase(contact.Id);
		case2.Coupon_Code_1__c = coupon.Id;

		try{
			insert case2;
			} catch(Exception e){
				doesTestBlowUp = true;
			} finally{
				System.assert(doesTestBlowUp);
			}

	}
}