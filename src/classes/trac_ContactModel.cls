/**
 *  @description Query calls for contact object
 *  @author      Graham Barnard, Traction on Demand.
 *  @date        2016-01-04
 */
public with sharing class trac_ContactModel {

    private static Id contactCustomerServiceRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Service RCL').getRecordTypeId();
    /**
     *  @description Return a list of contact records matching the email address
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-01
     */
    public static Contact findById(Id contactId) {
        List<Contact> contacts = [
            SELECT Id, AccountId, Birthdate, Email, FirstName, LastName, Phone, MobilePhone, Banner__c, Blacklist__c, Created_Source__c,
                    Customer_Id__c, Customer_Number__c, External_ID__c, Gender__c, Membership_Status__c,
                    Opt_In__c, Opt_In_Date__c, Preferred_Contact_Number__c, Preferred_Language__c, Preferred_Store__c, Synced__c,
                    MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode,
                    Thyme_Segmentation_Flag__c, Nestle_Segmentation_Flag__c, Huggies_Segmentation_Flag__c,
                    CST_Segmentation_Flag__c, Other_Segmentation_Flag__c, BMO_Segmentation_Flag__c,
                    Phone_Opt_In__c, Mail_Opt_In__c
            FROM Contact
            WHERE Id =: contactId AND RecordTypeId =: contactCustomerServiceRecordTypeId
        ];

        return (contacts.size() > 0) ? contacts[0] : null;
    }

    /**
     *  @description Return a list of contact records matching the email address
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-01
     */
    public static List<Contact> findAllUnsyncecByIds(Set<Id> contactIds) {
        List<Contact> contacts = [
            SELECT Id, AccountId, Birthdate, Email, FirstName, LastName, Phone, MobilePhone, Banner__c, Blacklist__c, Created_Source__c,
                    Customer_Id__c, Customer_Number__c, External_ID__c, Gender__c, Membership_Status__c,
                    Opt_In__c, Opt_In_Date__c, Preferred_Contact_Number__c, Preferred_Language__c, Preferred_Store__c, Synced__c,
                    MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode,
                    Thyme_Segmentation_Flag__c, Nestle_Segmentation_Flag__c, Huggies_Segmentation_Flag__c,
                    CST_Segmentation_Flag__c, Other_Segmentation_Flag__c, BMO_Segmentation_Flag__c,
                    Phone_Opt_In__c, Mail_Opt_In__c 
            FROM Contact
            WHERE Id IN: contactIds AND Synced__c = false AND RecordTypeId =: contactCustomerServiceRecordTypeId
        ];

        return contacts;
    }

    /**
     *  @description Find all contacts in a set of Ids
     *  @author      Jeremy Horan, Traction on Demand.
     *  @date        2016-05-09
     */
    public static List<Contact> findAllByIds(Set<Id> contactIds) {
        List<Contact> contacts = [
            SELECT Id, AccountId, Birthdate, Email, FirstName, LastName, Phone, MobilePhone, Banner__c, Blacklist__c, Created_Source__c,
                    Customer_Id__c, Customer_Number__c, External_ID__c, Gender__c, Membership_Status__c,
                    Opt_In__c, Opt_In_Date__c, Preferred_Contact_Number__c, Preferred_Language__c, Preferred_Store__c, Synced__c,
                    MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode,
                    Thyme_Segmentation_Flag__c, Nestle_Segmentation_Flag__c, Huggies_Segmentation_Flag__c,
                    CST_Segmentation_Flag__c, Other_Segmentation_Flag__c, BMO_Segmentation_Flag__c,
                    Phone_Opt_In__c, Mail_Opt_In__c 
            FROM Contact
            WHERE Id IN: contactIds AND RecordTypeId =: contactCustomerServiceRecordTypeId
        ];

        return contacts;
    }

    /**
     *  @description Return a list of contact records matching the email address
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2015-12-01
     */
    public static List<Contact> findAllByEmailAddress(String emailAddress) {
        List<Contact> contacts = [
            SELECT Id, AccountId, Birthdate, Email, FirstName, LastName, Phone, MobilePhone, Banner__c, Blacklist__c, Created_Source__c,
                    Customer_Id__c, Customer_Number__c, External_ID__c, Gender__c, Membership_Status__c,
                    Opt_In__c, Opt_In_Date__c, Preferred_Contact_Number__c, Preferred_Language__c, Preferred_Store__c, Synced__c,
                    MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode,
                    Thyme_Segmentation_Flag__c, Nestle_Segmentation_Flag__c, Huggies_Segmentation_Flag__c,
                    CST_Segmentation_Flag__c, Other_Segmentation_Flag__c, BMO_Segmentation_Flag__c,
                    Phone_Opt_In__c, Mail_Opt_In__c 
            FROM Contact
            WHERE Email =: emailAddress AND RecordTypeId =: contactCustomerServiceRecordTypeId
        ];

        return contacts;
    }

    /**
     *  @description Return a list of contact records matching the email address and Banner
     *  @author      Tanminder Rai, Traction on Demand.
     *  @date        2016-02-03
     */
    public static List<Contact> findAllByEmailAddressAndBanner(String emailAddress, String banner) {
        List<Contact> contacts = [
            SELECT Id, AccountId, Birthdate, Email, FirstName, LastName, Phone, MobilePhone, Banner__c, Blacklist__c, Created_Source__c,
                    Customer_Id__c, Customer_Number__c, External_ID__c, Gender__c, Membership_Status__c,
                    Opt_In__c, Opt_In_Date__c, Preferred_Contact_Number__c, Preferred_Language__c, Preferred_Store__c, Synced__c,
                    MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode,
                    Thyme_Segmentation_Flag__c, Nestle_Segmentation_Flag__c, Huggies_Segmentation_Flag__c,
                    CST_Segmentation_Flag__c, Other_Segmentation_Flag__c, BMO_Segmentation_Flag__c,
                    Phone_Opt_In__c, Mail_Opt_In__c 
            FROM Contact
            WHERE Email =: emailAddress AND Banner__c =: banner AND RecordTypeId =: contactCustomerServiceRecordTypeId
        ];

        return contacts;
    }

    /**
     *  @description Return a list of contact records matching the Customer Number
     *  @author      Jeremy Horan, Traction on Demand.
     *  @date        2016-03-07
     */
    public static List<Contact> findAllByCustomerNumbers(List<String> customerNumbers) {
        List<Contact> contacts = [
            SELECT Id, AccountId, Birthdate, Email, FirstName, LastName, Phone, MobilePhone, Banner__c, Blacklist__c, Created_Source__c,
                    Customer_Id__c, Customer_Number__c, External_ID__c, Gender__c, Membership_Status__c,
                    Opt_In__c, Opt_In_Date__c, Preferred_Contact_Number__c, Preferred_Language__c, Preferred_Store__c, Synced__c,
                    MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode,
                    Thyme_Segmentation_Flag__c, Nestle_Segmentation_Flag__c, Huggies_Segmentation_Flag__c,
                    CST_Segmentation_Flag__c, Other_Segmentation_Flag__c, BMO_Segmentation_Flag__c,
                    Phone_Opt_In__c, Mail_Opt_In__c 
            FROM Contact
            WHERE Customer_Number__c IN :customerNumbers AND RecordTypeId =: contactCustomerServiceRecordTypeId
        ];

        return contacts;
    }

    /**
     *  @description Returns Contact with given ID
     *  @author      Jeremy Horan, Traction on Demand.
     *  @date        2016-04-27
     */
    public static Contact findByCustomerNumberAndBanner(String customerNumber, String banner) {
        List<Contact> contacts = [
            SELECT Id, AccountId, Birthdate, Email, FirstName, LastName, Phone, MobilePhone, Banner__c, Blacklist__c, Created_Source__c,
                    Customer_Id__c, Customer_Number__c, External_ID__c, Gender__c, Membership_Status__c,
                    Opt_In__c, Opt_In_Date__c, Preferred_Contact_Number__c, Preferred_Language__c, Preferred_Store__c, Synced__c,
                    MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode,
                    Thyme_Segmentation_Flag__c, Nestle_Segmentation_Flag__c, Huggies_Segmentation_Flag__c,
                    CST_Segmentation_Flag__c, Other_Segmentation_Flag__c, BMO_Segmentation_Flag__c,
                    Phone_Opt_In__c, Mail_Opt_In__c 
            FROM Contact
            WHERE Customer_Number__c =: customerNumber AND Banner__c =: banner AND RecordTypeId =: contactCustomerServiceRecordTypeId
        ];

        return (contacts.size() > 0) ? contacts[0] : null;
    }
}