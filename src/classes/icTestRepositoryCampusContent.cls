@isTest
public with sharing class icTestRepositoryCampusContent {

	static void initTest() {
		icTestCampusObjectData.createCampusDataSet();
	}

	static testMethod void test_getCampusContentJunctionByCategoryAndLanguage_FR() {
		initTest();

		icRepositoryCampusContent.IClass repo = (icRepositoryCampusContent.IClass) icObjectFactory.GetSingletonInstance('icRepositoryCampusContent');
		List<Junction_Content_Category__c> result =  repo.getCampusContentJunctionByCategoryAndLanguage(icTestCampusObjectData.contentCategory, 'French');
	}

	static testMethod void test_getCampusContentJunctionByCategoryAndLanguage_EN() {
		initTest();

		icRepositoryCampusContent.IClass repo = (icRepositoryCampusContent.IClass) icObjectFactory.GetSingletonInstance('icRepositoryCampusContent');
		List<Junction_Content_Category__c> result =  repo.getCampusContentJunctionByCategoryAndLanguage(icTestCampusObjectData.contentCategory, 'English');
	}

	static testMethod void test_getCampusWelcomeContentJunctionByCategory() {
		initTest();

		icRepositoryCampusContent.IClass repo = (icRepositoryCampusContent.IClass) icObjectFactory.GetSingletonInstance('icRepositoryCampusContent');
		List<Junction_Content_Category__c> result =  repo.getCampusWelcomeContentJunctionByCategory(icTestCampusObjectData.contentCategory);
	}
}