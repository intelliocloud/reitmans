@isTest
public with sharing class icTestCampusObjectData {
	public static String contentCategory;
	
	public static void createCampusDataSet() {
		List<Campus_Category__c> newCategories = new List<Campus_Category__c>();
		for(Integer i = 0; i < 6; i++) {
			newCategories.add(
				new Campus_Category__c(
					Name = 'cName_' + i
					,Sort_Order__c = 'o_' + i
					,Label_EN__c = 'enName_' + i
					,Label_FR__c = 'frName_' + i
					,Active__c = true
				)
			);
		}
		insert newCategories;
		String top1Id = newCategories[0].Id;
		String top2Id = newCategories[1].Id;
		newCategories[2].Parent_Category__c = top1id;
		newCategories[3].Parent_Category__c = top1id;
		newCategories[4].Parent_Category__c = top2id;
		newCategories[5].Parent_Category__c = top2id;
		contentCategory = newCategories[5].Id;
		update newCategories;

		List<String> listLanguages = new List<String>();
		listLanguages.add('French');
		listLanguages.add('English');	
		List<Campus_Content__c> newContents = new List<Campus_Content__c>();		
		for(RecordType objRecordType: [SELECT Id, Name, SobjectType FROM RecordType WHERE SObjectType = 'Campus_Content__c']) {
			for(String strLanguage : listLanguages) {
				newContents.add(
					new Campus_Content__c(
						RecordTypeId = objRecordType.Id
						,Language__c = strLanguage
						,Name = objRecordType.Name
						,Title__c = 'Title__c'
						,Type__c = 'eLearning'
						,Short_Description__c = 'Short_Description__c'
						,Description__c = 'Description__c'
						,Cost__c = 'Cost__c'
						,Eligibility__c  = 'Eligibility__c'
						,How_to_Register__c  = 'How_to_Register__c'
						,Location__c = 'Location__c'
						,Partners__c = 'Partners__c'
						,Syllabus__c = 'Syllabus__c'
						,Target_Audience__c = 'Target_Audience__c'
						,Time_Commitment__c = 'Time_Commitment__c'						
						,Video__c = 'http://www.google.com'
						,Article__c = 'http://www.google.com'
						,Content_Details_EN__c = 'Content_Details_EN__c'
						,Content_Details_FR__c = 'Content_Details_FR__c'
						,Content_Heading_EN__c = 'Content_Heading_EN__c'
						,Content_Heading_FR__c = 'Content_Heading_FR__c'
						,Content_Sub_Heading_EN__c = 'Content_Sub_Heading_EN__c'
						,Content_Sub_Heading_FR__c = 'Content_Sub_Heading_FR__c'
						,Faculty_Name__c = 'Faculty_Name__c'
						,Faculty_Title_EN__c = 'Faculty_Title_EN__c'
						,Faculty_Title_FR__c = 'Faculty_Title_FR__c'
						,Faculty_Detail_EN__c = 'Faculty_Detail_EN__c'
						,Faculty_Detail_FR__c = 'Faculty_Detail_FR__c'
					)
				);
			}
		}
		insert newContents;

		List<Junction_Content_Category__c> newJunctions = new List<Junction_Content_Category__c>();
		for(Campus_Content__c thisContent: newContents) {
			newJunctions.add(
				new Junction_Content_Category__c(
					Campus_Category__c = contentCategory
					,Campus_Content__c = thisContent.Id
				)
			);
		}
		insert newJunctions;
	}

	public static User buildUser() {
		Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roles = [SELECT Id FROM UserRole LIMIT 1];
		ID roleId;
		if(roles.size()>0){
			roleId=roles[0].Id;
		}

		User newUser = new User(
			Firstname  = 'Test',
			Lastname = 'Admin',
			Alias='tsr',
			Email='tst+email@test.com',
			Username = 'tsr@test.com',
			CommunityNickname = 'tsrCommunity',
			EmailEncodingKey = 'ISO-8859-1',
			TimeZoneSidKey = 'America/New_York',
			LocaleSidKey = 'fr_CA',
			LanguageLocaleKey = 'fr',
			ProfileId = p.Id,
			UserRoleId = roleId			
		);
		
		return newUser;
	}
}