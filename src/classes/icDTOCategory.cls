global with sharing class icDTOCategory {
	@AuraEnabled global String id {get;set;}
	@AuraEnabled global String parent {get;set;}
	@AuraEnabled global String name {get;set;}
	@AuraEnabled global String label {get;set;}
}