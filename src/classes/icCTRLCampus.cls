global with sharing class icCTRLCampus {

	static icBusinessLogicCampusCategory.IClass blCategory = (icBusinessLogicCampusCategory.IClass) icObjectFactory.getSingletonInstance('icBusinessLogicCampusCategory');
	static icBusinessLogicCampusContent.IClass blContent = (icBusinessLogicCampusContent.IClass) icObjectFactory.getSingletonInstance('icBusinessLogicCampusContent');

	@AuraEnabled
	global static Map<String, List<icDTOCategory>> getNavigationCategories(){ 
		List<Campus_Category__c> listCampusCategories = blCategory.getAllActiveCampusCategory();
		return blCategory.getMapCampusCategoriesFromList(listCampusCategories);
	}

	@AuraEnabled
	global static Map<String, List<icDTOContent>> getCategoryContent(String categoryId){ 
		List<Junction_Content_Category__c> junctions = blContent.getCampusContentJunctionByCategory(categoryId);
		return blContent.getMapCampusContentFromList(junctions);
	}

	@AuraEnabled
	global static Map<String, List<icDTOContent>> getWelcomeContent(String categoryId){ 
		List<Junction_Content_Category__c> junctions = blContent.getCampusWelcomeContentJunctionByCategory(categoryId);
		System.debug('getWelcomeContent == ' + junctions);
		return blContent.getMapCampusWelcomeContentFromList(junctions);
	}
}