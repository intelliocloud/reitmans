global with sharing class icDTOContent {
	@AuraEnabled global String id {get;set;}
	@AuraEnabled global String recordType {get;set;}
	@AuraEnabled global String imageLocation {get;set;}	
	@AuraEnabled global String name {get;set;}
	@AuraEnabled global String title {get;set;}
	@AuraEnabled global String subTitle {get;set;}
	@AuraEnabled global String contentType {get;set;}
	@AuraEnabled global String shortDescription {get;set;}
	@AuraEnabled global String description {get;set;}
	@AuraEnabled global String cost {get;set;}
	@AuraEnabled global String eligibility {get;set;}
	@AuraEnabled global String howToRegister {get;set;}
	@AuraEnabled global String location {get;set;}
	@AuraEnabled global String partners {get;set;}
	@AuraEnabled global String syllabus {get;set;}
	@AuraEnabled global String targetAudience {get;set;}
	@AuraEnabled global String timeCommitment {get;set;}
	@AuraEnabled global String video {get;set;}
	@AuraEnabled global String article {get;set;}	
}