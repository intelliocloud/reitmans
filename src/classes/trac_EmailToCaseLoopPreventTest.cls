/**
 *  @description Tests for Email To Case Functionality
 *  @author      Graham Barnard, Traction on Demand.
 *  @date        2016-01-14
 */
@isTest
private class trac_EmailToCaseLoopPreventTest {

    /**
     *  @description Verify functionality doesn't run on other origin statuses
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-01-14
     */
    @isTest static void testInvalidOriginCaseLoop() {
        DateTime dt = Datetime.newInstanceGmt(2013, 01, 01);
        Case oldCase = new Case(Subject = 'Case 1.1', SuppliedEmail = 'case1@email.com', Origin = 'Email', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert oldCase;
        Test.setCreatedDate(oldCase.Id, dt);

        //String caseStaticResource = 'trac_EmailToCaseLoopPreventTest_CaseData';
        //List<sObject> caseData = Test.loadData(Case.sObjectType, caseStaticResource);
        //List<Case> clist = (List<Case>)caseData;
            
        Test.startTest();
        Case aCase1 = new Case(Subject = 'Case 1.2', SuppliedEmail = 'case1@email.com', Origin = 'Phone', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert aCase1;

        Case aCase2 = new Case(Subject = 'Case 1.3', SuppliedEmail = 'case1@email.com', Origin = 'Phone', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert aCase2;
        Test.stopTest();

        Map<Id,Case> allCases = new Map<Id,Case>([SELECT Id, CreatedDate, Don_t_Send_Auto_Response__c FROM Case]);
        System.assertEquals(false, allCases.get(oldCase.Id).Don_t_Send_Auto_Response__c);
        //System.assertEquals(false, allCases.get(cList[0].Id).Don_t_Send_Auto_Response__c);
        System.assertEquals(false, allCases.get(aCase1.Id).Don_t_Send_Auto_Response__c);
        System.assertEquals(false, allCases.get(aCase2.Id).Don_t_Send_Auto_Response__c);
    }

    /**
     *  @description Verify functionality doesn't run on other origin statuses
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-01-14
     */
    @isTest static void testInvalidEmailCaseLoop() {
        DateTime dt = Datetime.newInstanceGmt(2013, 01, 01);
        Case oldCase = new Case(Subject = 'Case 1.1', SuppliedEmail = 'case1@email.com', Origin = 'Email', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert oldCase;
        Test.setCreatedDate(oldCase.Id, dt);

        //String caseStaticResource = 'trac_EmailToCaseLoopPreventTest_CaseData';
        //List<sObject> caseData = Test.loadData(Case.sObjectType, caseStaticResource);
        //List<Case> clist = (List<Case>)caseData;

        Test.startTest();
        Case aCase1 = new Case(Subject = 'Case 1.2', SuppliedEmail = 'case2@email.com', Origin = 'Email', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert aCase1;

        Case aCase2 = new Case(Subject = 'Case 1.3', SuppliedEmail = 'case3@email.com', Origin = 'Email', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert aCase2;
        Test.stopTest();

        Map<Id,Case> allCases = new Map<Id,Case>([SELECT Id, CreatedDate, Don_t_Send_Auto_Response__c FROM Case]);
        //System.assertEquals(false, allCases.get(cList[0].Id).Don_t_Send_Auto_Response__c);
        System.assertEquals(false, allCases.get(oldCase.Id).Don_t_Send_Auto_Response__c);
        System.assertEquals(false, allCases.get(aCase1.Id).Don_t_Send_Auto_Response__c);
        System.assertEquals(false, allCases.get(aCase2.Id).Don_t_Send_Auto_Response__c);
    }

    /**
     *  @description Verify the functionality runs without a proper custom setting
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-01-14
     */
    @isTest static void testValidCaseLoop() {
        DateTime dt = Datetime.newInstanceGmt(2013, 01, 01);
        Case oldCase = new Case(Subject = 'Case 1.1', SuppliedEmail = 'case1@email.com', Origin = 'Email', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert oldCase;
        Test.setCreatedDate(oldCase.Id, dt);

        //String caseStaticResource = 'trac_EmailToCaseLoopPreventTest_CaseData';
        //List<sObject> caseData = Test.loadData(Case.sObjectType, caseStaticResource);
        //List<Case> clist = (List<Case>)caseData;

        Test.startTest();
        Case aCase1 = new Case(Subject = 'Case 1.2', SuppliedEmail = 'case1@email.com', Origin = 'Email', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert aCase1;

        Case aCase2 = new Case(Subject = 'Case 1.3', SuppliedEmail = 'case1@email.com', Origin = 'Email', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert aCase2;
        Test.stopTest();

        Map<Id,Case> allCases = new Map<Id,Case>([SELECT Id, CreatedDate, Don_t_Send_Auto_Response__c FROM Case]);
        //System.assertEquals(false, allCases.get(cList[0].Id).Don_t_Send_Auto_Response__c);
        System.assertEquals(false, allCases.get(oldCase.Id).Don_t_Send_Auto_Response__c);
        System.assertEquals(false, allCases.get(aCase1.Id).Don_t_Send_Auto_Response__c);
        System.assertEquals(true, allCases.get(aCase2.Id).Don_t_Send_Auto_Response__c);
    }

    /**
     *  @description Verify the functionality runs with a proper custom setting
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-01-14
     */
    @isTest static void testValidCustomSetting() {
        Email_To_Case_Loop_Prevent__c loopPreventCustomSetting = new Email_To_Case_Loop_Prevent__c();
        loopPreventCustomSetting.Interval__c = 60;
        insert loopPreventCustomSetting;

        //String caseStaticResource = 'trac_EmailToCaseLoopPreventTest_CaseData';
        //List<sObject> caseData = Test.loadData(Case.sObjectType, caseStaticResource);
        //List<Case> clist = (List<Case>)caseData;

        DateTime dt = Datetime.newInstanceGmt(2013, 01, 01);
        Case oldCase = new Case(Subject = 'Case 1.1', SuppliedEmail = 'case1@email.com', Origin = 'EMAIL', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert oldCase;
        Test.setCreatedDate(oldCase.Id, dt);

        Test.startTest();
        Case aCase1 = new Case(Subject = 'Case 1.2', SuppliedEmail = 'case1@email.com', Origin = 'Email', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert aCase1;


        Case aCase2 = new Case(Subject = 'Case 1.3', SuppliedEmail = 'case1@email.com', Origin = 'Email', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert aCase2;
        Test.stopTest();

        Map<Id,Case> allCases = new Map<Id,Case>([SELECT Id, CreatedDate, Don_t_Send_Auto_Response__c FROM Case]);
        System.assertEquals(false, allCases.get(oldCase.Id).Don_t_Send_Auto_Response__c);
        //System.assertEquals(false, allCases.get(cList[0].Id).Don_t_Send_Auto_Response__c);
        System.assertEquals(false, allCases.get(aCase1.Id).Don_t_Send_Auto_Response__c);
        System.assertEquals(true, allCases.get(aCase2.Id).Don_t_Send_Auto_Response__c);
    }

    /**
     *  @description Verify the functionality runs with a negative custom setting
     *  @author      Graham Barnard, Traction on Demand.
     *  @date        2016-01-14
     */
    @isTest static void testNegativeCustomSetting() {
        Email_To_Case_Loop_Prevent__c loopPreventCustomSetting = new Email_To_Case_Loop_Prevent__c();
        loopPreventCustomSetting.Interval__c = -1;
        insert loopPreventCustomSetting;

        //String caseStaticResource = 'trac_EmailToCaseLoopPreventTest_CaseData';
        //List<sObject> caseData = Test.loadData(Case.sObjectType, caseStaticResource);
        //List<Case> clist = (List<Case>)caseData;
        
        DateTime dt = Datetime.newInstanceGmt(2013, 01, 01);
        Case oldCase = new Case(Subject = 'Case 1.1', SuppliedEmail = 'case1@email.com', Origin = 'Email', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert oldCase;
        Test.setCreatedDate(oldCase.Id, dt);

        Test.startTest();
        Case aCase1 = new Case(Subject = 'Case 1.2', SuppliedEmail = 'case1@email.com', Origin = 'Email', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert aCase1;


        Case aCase2 = new Case(Subject = 'Case 1.3', SuppliedEmail = 'case1@email.com', Origin = 'Email', Category__c = 'Gift Card', Sub_Category__c = 'Activation');
        insert aCase2;
        Test.stopTest();

        Map<Id,Case> allCases = new Map<Id,Case>([SELECT Id, CreatedDate, Don_t_Send_Auto_Response__c FROM Case]);
        System.assertEquals(false, allCases.get(oldCase.Id).Don_t_Send_Auto_Response__c);
        //System.assertEquals(false, allCases.get(cList[0].Id).Don_t_Send_Auto_Response__c);
        System.assertEquals(false, allCases.get(aCase1.Id).Don_t_Send_Auto_Response__c);
        System.assertEquals(false, allCases.get(aCase2.Id).Don_t_Send_Auto_Response__c);
    }
}