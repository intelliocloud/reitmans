public with sharing class trac_Case {

  public String bannerType{get;set;}
  public String bannerLang{get;set;} 
  
  private final static String LANGUAGE_ENG ='Eng';
  private final static String LANGUAGE_FR ='Fr';

  private final static String REITMANS ='Reitmans';
  private final static String REITMANS_PAGENAME_ENG ='ContactUsReitmansEng';
  private final static String REITMANS_PAGENAME_FR ='ContactUsReitmansFR';
  private final static String ADDITIONELLE ='Addition Elle';
  private final static String ADDITIONELLE_PAGENAME_ENG ='ContactUsAdditionelleEng';
  private final static String ADDITIONELLE_PAGENAME_FR ='ContactUsAdditionelleFr';
  private final static String PENNINGTON ='Penningtons';
  private final static String PENNINGTON_PAGENAME_ENG ='ContactUsPenningtonEng';
  private final static String PENNINGTON_PAGENAME_FR ='ContactUsPenningtonFr';
  private final static String RWCO ='RW&Co';
  private final static String RWCO_PAGENAME_ENG ='ContactUsRwcoEng';
  private final static String RWCO_PAGENAME_FR ='ContactUsRwcoFr';
  private final static String THYME ='Thyme';
  private final static String THYME_PAGENAME_ENG ='ContactUsThymeEng';
  private final static String THYME_PAGENAME_FR ='ContactUsThymeFr';

    public Case newCase{get;set;}
    public Attachment myAttachment{get;set;}
    public String fileName{get;set;} 
    public Blob fileBody{get;set;}
  public String firstName{get;set;} 
  public String lastName{get;set;} 


    public trac_Case()
    {
        newCase = new Case();
        myAttachment = new Attachment();
    }
    public pagereference save()
    {
        System.debug('fileName: ' + fileName);
        if(lastName == '' || lastName == null 
           || firstName == '' || firstName == null 
           || newCase.SuppliedEmail == '' || newCase.SuppliedEmail == null 
           || newCase.Subject == '' || newCase.Subject == null
           || newCase.Description == '' || newCase.Description == null){
          if (bannerLang == LANGUAGE_FR) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'* s\'il vous plaît completer tous les champs obligatoire.'));
          } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'* Please enter all the required fields.'));
          }
          return null;
        }
        
        //order number validation
        if(
              ((newCase.Web_Category__c=='An online Order you placed?') || 
              (newCase.Web_Category__c=='Tracking the Shipment for your online order?') ||
              (newCase.Web_Category__c=='Online order returns and credits?') ||
              (newCase.Web_Category__c=='an order placed on store')) &&
              (newCase.Order_Number__c == '' || newCase.Order_Number__c == null))
          {
              
                  if (bannerLang == LANGUAGE_FR) {        
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Oops, il manque votre numéro de commande!'));
                  } else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Oups, the order number is missing!'));
                  }
                  return null;
        }

        //style or product number validation
        if(
              ((newCase.Web_Category__c=='Product details and availability') ||
              (newCase.Web_Category__c=='Quality of an item purchased?')) &&
              (newCase.Product_Number_SKU__c== '' || newCase.Product_Number_SKU__c== null))
          {
              
                  if (bannerLang == LANGUAGE_FR) {        
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Oops, il manque votre numéro de style ou numéro de produit!'));
                  } else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Oops, the style number or product number is missing!'));
                  }
                  return null;
        }
        
        //gift card number validation
        if(
              (newCase.Web_Category__c=='Gift card balance and replacement') &&
              (newCase.Gift_card_number__c== '' || newCase.Gift_card_number__c== null))
          {
              
                  if (bannerLang == LANGUAGE_FR) {        
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Oops, il manque votre numéro de la carte cadeau!'));
                  } else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Oops, the gift card number is missing!'));
                  }
                  return null;
        }
        
        //Loyalty program validation
        if(
              (newCase.Web_Category__c=='Your rewards points balance and redemptions?') &&
              (newCase.Loyalty_program_membership__c== '' || newCase.Loyalty_program_membership__c== null))
          {
              
                  if (bannerLang == LANGUAGE_FR) {        
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Oops, il manque votre numéro de membre du programme loyauté!'));
                  } else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Oops, your loyalty program membership number is missing!'));
                  }
                  return null;
        }
        newCase.SuppliedName = firstName + ' ' + lastName;
        insert newCase;
        System.debug('@@@@@fileBody'+fileBody);     
        myAttachment  = new Attachment();
        Integer i=0;
        myAttachment.clear();
        myAttachment.Body = fileBody; 
        myAttachment.Name = newCase.id+'.jpeg' ; 
        myAttachment.ParentId = newCase.id;   
        if (fileBody!=null){     
          insert myAttachment; 
        }    

        clearFields();

        // If bannertype is not null, assign redirect accordingly
        pagereference pr;
        if(bannerType == REITMANS && bannerLang == LANGUAGE_ENG){
          pr = new pagereference('/apex/'+REITMANS_PAGENAME_ENG);
        }else if(bannerType == REITMANS && bannerLang == LANGUAGE_FR){
          pr = new pagereference('/apex/'+REITMANS_PAGENAME_FR);
        }
        else if(bannerType == ADDITIONELLE && bannerLang == LANGUAGE_ENG){
          pr = new pagereference('/apex/'+ADDITIONELLE_PAGENAME_ENG);
        }else if(bannerType == ADDITIONELLE && bannerLang == LANGUAGE_FR){
          pr = new pagereference('/apex/'+ADDITIONELLE_PAGENAME_FR);
        }
        else if(bannerType == PENNINGTON && bannerLang == LANGUAGE_ENG){
          pr = new pagereference('/apex/'+PENNINGTON_PAGENAME_ENG);
        }else if(bannerType == PENNINGTON && bannerLang == LANGUAGE_FR){
          pr = new pagereference('/apex/'+PENNINGTON_PAGENAME_FR);
        }
        else if(bannerType == RWCO && bannerLang == LANGUAGE_ENG){
          pr = new pagereference('/apex/'+RWCO_PAGENAME_ENG);
        }else if(bannerType == RWCO && bannerLang == LANGUAGE_FR){
          pr = new pagereference('/apex/'+RWCO_PAGENAME_ENG);
        }
        else if(bannerType == THYME && bannerLang == LANGUAGE_ENG){
          pr = new pagereference('/apex/'+THYME_PAGENAME_ENG);
        }else if(bannerType == THYME && bannerLang == LANGUAGE_FR){
          pr = new pagereference('/apex/'+THYME_PAGENAME_FR);
        }                                   
        return pr;
    }

    private void clearFields(){
        newCase = new Case();
        myAttachment = new Attachment();
        fileName = ''; 
        fileBody = null;
        firstName = ''; 
        lastName = ''; 
    }
}