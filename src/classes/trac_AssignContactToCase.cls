/**
 *  @description Assigns the contact to a case for the case created via Web to case form,
 *					looks for an existing contact based on email and banner provided by user,
 *					if it doesn't exist, creates a new one
 *  @author 	 Tanminder Rai, Traction on Demand.
 *  @date        2016-02-03
 */
global with sharing class trac_AssignContactToCase {

	private static Id customerServiceRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Service RCL').getRecordTypeId();

	/**
	 *  @description On update, only run assignContact if the banner is added
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-02-03
	 */
	public static void onUpdate(Map<Id,Case> newCasesMap, Map<Id,Case> oldCasesMap){
        List<Case> changedCases = new List<Case>();
        for( Case newCase : newCasesMap.values() ) {
            Case oldCase = oldCasesMap.get( newCase.Id );
            if( String.isNotBlank(newCase.Banner__c) && String.isBlank(oldCase.Banner__c) ) {
                changedCases.add(newCase);
            }
        }
        if( !changedCases.isEmpty() ) {
            assignContact(changedCases);
        }
	}

	/**
	 *  @description Searches for contact, if not found, launches a future call
	 *					to search for contact in the external system
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-02-03
	 */
	public static void assignContact(List<Case> newCases) {
		List<Case> filteredCases 	= filterWebFormEmailCases(newCases);
		if(filteredCases.isEmpty()) return;

	    List<Case> casesToUpdate 				= new List<Case>();
		List<Case> casesToAddContact			= new List<Case>();
	    List<Contact> contactsToInsert 			= new List<Contact>();
		Map<String, Contact> emailToContactMap 	= new Map<String,Contact>();


	    for(Case caseObj : filteredCases) {
	    	String emailAdr 		= caseObj.SuppliedEmail;
	    	String banner 			= caseObj.Banner__c;
	    	List<Contact> queriedContacts 	= trac_ContactModel.findAllByEmailAddressAndBanner(emailAdr, banner);

	    	if(queriedContacts.size() >= 1) {
	    		Case updateCase = new Case(Id = caseObj.Id, ContactId = queriedContacts[0].Id);
	    		casesToUpdate.add(updateCase);
    		}
    		else if(queriedContacts.size() == 0) {
				Contact newContact = createContactFromCase(caseObj);
				contactsToInsert.add(newContact);
	    		emailToContactMap.put(emailAdr, newContact);
	    		casesToAddContact.add(caseObj);
	    	}
		}

    	if(!contactsToInsert.isEmpty()){
	    	insert contactsToInsert;
	    }

	    for(Case caseObj : casesToAddContact) {
	        Contact newContact = emailToContactMap.get(caseObj.SuppliedEmail);
	        Case c = new Case(Id = caseObj.Id, ContactId = newContact.Id);
	        casesToUpdate.add(c);
	    }

	    if(!casesToUpdate.isEmpty()){
	    	update casesToUpdate;
	    }

	    Set<Id> contactIds = trac_SObjectModel.getIds(emailToContactMap.values());
	    if(!contactIds.isEmpty()) {
	    	futureSyncContacts(contactIds);
	    }
	}

	/**
	 *  @description
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-02-03
	 */
	@future
	public static void futureSyncContacts(Set<Id> contactIds) {
		if(contactIds.isEmpty()) return;

	    List<Contact> contactsToUpdate = new List<Contact>();
	    List<Contact> contacts = trac_ContactModel.findAllUnsyncecByIds(contactIds);
		if(contacts.isEmpty()) return;

		for(Contact contact : contacts) {
	    	List<Ext_Contact__x> queriedExtContacts = new List<Ext_Contact__x>();
	        if(String.isNotBlank(contact.Email) && String.isNotBlank(contact.Banner__c)) {
	        	try {
	            	queriedExtContacts = trac_ExternalContactModel.findAllByEmailAndBanner(contact.Email, trac_ExternalCRM.BRAND_MAP.get(contact.Banner__c));
	    		}
	    		catch(Exception e) {
	    			System.debug('Exception' + e);
	    		}
	    	}

	    	contact.Synced__c = true;
			if(queriedExtContacts.size() >= 1) {
				Contact preSyncedContact = new Contact(FirstName = contact.FirstName, LastName = contact.LastName);
	    		contact = trac_SyncFromExternalObject.mapContact(contact, queriedExtContacts[0], contact.AccountId, false);
	    		if (String.isNotBlank(preSyncedContact.FirstName) && preSyncedContact.FirstName != contact.FirstName) {
		            contact.FirstName = preSyncedContact.FirstName;
		        }

		        if (String.isNotBlank(preSyncedContact.LastName) && preSyncedContact.LastName != contact.LastName) {
		            contact.LastName = preSyncedContact.LastName;
		        }
		    }

	    	contactsToUpdate.add(contact);
		}

	    if (!contactsToUpdate.isEmpty()){
	    	update contactsToUpdate;
	    }
	}


	/**
	 *  @description Gets the Web to case cases
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-02-03
	 */
	private static List<Case> filterWebFormEmailCases(List<Case> newCases){
	    List<Case> cases = new List<Case>();
	    for (Case caseObj:newCases) {
	    	Boolean isWebCase 		= caseObj.Origin == 'Web';
	    	Boolean isEmailCase 	= caseObj.Origin == 'Email';
	    	Boolean isLiveChatCase 	= caseObj.Origin == 'Live Chat';
	    	Boolean hasEmail 		= String.isNotBlank(caseObj.SuppliedEmail);
	    	Boolean hasBannerValue 	= String.isNotBlank(caseObj.Banner__c) ;

	        if ((isWebCase || isEmailCase ) && hasEmail && hasBannerValue){
	        	cases.add(caseObj);
	        }
	    }
	    return cases;
	}

	/**
	 *  @description Creates a contact from a case
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-02-03
	 */
	private static Contact createContactFromCase(Case caseObj){
		String firstName = '';
		String lastName = '';
		Boolean hasName = String.isNotBlank( caseObj.SuppliedName );
		if (hasName){
	        String[] nameParts = caseObj.SuppliedName.split(' ',2) ;
	        if (nameParts.size() == 2) {
	        	firstName = nameParts[0];
	        	lastName = nameParts[1];
	        }else if (nameParts.size() == 1){
	        	lastName = nameParts[0];
	        }
	    }

	    if(String.isBlank(lastName)){
	    	lastName = caseObj.SuppliedEmail.substringBefore('@');
	    }

        return new Contact(
        	FirstName 				= firstName,
			LastName  				= lastName,
			Email 					= caseObj.SuppliedEmail,
			Preferred_language__c 	= caseObj.Language__c,
			Banner__c 				= caseObj.Banner__c,
			Synced__c 				= false,
			RecordTypeId            = customerServiceRecordTypeId
		);
	}

	/**
	 *  @description Filters the cases to be reassigned
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-02-03
	 */
	public static void runAssignmentRules(Case[] newCases){
		Set<Id> caseIds = new Set<Id>();
		for(Case c : newCases){
			if (!c.Case_Reassigned__c && c.Priority != null && c.Status == 'New'){
				caseIds.add(c.Id);
			}
		}
		if(!caseIds.isEmpty()){
			futureRunAssignmentRules(caseIds);
		}
	}

	/**
	 *  @description Future method that re-runs the assignment rules
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-02-03
	 */
	@future
	public static void futureRunAssignmentRules(Set<Id> caseIds){
		
		List<Case>  newCases = trac_CaseModel.findAllByIds(caseIds);
		Database.DMLOptions dmlopts = new Database.DMLOptions();
		dmlopts.assignmentRuleHeader.useDefaultRule = true;

		List<Case> casesToUpdate = new List<Case>();
		for(Case c:newCases){
			Case newCase = new Case(Id=c.Id, Case_Reassigned__c = true);
			casesToUpdate.add(newCase);
		}

		if(!casesToUpdate.isEmpty()){
			Database.update(casesToUpdate, dmlopts);
		}
	}
}