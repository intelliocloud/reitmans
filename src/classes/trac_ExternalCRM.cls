/**
 *  @description Functionaly surrounding Contact object and External Web Service Updates
 *  @author 	 Graham Barnard, Traction on Demand.
 *  @date        2016-01-04
 */
public with sharing class trac_ExternalCRM {
	private static Integer CRM_DATABASE_GROUP_ID          = 200;
	private static Integer CRM_STORE_NUMBER               = 999;
	private static Integer CRM_CUSTOMER_NUMBER            = -1;
	private static Integer CRM_LANGUAGE_ID                = 1033;
	private static Integer CRM_CUSTOMER_ID                = -1;
	private static Integer CRM_SALES_ASSOCIATE_NUMBER     = 999999;
	private static Integer CRM_CUSTOMER_ADDRESS_ID        = -1;
	private static Integer CRM_CUSTOMER_ADDRESS_ID_UPDATE = 1;
	private static Integer CRM_DEFAULT_MAIL_INDICATOR     = 0;
	private static Integer CRM_DEFAULT_UNKNOWN_INDICATOR  = 9;
	private static Integer CRM_DEFAULT_VALUE              = 1;
	private static Integer CRM_MINIMUM_ADDRESS_LENGTH     = 5;

	private static String CRM_CUSTOMER_DEFAULT_OPT_IN            = '0';
	private static String CRM_CUSTOMER_OPT_IN                    = '1';
	private static String CRM_CUSTOMER_OPT_OUT                   = '2';
	private static String CRM_CUSTOMER_GENDER                    = 'U';
	private static String CRM_CUSTOMER_MARITAL_STATUS            = 'U';
	private static String CRM_CUSTOMER_MEMBERSHIP_TYPE           = 'REGL';
	private static String CRM_DEFAULT_LANGUAGE_CODE              = 'ENG';
	private static String CRM_DEFAULT_COUTNRY                    = 'CAN';
	private static String CRM_ADDED_RECORD_STATE                 = 'Added';
	private static String CRM_UNCHANGED_RECORD_STATE             = 'Unchanged';
	private static String CRM_MODIFIED_RECORD_STATE              = 'Modified';
	private static String CRM_CUSTOMER_DEFAULT_ADRESS_TYPE       = 'HOME';
	private static String CRM_CUSTOMER_DEFAULT_STATUS_CODE       = 'A';
	private static String CRM_THYME_MEMBERSHIP_SEGMENTATION_FLAG = 'segmentation_flag_a';
	private static String CRM_THYME_MEMBERSHIP                   = 'Thyme ID';
	private static String CRM_MEMBERSHIP_GROUPING_CODE           = 'LOYC';
	private static String CRM_DEFAULT_BIRTHDAY_LABEL             = 'Date de Naiss./Bday';
	private static String CRM_DEFAULT_COMMENT                    = '';
	private static String CRM_DEFAULT_MEMBERSHIP_CODE            = '1';
	private static String CRM_TIMEZONE_EAST                      = 'America/New_York';

	private static String DATE_FORMAT = 'yyyy-MM-dd\'T\'HH:mm:ss';

    private final static Map<String, String> CRM_GENDER_MAP = new Map<String, String>{
        'Male'    => 'M',
        'Female'  => 'F',
        'Unknown' => 'U'
    };

	private final static Map<String, Integer> BANNER_TO_DATABASE = new Map<String, Integer>{
		'RW&Co'         => 203,
		'Thyme'         => 204,
		'Addition Elle' => 205,
		'Penningtons'   => 206,
		'Reitmans'      => 207,
		'Smart Set'     => 208,
		'Hyba'          => 209
    };

    private final static Map<String, String> MEMBERSHIP_TO_CODE = new Map<String, String>{
		'INSIDER (PEN)'    => '1',
		'PRIVILEGE (PEN)'  => '2',
		'ALL ACCESS (PEN)' => '3',
		'VIP (AE)'         => '1',
		'PLATINUM (AE)'    => '2'
    };

    public final static Map<String, String> BRAND_MAP = new Map<String, String>{
        'Addition Elle' => 'AE',
        'Hyba'          => 'HY',
        'Penningtons'   => 'PE',
        'Reitmans'      => 'RE',
        'RW&Co'         => 'RW',
        'Smart Set'     => 'SS',
        'Thyme'         => 'TH'
    };

    public final static Map<String, String> BANNER_CODE_TO_MEMBERSHIP = new Map<String, String>{
        'Penningtons1' => 'INSIDER (PEN)',
        'Penningtons2' => 'PRIVILEGE (PEN)',
        'Penningtons3' => 'ALL ACCESS (PEN)',
        'Addition Elle1' => 'VIP (AE)',
        'Addition Elle2' => 'PLATINUM (AE)'
    };

    public final static String LOYALTY_CODE = 'LOYC';

    public final static Set<String> MEMBERSHIP_BANNERS = new Set<String>{'Addition Elle', 'Penningtons'};

	private static String REGEX_NON_DIGITS = '[^0-9]';

	private Ext_Contact__x externalContact;
	private Contact contact;
	private Contact oldContact;

	private String existingCode;
	private String currentDate;

	private Boolean firstRun = false;

	


	/**
	 *  @description Initial Constructor
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-01-04
	 */
	public trac_ExternalCRM(Id contactId) {
		contact = trac_ContactModel.findById(contactId);
	}

	public trac_ExternalCRM(Contact unsavedContact) {
		contact = unsavedContact;
	}

	/**
	 *  @description Update Customer in External System with Membership change
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-04-14
	 */
	public Boolean save(Map<String, String> oldContactMap) {
		oldContact = new Contact();
		oldContact.Membership_Status__c = oldContactMap.get('Membership_Status__c');
		oldContact.Opt_In__c            = oldContactMap.get('Opt_In__c') == 'true' ? true : false;
		oldContact.Phone_Opt_In__c      = oldContactMap.get('Phone_Opt_In__c') == 'true' ? true : false;
		oldContact.Mail_Opt_In__c       = oldContactMap.get('Mail_Opt_In__c') == 'true' ? true : false;
		oldContact.MailingStreet        = oldContactMap.get('MailingStreet');
		oldContact.MailingState         = oldContactMap.get('MailingState');
		oldContact.MailingCountry       = oldContactMap.get('MailingCountry');
		oldContact.MailingCity          = oldContactMap.get('MailingCity');
		oldContact.MailingPostalCode    = oldContactMap.get('MailingPostalCode');
		oldContact.Phone                = oldContactMap.get('Phone');

		if (MEMBERSHIP_TO_CODE.containsKey(oldContact.Membership_Status__c)) {
			existingCode = MEMBERSHIP_TO_CODE.get(oldContact.Membership_Status__c);
		} else {
			existingCode = CRM_DEFAULT_MEMBERSHIP_CODE;
		}
		return save();
	}

	/**
	 *  
	 *  
	 *  @description Update Customer in External System
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-01-04
	 */
	public Boolean save() {
		DateTime dt = DateTime.now();
		currentDate = dt.format(DATE_FORMAT,CRM_TIMEZONE_EAST);

		Boolean isNewCustomer = false;
		Boolean checkDuplicates = false;

		if (contact.Customer_Number__c != null) {
			externalContact = trac_ExternalContactModel.findByCustomerNoAndBrand(Long.valueOf(contact.Customer_Number__c), BRAND_MAP.get(contact.Banner__c));
		}

		trac_CRM_SoapWebServices.CustomerRelationshipManagementWebServiceSoap soapClient = new trac_CRM_SoapWebServices.CustomerRelationshipManagementWebServiceSoap();

		trac_CRM_SoapWebServices.CustomerFKCollectionWS customerFKCollectionWS = soapClient.Save(
			getInitializeParameters(isNewCustomer),
			getCheckDuplicates(checkDuplicates),
			getCustomerWS(isNewCustomer),
			getCustomerAddressCollectionWS(isNewCustomer),
			getCustomerRemarkWS(),
			getCustomerAlternateKeyCollectionWS(isNewCustomer),
			getCustomerAttributeCollectionWS(isNewCustomer),
			getCustomerSegmentationWS(isNewCustomer)
		);

		return false;
	}

	/**
	 *  
	 *  @description Update Customer in External System
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-01-04
	 */
	public Boolean saveNewCustomer() {
		DateTime dt = DateTime.now();
		currentDate = dt.format(DATE_FORMAT,CRM_TIMEZONE_EAST);

		Boolean isNewCustomer = true;
		Boolean checkDuplicates = true;

		trac_CRM_SoapWebServices.CustomerRelationshipManagementWebServiceSoap soapClient = new trac_CRM_SoapWebServices.CustomerRelationshipManagementWebServiceSoap();

		trac_CRM_SoapWebServices.SaveNewCustomerResponse_element saveNewCustomerResponse = soapClient.SaveNewCustomer(
			getInitializeParameters(isNewCustomer),
			getCheckDuplicates(checkDuplicates),
			getCustomerWS(isNewCustomer),
			getCustomerAddressCollectionWS(isNewCustomer)
		);

		if (saveNewCustomerResponse != null && saveNewCustomerResponse.parameters != null && saveNewCustomerResponse.parameters.CustomerNumber != null) {
			contact.Customer_Number__c = String.valueOf(saveNewCustomerResponse.parameters.CustomerNumber);
			try {
				Ext_Contact__x externalContact = trac_ExternalContactModel.findByCustomerNoAndBrand(Long.valueOf(contact.Customer_Number__c), BRAND_MAP.get(contact.Banner__c));
				contact.Customer_Id__c = externalContact.customer_id__c;
				contact.External_ID__c = externalContact.brand_prefix__c + '-' + externalContact.customer_id__c;
			}
			catch (Exception e) {
				System.debug('e: ' + e.getStackTraceString());
			}
			if (!Test.isRunningTest()) {
				firstRun = true;
				save();
			}
			update contact;
		}

		return false;
	}

	/**
	 *  @description Build the Inititalize Parameters
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-01-05
	 */
	private trac_CRM_SoapWebServices.InitializeParameters getInitializeParameters(Boolean isNewCustomer) {
		trac_CRM_SoapWebServices.InitializeParameters parameters = new trac_CRM_SoapWebServices.InitializeParameters();
		parameters.DatabaseGroupID = BANNER_TO_DATABASE.get(contact.Banner__c);
        parameters.SalesAssociateNumber = CRM_SALES_ASSOCIATE_NUMBER;
        parameters.StoreNumber = CRM_STORE_NUMBER;
        parameters.CustomerNumber = (isNewCustomer) ? CRM_CUSTOMER_NUMBER  : Long.valueOf(contact.Customer_Number__c);
        parameters.LanguageID = CRM_LANGUAGE_ID;
		return parameters;
	}

	/**
	 *  @description Build the Check Duplicate Variable
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-01-05
	 */
	private Boolean getCheckDuplicates(Boolean checkDuplicates) {
		return checkDuplicates;
	}

	/**
	 *  @description Build the CustomerWS Variable
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-01-05
	 */
	private trac_CRM_SoapWebServices.CustomerWS getCustomerWS(Boolean isNewCustomer) {
		System.debug('currentDate: ' + currentDate);

		trac_CRM_SoapWebServices.CustomerWS customerWS = new trac_CRM_SoapWebServices.CustomerWS();
		customerWS.ID = CRM_CUSTOMER_ID;
		customerWS.Number_x = CRM_CUSTOMER_NUMBER;
		customerWS.Gender = (contact.Gender__c != null && CRM_GENDER_MAP.containsKey(contact.Gender__c)) ? CRM_GENDER_MAP.get(contact.Gender__c) : CRM_CUSTOMER_GENDER;
		customerWS.MaritalStatus = CRM_CUSTOMER_MARITAL_STATUS;

        customerWS.Number_x = (isNewCustomer) ? CRM_CUSTOMER_NUMBER  : Long.valueOf(contact.Customer_Number__c);
        customerWS.OriginalNumber = (isNewCustomer) ? CRM_CUSTOMER_NUMBER  : Long.valueOf(contact.Customer_Number__c);

		customerWS.StoreNumber = CRM_STORE_NUMBER;

        customerWS.FirstName = clean(contact.FirstName);
        customerWS.LastName = clean(contact.LastName);
        customerWS.EmailAddress = clean(contact.Email);
        customerWS.RecordState = (isNewCustomer) ? CRM_ADDED_RECORD_STATE : CRM_UNCHANGED_RECORD_STATE;

        if (externalContact != null && externalContact.membership_date__c != null) {
        	customerWS.MembershipDate = externalContact.membership_date__c.format(DATE_FORMAT);
        	if (externalContact.membership_type_code__c != null) {
        		customerWS.MembershipTypeCode = externalContact.membership_type_code__c;
        	}
        } else {
        	customerWS.MembershipDate = currentDate;
        }

        if (externalContact != null && externalContact.create_date__c != null && !isNewCustomer && !firstRun) {
        	customerWS.CreateDate = externalContact.create_date__c.format(DATE_FORMAT);
        }
        else {
        	customerWS.CreateDate = currentDate;
        }
        customerWS.LastUpdateDate = currentDate;

        customerWS.SalesAssociateNumber = CRM_SALES_ASSOCIATE_NUMBER;
        customerWS.NumberMailings = 0;
        customerWS.EmailIndicatorID = 0;
        customerWS.HeadOfHousehold = true;
        customerWS.StatusCode = CRM_CUSTOMER_DEFAULT_STATUS_CODE;
        customerWS.LanguageCode = contact.Preferred_Language__c != null ? contact.Preferred_Language__c : CRM_DEFAULT_LANGUAGE_CODE;
        if (contact.Created_Source__c != null) {
        	customerWS.CreateSource = contact.Created_Source__c;
        }

        trac_CRM_SoapWebServices.LanguageFKWS languageFKWS = new trac_CRM_SoapWebServices.LanguageFKWS();
        languageFKWS.RecordState = (isNewCustomer) ? CRM_ADDED_RECORD_STATE : CRM_UNCHANGED_RECORD_STATE;
        languageFKWS.RootLanguageCode = CRM_DEFAULT_LANGUAGE_CODE;
		customerWS.Language = languageFKWS;

        trac_CRM_SoapWebServices.EmployeeFKWS employeeFKWS = new trac_CRM_SoapWebServices.EmployeeFKWS();
        employeeFKWS.RecordState = (isNewCustomer) ? CRM_ADDED_RECORD_STATE : CRM_UNCHANGED_RECORD_STATE;
        employeeFKWS.Number_x = CRM_SALES_ASSOCIATE_NUMBER;
		customerWS.SalesAssociate = employeeFKWS;

        trac_CRM_SoapWebServices.CodeEntityFKWS codeEntityFKWS = new trac_CRM_SoapWebServices.CodeEntityFKWS();
		codeEntityFKWS.RecordState = (isNewCustomer) ? CRM_ADDED_RECORD_STATE : CRM_MODIFIED_RECORD_STATE;
		codeEntityFKWS.Code = CRM_CUSTOMER_DEFAULT_OPT_IN;
		customerWS.EmailOptInFlag = codeEntityFKWS;
        customerWS.EmailOptInFlagCode = contact.Opt_In__c ? CRM_CUSTOMER_OPT_IN : (oldContact != null && contact.Opt_In__c == false && oldContact.Opt_In__c != false) ? 
        CRM_CUSTOMER_OPT_OUT : (externalContact != null && externalContact.email_opt_in__c != null && 
			oldContact != null && contact.Opt_In__c == oldContact.Opt_In__c) ? String.valueOf(externalContact.email_opt_in__c) : CRM_CUSTOMER_DEFAULT_OPT_IN;

		customerWS.EmailOptInDate = (isNewCustomer || externalContact == null || externalContact.email_opt_in_date__c == null || 
			(oldContact != null && contact.Opt_In__c != false && oldContact.Opt_In__c == false)) ? currentDate : externalContact.email_opt_in_date__c.format(DATE_FORMAT);

		return customerWS;
	}

	/**
	 *  @description Build the CustomerAddressCollectionWS Variable
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-01-05
	 */
	private trac_CRM_SoapWebServices.CustomerAddressCollectionWS getCustomerAddressCollectionWS(Boolean isNewCustomer) {
	 	trac_CRM_SoapWebServices.CustomerAddressCollectionWS customerAddressCollectionWS = new trac_CRM_SoapWebServices.CustomerAddressCollectionWS();
	 	customerAddressCollectionWS.Items  = new trac_CRM_SoapWebServices.ArrayOfCustomerAddressWS();
	 	customerAddressCollectionWS.Items.CustomerAddressWS = new List<trac_CRM_SoapWebServices.CustomerAddressWS>();
	 	trac_CRM_SoapWebServices.CustomerAddressWS customerAddressWS = new trac_CRM_SoapWebServices.CustomerAddressWS();

	 	customerAddressWS.OriginalID = 1;
	 	String address1 = contact.MailingStreet != null && contact.MailingStreet.length() >= CRM_MINIMUM_ADDRESS_LENGTH ? contact.MailingStreet : null;

        customerAddressWS.Address1 = ((oldContact != null && contact.MailingStreet != null && contact.MailingStreet != oldContact.MailingStreet) || isNewCustomer || firstRun || externalContact == null) ? clean(address1) : externalContact.street_no__c;
        
        customerAddressWS.City = ((oldContact != null && contact.MailingCity != null && contact.MailingCity != oldContact.MailingCity) || isNewCustomer || firstRun || externalContact == null) ? clean(contact.MailingCity) : externalContact.city__c;
        
        customerAddressWS.State = ((oldContact != null && contact.MailingState != null && contact.MailingState != oldContact.MailingState) || isNewCustomer || firstRun || externalContact == null) ? clean(contact.MailingState) : externalContact.province__c;
        
        if ((oldContact != null && contact.MailingCountry != null && contact.MailingCountry != oldContact.MailingCountry) || isNewCustomer || firstRun || externalContact == null) {
        	customerAddressWS.Address5 =  contact.MailingCountry != null ? contact.MailingCountry : CRM_DEFAULT_COUTNRY;
        	customerAddressWS.CountryCode = contact.MailingCountry != null ? contact.MailingCountry : CRM_DEFAULT_COUTNRY;
        }
        else {
        	customerAddressWS.Address5 = externalContact.country_code__c;
        	customerAddressWS.CountryCode = externalContact.country_code__c;
        }
        
        customerAddressWS.PostalCode = ((oldContact != null && contact.MailingPostalCode != null && contact.MailingPostalCode != oldContact.MailingPostalCode) || isNewCustomer || firstRun || externalContact == null) ? contact.MailingPostalCode : externalContact.post_code__c;
   		
   		if ((oldContact != null && contact.Phone != null && contact.Phone != oldContact.Phone) || isNewCustomer || firstRun || externalContact == null) {
   			customerAddressWS.TelephoneNumber = (contact.Phone != null) ? contact.Phone.replaceAll(REGEX_NON_DIGITS, '') : '';
   		}
   		else {
   			customerAddressWS.TelephoneNumber = externalContact.telephone_no__c;
   		}
   		
   		customerAddressWS.PhoneOptInDate = currentDate;
		customerAddressWS.PhoneIndicatorID = CRM_DEFAULT_UNKNOWN_INDICATOR;

 		customerAddressWS.RecordState = (isNewCustomer || !isAddressUpdated()) ? CRM_UNCHANGED_RECORD_STATE : CRM_MODIFIED_RECORD_STATE;
 		Integer addressId = getAddressIdByContact();
        customerAddressWS.ID = (isNewCustomer) ? CRM_CUSTOMER_ADDRESS_ID : addressId != null ? addressId : CRM_CUSTOMER_ADDRESS_ID_UPDATE;
		customerAddressWS.MailOptInDate = currentDate;
		customerAddressWS.Longitude = 0;
		customerAddressWS.Latitude = 0;

        customerAddressWS.AddressTypeCode = CRM_CUSTOMER_DEFAULT_ADRESS_TYPE;
        customerAddressWS.MailIndicatorID = CRM_DEFAULT_UNKNOWN_INDICATOR;

        customerAddressWS.EffectiveDate = currentDate;
		customerAddressWS.ExpiryDate = currentDate;
		customerAddressWS.DateLastModified = currentDate;
		customerAddressWS.NcoaDate = currentDate;

		customerAddressWS.ActiveFlag = true;
		customerAddressWS.RecurringFlag = false;

	 	customerAddressCollectionWS.Items.CustomerAddressWS.add(customerAddressWS);
        customerAddressCollectionWS.CustomerNumber = (isNewCustomer) ? CRM_CUSTOMER_NUMBER  : Long.valueOf(contact.Customer_Number__c);

        customerAddressWS.PhoneOptInFlagCode = contact.Phone_Opt_In__c ? CRM_CUSTOMER_OPT_IN : (oldContact != null && contact.Phone_Opt_In__c == false && oldContact.Phone_Opt_In__c != false) ? 
        CRM_CUSTOMER_OPT_OUT : (externalContact != null && externalContact.phone_opt_in_flag__c != null && 
			oldContact != null && contact.Phone_Opt_In__c == oldContact.Phone_Opt_In__c) ? String.valueOf(externalContact.phone_opt_in_flag__c) : CRM_CUSTOMER_DEFAULT_OPT_IN;

        customerAddressWS.MailOptInFlagCode = contact.Mail_Opt_In__c ? CRM_CUSTOMER_OPT_IN : (oldContact != null && contact.Mail_Opt_In__c == false && oldContact.Mail_Opt_In__c != false) ? 
        CRM_CUSTOMER_OPT_OUT : (externalContact != null && externalContact.mail_opt_in_flag__c != null && 
			oldContact != null && contact.Mail_Opt_In__c == oldContact.Mail_Opt_In__c) ? String.valueOf(externalContact.mail_opt_in_flag__c) : CRM_CUSTOMER_DEFAULT_OPT_IN;

	 	return customerAddressCollectionWS;
	 }

	/**
	 *  @description Build the CustomerRemarkWS Variable
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-01-05
	 */
	 private trac_CRM_SoapWebServices.CustomerRemarkWS getCustomerRemarkWS() {
	 	trac_CRM_SoapWebServices.CustomerRemarkWS customerRemarkWS = new trac_CRM_SoapWebServices.CustomerRemarkWS();
	 	return customerRemarkWS;
	 }

	/**
	 *  @description Build the CustomerAlternateKeyCollectionWS Variable
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-01-05
	 */
	 private trac_CRM_SoapWebServices.CustomerAlternateKeyCollectionWS getCustomerAlternateKeyCollectionWS(Boolean isNewCustomer) {
	 	trac_CRM_SoapWebServices.CustomerAlternateKeyCollectionWS customerAlternateKeyCollectionWS = new trac_CRM_SoapWebServices.CustomerAlternateKeyCollectionWS();
        customerAlternateKeyCollectionWS.CustomerNumber = (isNewCustomer) ? CRM_CUSTOMER_NUMBER  : Long.valueOf(contact.Customer_Number__c);
	 	return customerAlternateKeyCollectionWS;
	 }

	/**
	 *  @description Build the CustomerAttributeCollectionWS Variable
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-01-05
	 */
	 private trac_CRM_SoapWebServices.CustomerAttributeCollectionWS getCustomerAttributeCollectionWS(Boolean isNewCustomer) {
	 	trac_CRM_SoapWebServices.CustomerAttributeCollectionWS customerAttributeCollectionWS = new trac_CRM_SoapWebServices.CustomerAttributeCollectionWS();
	 	customerAttributeCollectionWS.CustomerNumber = Decimal.valueOf(contact.Customer_Number__c);
        if (MEMBERSHIP_BANNERS.contains(contact.Banner__c)) {

        	String membershipCode = CRM_DEFAULT_MEMBERSHIP_CODE;
        	if (MEMBERSHIP_TO_CODE.containsKey(contact.Membership_Status__c)) {
        		membershipCode = MEMBERSHIP_TO_CODE.get(contact.Membership_Status__c);
        	}

        	customerAttributeCollectionWS.Items = new trac_CRM_SoapWebServices.ArrayOfCustomerAttributeWS();
	        customerAttributeCollectionWS.Items.CustomerAttributeWS = new List<trac_CRM_SoapWebServices.CustomerAttributeWS>();
	        trac_CRM_SoapWebServices.CustomerAttributeWS customerAttributeWS = new trac_CRM_SoapWebServices.CustomerAttributeWS();
	        customerAttributeWS.RecordState = (firstRun) ? CRM_ADDED_RECORD_STATE : CRM_MODIFIED_RECORD_STATE;
	        customerAttributeWS.GroupCode = CRM_MEMBERSHIP_GROUPING_CODE;
	        customerAttributeWS.Code = membershipCode;
	        if (!firstRun && existingCode != null) {
	        	customerAttributeWS.OriginalGroupCode = CRM_MEMBERSHIP_GROUPING_CODE;
	        	customerAttributeWS.OriginalCode = existingCode;
	        }
	        customerAttributeWS.Value = CRM_DEFAULT_VALUE;
	        customerAttributeWS.Date_x = currentDate;
	        customerAttributeWS.Comment = CRM_DEFAULT_COMMENT;
	        customerAttributeCollectionWS.Items.CustomerAttributeWS.add(customerAttributeWS);
        }
        return customerAttributeCollectionWS;
	 }

	/**
	 *  @description Build the CustomerSegmentationWS Variable
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-01-05
	 */
	 private trac_CRM_SoapWebServices.CustomerSegmentationWS getCustomerSegmentationWS(Boolean isNewCustomer) {
	 	trac_CRM_SoapWebServices.CustomerSegmentationWS customerSegmentationWS = new trac_CRM_SoapWebServices.CustomerSegmentationWS();
        customerSegmentationWS.CustomerNumber = (isNewCustomer) ? CRM_CUSTOMER_NUMBER  : Long.valueOf(contact.Customer_Number__c);
       	customerSegmentationWS.RecordState = (isNewCustomer) ? CRM_ADDED_RECORD_STATE : CRM_UNCHANGED_RECORD_STATE;
       	if (contact.Birthdate != null) {
       		customerSegmentationWS.CustomerSegmentationDates = new trac_CRM_SoapWebServices.ArrayOfCustomerSegmentationDateWS();
       		customerSegmentationWS.CustomerSegmentationDates.CustomerSegmentationDateWS = new List<trac_CRM_SoapWebServices.CustomerSegmentationDateWS>();
	       	trac_CRM_SoapWebServices.CustomerSegmentationDateWS customerSegmentationDateWS = new trac_CRM_SoapWebServices.CustomerSegmentationDateWS();
	       	customerSegmentationDateWS.RecordState = CRM_ADDED_RECORD_STATE;
	       	customerSegmentationDateWS.Code = CRM_CUSTOMER_DEFAULT_STATUS_CODE;

	       	Date d = contact.Birthdate;
			Datetime dt = Datetime.newInstance(d.year(), d.month(),d.day());
	       	customerSegmentationDateWS.Date_x = dt.format(DATE_FORMAT);
	       	customerSegmentationDateWS.SegmentationDate = new trac_CRM_SoapWebServices.SegmentationDateFKWS();
	       	customerSegmentationDateWS.SegmentationDate.RecordState = CRM_ADDED_RECORD_STATE;
	       	customerSegmentationDateWS.SegmentationDate.Code = CRM_CUSTOMER_DEFAULT_STATUS_CODE;

	       	Datetime dt2 = Datetime.newInstance(1900,1,1);
	       	customerSegmentationDateWS.SegmentationDate.DateDefault = dt2.format(DATE_FORMAT);
	       	customerSegmentationDateWS.SegmentationDate.Label = CRM_DEFAULT_BIRTHDAY_LABEL;
	       	customerSegmentationWS.CustomerSegmentationDates.CustomerSegmentationDateWS.add(customerSegmentationDateWS);
       	}
       	if (contact.Membership_Status__c == CRM_THYME_MEMBERSHIP) {
       		customerSegmentationWS.CustomerSegmentationFlags = new trac_CRM_SoapWebServices.ArrayOfCustomerSegmentationFlagWS();
	       	customerSegmentationWS.CustomerSegmentationFlags.CustomerSegmentationFlagWS = new List<trac_CRM_SoapWebServices.CustomerSegmentationFlagWS>();
	       	trac_CRM_SoapWebServices.CustomerSegmentationFlagWS customerSegmentationFlagWS = new trac_CRM_SoapWebServices.CustomerSegmentationFlagWS();
	       	customerSegmentationFlagWS.RecordState = CRM_ADDED_RECORD_STATE;
	       	customerSegmentationFlagWS.Code = CRM_CUSTOMER_DEFAULT_STATUS_CODE;
	       	customerSegmentationFlagWS.Flag = true;
	       	customerSegmentationFlagWS.SegmentationFlag = new trac_CRM_SoapWebServices.SegmentationFlagFKWS();
	       	customerSegmentationFlagWS.SegmentationFlag.RecordState = CRM_ADDED_RECORD_STATE;
	       	customerSegmentationFlagWS.SegmentationFlag.Code = CRM_CUSTOMER_DEFAULT_STATUS_CODE;
	       	customerSegmentationFlagWS.SegmentationFlag.FlagDefault = true;
	       	customerSegmentationFlagWS.SegmentationFlag.Label = CRM_THYME_MEMBERSHIP_SEGMENTATION_FLAG;
	       	customerSegmentationWS.CustomerSegmentationFlags.CustomerSegmentationFlagWS.add(customerSegmentationFlagWS);
       	}

	 	return customerSegmentationWS;
	 }

	private Integer getAddressIdByContact() {
	 	if (contact.Customer_Number__c != null && contact.Banner__c != null) {
	 		Ext_Contact__x xContact = trac_ExternalContactModel.findByCustomerNoAndBrand(Long.valueOf(contact.Customer_Number__c), contact.Banner__c);
	 		if (xContact != null && xContact.address_id__c != null) {
	 			return Integer.valueOf(xContact.address_id__c);
	 		}
	 	}
	 	return null;
	}

	private Boolean isAddressUpdated() {
		if (oldContact == null) {
			return true;
		}
		if (contact.MailingStreet != oldContact.MailingStreet) {
			return true;
		}
		if (contact.MailingCountry != oldContact.MailingCountry) {
			return true;
		}
		if (contact.MailingCity != oldContact.MailingCity) {
			return true;
		}
		if (contact.MailingPostalCode != oldContact.MailingPostalCode) {
			return true;
		}
		if (contact.Phone != oldContact.Phone) {
			return true;
		}
		if (contact.Phone_Opt_In__c != oldContact.Phone_Opt_In__c) {
			return true;
		}
		if (contact.Mail_Opt_In__c != oldContact.Mail_Opt_In__c) {
			return true;
		}
		return false;
	}

	private String clean(String textToClean) {
		if (textToClean == null) {
			return null;
		}
		textToClean = textToClean.replace('\r\n', ' ');
		textToClean = textToClean.replace('\n', ' ');
		textToClean = textToClean.replace('\r', ' ');
		textToClean = textToClean.replace('\t', ' ');
		textToClean = textToClean.replace('\\', ' ');
		return textToClean;
	}
}