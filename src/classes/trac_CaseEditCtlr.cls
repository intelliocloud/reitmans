/**
 *  @description Mass Case Editing and Email Sending Functionality
 *  @author 	 Jeremy Horan, Traction on Demand.
 *  @date        2016-09-28
 */
public with sharing class trac_CaseEditCtlr {
	private static final String CONSOLE_ORIGIN = '/500?isdtp=nv';
	private static final String MSG_VALIDATION = 'You must fill in the Category and Sub-Category fields.\nThey cannot be left blank';
	private static final String SUPPORT_EMAIL_ADDRESS = 'support@somewhere.com';

    public EmailMessage emailMsg {get; private set;}

    private OrgWideEmailAddress sender;
    public Id senderId{get;set;}

	private List<String> caseIdsToEdit;
	private Map<Id, EmailTemplate> emailTemplatesById;
	private Map<Id, OrgWideEmailAddress> orgWideEmailAddressById;

	public String selectedTemplateId{get;set;}

	public Boolean sendMassEmail{get;set;}

	public Case c {get;set;}

	public trac_CaseEditCtlr() {
		List<OrgWideEmailAddress> emails = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress Limit 1000];
    	orgWideEmailAddressById = new Map<Id, OrgWideEmailAddress>();
    	for (OrgWideEmailAddress email : emails) {
            orgWideEmailAddressById.put(email.Id, email);
        }
        if (orgWideEmailAddressById.values().size() > 0) {
        	senderId = orgWideEmailAddressById.values()[0].Id;
        }

		emailTemplatesById = new Map<Id, EmailTemplate>();
		for (EmailTemplate emailTemplate : [SELECT Id, Name, Body FROM EmailTemplate ORDER BY Name LIMIT 10000]) {
			emailTemplatesById.put(emailTemplate.Id, emailTemplate);
		}

		if (emailTemplatesById.values().size() > 0) {
			selectedTemplateId = emailTemplatesById.values()[0].Id;
		}

		c = new Case(Origin = 'Web');
		String idString = ApexPages.currentPage().getParameters().get('ids');
		caseIdsToEdit = idString.split(',');
        emailMsg = new EmailMessage();
        sendMassEmail = false;
	}

    public List<SelectOption> getTemplateOptions() {
        List<SelectOption> options = new List<SelectOption>();
        for (EmailTemplate emailTemplate : emailTemplatesById.values()) {
            options.add(new SelectOption(emailTemplate.Id, emailTemplate.Name));
        }
        return options;
    }

    public List<SelectOption> getEmailOptions() {
        List<SelectOption> options = new List<SelectOption>();
        for (OrgWideEmailAddress email : orgWideEmailAddressById.values()) {
            options.add(new SelectOption(email.Id, email.Address));
        }
        return options;
    }

    public PageReference editCases() {
    	List<Case> casesToEdit = new List<Case>();
    	for (String caseId : caseIdsToEdit) {
    		Case caseToEdit = new Case(Id = caseId);
    		if (c.Web_Category__c != null) {
		    	caseToEdit.Status = c.Status;
		    }

		    if (c.Web_Category__c != null) {
		    	caseToEdit.Web_Category__c = c.Web_Category__c;
		    }
		    
		    if (c.Category__c != null) {
		    	caseToEdit.Category__c = c.Category__c;
		    }

		    if (c.Sub_Category__c != null) {
		    	caseToEdit.Sub_Category__c = c.Sub_Category__c;
		    }

		    if (c.Sub_Sub_Category__c != null) {
		    	caseToEdit.Sub_Sub_Category__c = c.Sub_Sub_Category__c;
		    }

		    if (c.Resolution_Code__c != null) {
                caseToEdit.Resolution_Code__c = c.Resolution_Code__c;
            }

            if (c.Email_Message__c != null) {
                caseToEdit.Email_Message__c = c.Email_Message__c;
            }
            caseToEdit.OwnerId = UserInfo.getUserId();
		    casesToEdit.add(caseToEdit);
    	}

    	if (c.Category__c != null && c.Sub_Category__c != null) {
    		if (casesToEdit.size() > 0) {
	    		update casesToEdit;
	    	}

	    	if (sendMassEmail) {
	    		sendMassEmail();
	    	}
	
	    	return new PageReference(CONSOLE_ORIGIN);
    	}
    	else {
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,MSG_VALIDATION));
    	}

    	return null;
    }

    public PageReference cancel() {
		return new PageReference(CONSOLE_ORIGIN);
	}

    public Attachment attachment {
        get {
            if (attachment==null) {
                
                attachment = new Attachment();
            }
            return attachment;
        }
        set;
    }

    // send email message per the attributes specified by user.
    public PageReference sendMassEmail() {
    	Set<Id> caseIdsToSearch = new Set<Id>();
    	caseIdsToSearch.addAll((List<Id>)caseIdsToEdit);
    	List<Case> casesToEmail = trac_CaseModel.findAllByIds(caseIdsToSearch);

        try {

        	sender = orgWideEmailAddressById.get((Id)senderId);
        	List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();
            if (!String.isBlank(c.Email_Message__c)) {
                for (Case caseToEmail : casesToEmail) {
                    caseToEmail.Email_Message__c = c.Email_Message__c;
                }
                if (casesToEmail.size() > 0) {
                    update casesToEmail;
                }
            }

            for (Case caseToEmail : casesToEmail) {

                if (c.Email_Message__c != null) {
                    caseToEmail.Email_Message__c = c.Email_Message__c;

                }

		        if (caseToEmail.ContactId != null && caseToEmail.Contact.Email != null) {
		        	Messaging.SingleEmailMessage singleEmailMsg = new Messaging.SingleEmailMessage();
			        singleEmailMsg.setTemplateId(selectedTemplateId);

			        singleEmailMsg.setOrgWideEmailAddressId(sender.Id);
	
			        singleEmailMsg.setToAddresses(new List<String>{caseToEmail.Contact.Email});
			        singleEmailMsg.setReplyTo(sender.Address);
	
			        singleEmailMsg.setTargetObjectId(caseToEmail.ContactId);
	        		singleEmailMsg.setWhatId(caseToEmail.Id);

	        		if (emailMsg.BccAddress != null && emailMsg.BccAddress != '') {
		                singleEmailMsg.setBccAddresses(emailMsg.BccAddress.split(';'));
		            }
	
		            emailMessages.add(singleEmailMsg);
		        }
            }


            if (emailMessages.size() > 0) {
            	List<Messaging.SendEmailResult> results = Messaging.sendEmail(emailMessages);
	
	            // now parse  our results
	            // on success, return to calling page - Case view.
	            if (!results[0].success) {
	                // on failure, display error message on existing page so return null to return there.
	                String errorMsg = 'Error sending Email Message. Details = ' + results.get(0).getErrors()[0].getMessage();
	                
	                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg));
	                return null;
	            }
            }

            return null;
        }
        catch (Exception e) {
            // on failure, display error message on existing page so return null to return there.
            String errorMsg = 'Exception thrown trying to send Email Message. Details = ' + e;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg));
            return null;
        }

        return null;
    }

    // populate our Email Message Template from the database, filling merge fields with
    // values from our Case object. Then store resulting template in EmailMessage for
    // display to end-user who is free to edit it before sending.
    public PageReference populateTemplate() {
        // we need to perform the merge for this email template before displaying to end-user.

        EmailTemplate emailTemplate = [SELECT Body, HtmlValue, Subject, DeveloperName, BrandTemplateId 
            FROM EmailTemplate WHERE Id =: selectedTemplateId];

        // construct dummy email to have Salesforce merge BrandTemplate (HTML letterhead) with our email
        Messaging.SingleEmailMessage dummyEmailMsg = new Messaging.SingleEmailMessage();
        dummyEmailMsg.setTemplateId(emailTemplate.Id);
        // This ensures that sending this email is not saved as an activity for the targetObjectId. 
        dummyEmailMsg.setSaveAsActivity(false);

        // send dummy email to populate HTML letterhead in our EmailMessage object's html body.
        String[] toAddresses = new String[]{'admin@tractionondemand.com'};
        dummyEmailMsg.setToAddresses(toAddresses);
        dummyEmailMsg.setReplyTo(SUPPORT_EMAIL_ADDRESS); 
        
        // now send email and then roll it back but invocation of sendEmail() 
        // means merge of letterhead & body is done

        // TargetObject is User. This tells the emailMsg to use the email message
        // associated with our dummy User. This is necessary so we can populate our
        // email message body & subject with merge fields from template
        Savepoint sp = Database.setSavepoint();

        Account dummyAcct = new Account(Name='dummy account');
        insert dummyAcct;
        
        Contact dummyContact          = new Contact(AccountId=dummyAcct.Id);
        dummyContact.FirstName        = 'FirstName';
        dummyContact.LastName         = 'LastName';
        dummyContact.Email            = 'Email@Address.com';
        insert dummyContact;

        if (!String.isBlank(c.Email_Message__c)) {
            Case tempCase = new Case (
                Id = caseIdsToEdit[0],
                Email_Message__c = c.Email_Message__c,
                OwnerId = UserInfo.getUserId()
            );
            update tempCase;
        }

        dummyEmailMsg.setTargetObjectId(dummyContact.Id);
        dummyEmailMsg.setWhatId(caseIdsToEdit[0]);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {dummyEmailMsg});
        // now rollback our changes.
        Database.rollback(sp);

        // now populate our fields with values from SingleEmailMessage.
        emailMsg.BccAddress  = UserInfo.getUserEmail();
        emailMsg.Subject     = dummyEmailMsg.getSubject();
        emailMsg.TextBody    = dummyEmailMsg.getPlainTextBody();
        emailMsg.HtmlBody    = dummyEmailMsg.getHtmlBody();
        emailMsg.ToAddress   = dummyEmailMsg.getToAddresses().get(0);
        emailMsg.FromAddress = SUPPORT_EMAIL_ADDRESS; 
        emailMsg.CcAddress   = '';
        emailMsg.ParentId    = caseIdsToEdit[0];
        return null;
    }


}