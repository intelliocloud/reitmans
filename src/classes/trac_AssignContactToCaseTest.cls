/**
 *  @description Tests for trac_AssignContactToCase
 *  @author 	 Tanminder Rai, Traction on Demand.
 *  @date        2016-02-03
 */
@isTest
private class trac_AssignContactToCaseTest {

    /**
     *  @description Verify that new contact is created if contact doesn't exist
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-02-03
     */
    @isTest static void testContactGetsCreated() {
        Case c = new Case(
            SuppliedEmail = 'test_email@testEmail.com',
            SuppliedName = 'Test Name',
            Subject = 'Feedback - Something',
            Origin = 'Web',
            Banner__c = 'Reitmans',
            Category__c = 'Gift Card',
            Sub_Category__c = 'Activation'
        );

        Test.startTest();
        insert c;
        Test.stopTest();

        List<Contact> resultContacts = [select Id from Contact where Email='test_email@testEmail.com'];
        System.assertEquals(1, resultContacts.size(), 'There should be one Contact!');
        
        List<Case> resultCases = [select ContactId from Case where Id=:c.Id];
        System.assertEquals(resultContacts[0].Id, resultCases[0].ContactId, 'The contact id on the case should be populated.');
    }
 
     /**
     *  @description Verify that existing contact is assigned to case if exists
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-02-03
     */
    @isTest static void testContactIsAssignedIfExists() {
        Contact contact = new Contact(FirstName = 'firstName',
                                LastName = 'lastName',
                                Email='test_email@testEmail.com',
                                Banner__c='Reitmans');

        insert contact;

        Case case1 = new Case(
            SuppliedEmail = 'test_email@testEmail.com',
            SuppliedName = 'Test Name',
            Subject = 'Feedback - Something',
            Origin = 'Web',
            Banner__c = 'Reitmans',
            Category__c = 'Gift Card',
            Sub_Category__c = 'Activation'
        );

        insert case1;

        List<Contact> resultContacts = [select Id from Contact where Email='test_email@testEmail.com'];
        List<Case> resultCases = [Select ContactId From Case Where Id =: case1.Id];

        System.assertEquals(1 , resultContacts.size(),'There should be only one Contact Created!');
        System.assertEquals(contact.Id, resultCases[0].ContactId);
    }

     /**
     *  @description Verify that external contact is assigned to case if exists
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-02-03
     */
    @isTest static void testExternalContactIsAssignedIfExists() {
    	Ext_Contact__x externalContact = trac_TestUtils.setupCustomer();
        externalContact.Email_Address__c='test_email@testEmail.com';
    	trac_ExternalContactModel.mockExternalContacts.add(externalContact);

        Case case1 = new Case(
            SuppliedEmail = 'test_email@testEmail.com',
            SuppliedName = 'Test Name',
            Subject = 'Feedback - Something',
            Origin = 'Web',
            Banner__c = 'Reitmans',
            Category__c = 'Gift Card',
            Sub_Category__c = 'Activation'
        );

        Test.startTest();
        insert case1;
        Test.stopTest();

        List<Contact> resultContacts = [select Id, FirstName from Contact where Email='test_email@testEmail.com'];
        List<Case> resultCases = [Select ContactId From Case Where Id =: case1.Id];

        System.assertEquals(1 , resultContacts.size(),'There should be one Contact Created!');
        System.assertEquals('test', resultContacts[0].FirstName.toLowerCase(), 'Contact should have the details of the mock external contact');
    }


    /**
     *  @description Verify that sync is set to true by default
     *  @author      Tanminder Rai, Traction on Demand.
     *  @date        2016-02-03
     */
    @isTest static void testDefaultFutureSyncContacts() {
        Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        insert contact;

        Test.startTest();
        List<Contact> resultContacts = [SELECT Id, FirstName, Synced__c FROM Contact];
        System.assertEquals(1, resultContacts.size(), 'There should be one Contact!');
        System.assertEquals(resultContacts[0].Synced__c, true, 'Synced should be default to true');
        Test.stopTest();

        resultContacts = [SELECT Id, FirstName, Synced__c FROM Contact];
        System.assertEquals(1, resultContacts.size(), 'There should be one Contact!');
        System.assertEquals(resultContacts[0].Synced__c, true, 'Synced should be default to true');
    }

    /**
     *  @description Verify that the future job can still sync with no external object results
     *  @author      Tanminder Rai, Traction on Demand.
     *  @date        2016-02-03
     */
    @isTest static void testCaseCreationWithInvalidFutureSyncContacts() {
        Case case1 = new Case(
            SuppliedEmail = 'test_email@testEmail.com',
            SuppliedName = 'Test Name',
            Subject = 'Feedback - Something',
            Origin = 'Web',
            Banner__c = 'Reitmans',
            Category__c = 'Gift Card',
            Sub_Category__c = 'Activation'
        );

        Test.startTest();
        insert case1;

        List<Contact> resultContacts = [SELECT Id, FirstName, Synced__c FROM Contact];
        System.assertEquals(1, resultContacts.size(), 'There should be one Contact!');
        System.assertEquals(resultContacts[0].Synced__c, false, 'Synced should be default to true');
        Test.stopTest();

        resultContacts = [SELECT Id, FirstName, Synced__c FROM Contact];
        System.assertEquals(1, resultContacts.size(), 'There should be one Contact!');
        System.assertEquals(resultContacts[0].Synced__c, true, 'Synced should be default to true');
    }

    /**
     *  @description Verify that the future job will sync information over
     *  @author      Tanminder Rai, Traction on Demand.
     *  @date        2016-02-03
     */
    @isTest static void testCaseCreationValidFutureSyncContacts() {
        Case case1 = new Case(
            SuppliedEmail = 'test_email@testEmail.com',
            SuppliedName = 'Test Name',
            Subject = 'Feedback - Something',
            Origin = 'Web',
            Banner__c = 'Reitmans',
            Category__c = 'Gift Card',
            Sub_Category__c = 'Activation'
        );

        Test.startTest();
        insert case1;

        Ext_Contact__x externalContact = trac_TestUtils.setupCustomer();
        externalContact.email_address__c = 'test_email@testEmail.com';
        externalContact.first_name__c = 'sync succesful';
        externalContact.telephone_no__c = '5141234567';
        trac_ExternalContactModel.mockExternalContacts.add(externalContact);


        List<Contact> resultContacts = [SELECT Id, FirstName, Synced__c FROM Contact];
        System.assertEquals(1, resultContacts.size(), 'There should be one Contact!');
        System.assertEquals(resultContacts[0].Synced__c, false, 'Synced should be default to true');
        System.assertNotEquals(resultContacts[0].FirstName.toLowerCase(), 'sync succesful', 'First name has not been synced over yet');
        Test.stopTest();

        resultContacts = [SELECT Id, FirstName, Phone, Synced__c FROM Contact];
        System.assertEquals(1, resultContacts.size(), 'There should be one Contact!');
        System.assertEquals(resultContacts[0].Synced__c, true, 'Synced should be default to true');
        System.assertEquals(resultContacts[0].Phone, '5141234567', 'Phone synced over');
    }

     /**
     *  @description Bulk test
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-02-03
     */
    @isTest  static void testBulkContactsGetCreated() {
        List<Case> cases = new List<Case>();
        for (Integer i = 0; i<50; i++) {
            Case c = new Case(
                SuppliedEmail = 'test_email@testEmail.com' + i,
                SuppliedName = 'Test Name' + i,
                Subject = 'Feedback - Something' + i,
                Origin = 'Web',
                Banner__c = 'Reitmans',
                Category__c = 'Gift Card',
                Sub_Category__c = 'Activation'
            );
            cases.add(c);
        }
        Test.startTest();
        insert cases;
        Test.stopTest();
        
        List<Id> newCaseIds = new List<Id>();
        for (Case caseObj:cases) {
            newCaseIds.add(caseObj.Id);    
        }
        
        List<Case> updatedCases = [Select ContactId From Case Where Id in:newCaseIds];
        
        System.assert(updatedCases.size()>0);
        for (Case caseObj:updatedCases) {
            System.assert(caseObj.ContactId!=null,'There should be no null contacts');
        }
    }

    /**
     *  @description Verify that assignment rules update the ownerId
     *  @author      Tanminder Rai, Traction on Demand.
     *  @date        2016-02-03
     */
    @isTest static void testAssignmentRuleRuns() {
        Case case1 = new Case(
            Status = 'New',
            SuppliedEmail = 'test_email@testEmail.com',
            SuppliedName = 'Test Name',
            Subject = 'Feedback - Something',
            Origin = 'Web',
            Banner__c = 'Reitmans',
            Language__c = 'ENG',
            Priority = 'Medium',
            Category__c = 'Gift Card',
            Sub_Category__c = 'Activation'
        );
        insert case1;

        List<Case> caseBeforeUpdate = [Select OwnerId From Case Where Id =: case1.Id];

        case1.Priority='High';
        case1.Case_Reassigned__c=false;

        Test.startTest();
        update case1;
        Test.stopTest();

        List<Case> caseAfterUpdate = [Select OwnerId From Case Where Id =: case1.Id];

        System.assertNotEquals(caseBeforeUpdate[0].OwnerId, caseAfterUpdate[0].OwnerId, 'The owner should be updated!');
    }
}