/**
 *  @description Functionality for trac_Contact trigger
 *  @author 	 Jeremy Horan, Traction on Demand.
 *  @date        2016-03-17
 */
public with sharing class trac_Contact {

    private static Id customerServiceRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Service RCL').getRecordTypeId();

	/**
	 *  @description Create new Contacts in the External System.
	 *               Only used for manual entry. (API can't handle bulk and batch updates will loop)
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-03-17
	 */	
	public static void saveNewContacts(List<Contact> newContacts) {
        if (newContacts.size() == 1 && newContacts[0].Customer_Number__c == null && !Test.isRunningTest() && !System.isBatch() && newContacts[0].RecordTypeId == customerServiceRecordTypeId) {
            saveNewCustomer(newContacts[0].Id);
        }
    }

    /**
     *  @description Update existing Contacts in the External System
     *               Only used for manual entry. (API can't handle bulk)
     *  @author 	 Jeremy Horan, Traction on Demand.
     *  @date        2016-03-17
     */
    public static void saveExistingContacts(List<Contact> newContacts, Map<Id, Contact> oldMap) {
        if (newContacts.size() == 1 && newContacts[0].Customer_Number__c != null && oldMap.containsKey(newContacts[0].Id) && oldMap.get(newContacts[0].Id).Customer_Number__c != null && !Test.isRunningTest() && !System.isBatch() && newContacts[0].RecordTypeId == customerServiceRecordTypeId) {
            try {
                saveExistingCustomer(newContacts[0].Id, getOldValues(oldMap.get(newContacts[0].Id)));
            }
            catch (Exception e) {
                System.debug(e.getStackTraceString());
            }
            
        }
    }

    /**
     *  @description update Membership of Penn and Addition Elle contacts
     *  @author      Jeremy Horan, Traction on Demand.
     *  @date        2016-05-09
     */
    public static void updateMembership(List<Contact> newContacts) {
        Set<Id> filteredContactIds = new Set<Id>();
        for (Contact contact : newContacts) {
            if (trac_ExternalCRM.MEMBERSHIP_BANNERS.contains(contact.Banner__c) && contact.External_ID__c != null && contact.RecordTypeId == customerServiceRecordTypeId) {
                filteredContactIds.add(contact.Id);
            }
        }

        if (filteredContactIds.size() > 0) {
            futureUpdateMembership(filteredContactIds);
        }
    }

    /**
     *  @description update Membership of Penn and Addition Elle contacts
     *  @author      Jeremy Horan, Traction on Demand.
     *  @date        2016-05-09
     */
    public static void updateMembership(List<Contact> newContacts, Map<Id, Contact> oldMap) {
        Set<Id> filteredContactIds = new Set<Id>();
        for (Contact contact : newContacts) {
            if (trac_ExternalCRM.MEMBERSHIP_BANNERS.contains(contact.Banner__c) && contact.External_ID__c != null && contact.Membership_Status__c == oldMap.get(contact.Id).Membership_Status__c && contact.RecordTypeId == customerServiceRecordTypeId) {
                filteredContactIds.add(contact.Id);
            }
        }

        if (filteredContactIds.size() > 0 && !System.isFuture() && !System.isBatch() && !System.isScheduled()) {
            futureUpdateMembership(filteredContactIds);
        }
    }

    /**
     *  @description future method to reference external object to update Membership of Penn and Addition Elle contacts
     *  @author      Jeremy Horan, Traction on Demand.
     *  @date        2016-05-09
     */
    @future(callout=true)
    private static void futureUpdateMembership(Set<Id> contactIds) {

        List<Contact> filteredContacts = trac_ContactModel.findAllByIds(contactIds);

        List<String> externalIds = new List<String>();
        for (Contact filteredContact : filteredContacts) {
            externalIds.add(filteredContact.External_ID__c);
        }

        List<Ext_cust_attribute__x> externalContactAttributes = new List<Ext_cust_attribute__x>();
        if (externalIds.size() > 0) {
            externalContactAttributes = trac_ExternalCustomerAttributeModel.findAllByExternalIds(externalIds);
        }

        Map<String, Ext_cust_attribute__x> loyaltyAttributes = new Map<String, Ext_cust_attribute__x>();

        for (Ext_cust_attribute__x externalContactAttribute : externalContactAttributes) {
            if (externalContactAttribute.attribute_grouping_code__c == trac_ExternalCRM.LOYALTY_CODE) {
                loyaltyAttributes.put(externalContactAttribute.ExternalId, externalContactAttribute);
            }
        }

        List<Contact> contactsToUpdate = new List<Contact>();
        for (Contact filteredContact : filteredContacts) {
            if (loyaltyAttributes.containsKey(filteredContact.External_ID__c)) {
                if (loyaltyAttributes.get(filteredContact.External_ID__c).attribute_code__c != null) {
                    String membershipCode = filteredContact.Banner__c + loyaltyAttributes.get(filteredContact.External_ID__c).attribute_code__c;
                    if (trac_ExternalCRM.BANNER_CODE_TO_MEMBERSHIP.containsKey(membershipCode) && filteredContact.Membership_Status__c != trac_ExternalCRM.BANNER_CODE_TO_MEMBERSHIP.get(membershipCode)) {
                        filteredContact.Membership_Status__c = trac_ExternalCRM.BANNER_CODE_TO_MEMBERSHIP.get(membershipCode);
                        contactsToUpdate.add(filteredContact);
                    }
                }
            }
        }

        if (contactsToUpdate.size() > 0) {
            update contactsToUpdate;
        }
    }

    /**
     *  @description Future Callout to send SOAP call to External System to save a new customer
     *  @author 	 Jeremy Horan, Traction on Demand.
     *  @date        2016-03-23
     */
    @future(callout=true)
    private static void saveNewCustomer(String contactId) {
    	Boolean existsInExternalSystem = false;

        Contact contact = trac_ContactModel.findById(contactId);

        if (contact != null && !String.isEmpty(contact.Email) && !String.isEmpty(contact.Banner__c)) {
            List<Ext_Contact__x> externalContacts = new List<Ext_Contact__x>();
            try {
                externalContacts = trac_ExternalContactModel.findAllByEmailAndBanner(contact.Email, trac_ExternalCRM.BRAND_MAP.get(contact.Banner__c));
            }
            catch(Exception e) {
                System.debug('Exception: ' + e.getStackTraceString());
            }

            if (externalContacts.size() > 0) {
                Contact newContact = new Contact(FirstName = contact.FirstName, LastName = contact.LastName);
                existsInExternalSystem = true;

                contact = trac_SyncFromExternalObject.mapContact(contact, externalContacts[0], contact.AccountId, false);
                contact = updateNameFromWeb(contact, newContact);
                contact = updateMembership(contact);
                trac_ExternalCRM externalCRM = new trac_ExternalCRM(contact);
                externalCRM.save();
                update contact;
            }
        }

        if (!existsInExternalSystem) {
            trac_ExternalCRM externalCRM = new trac_ExternalCRM(contactId);
            externalCRM.saveNewCustomer();
        }
    }

    /**
     *  @description Future Callout to send SOAP call to External System to update an existing customer
     *  @author 	 Jeremy Horan, Traction on Demand.
     *  @date        2016-03-23
     */
    @future(callout=true)
    private static void saveExistingCustomer(String contactId, Map<String, String> oldContactMap) {
    	trac_ExternalCRM externalCRM = new trac_ExternalCRM(contactId);
        externalCRM.save(oldContactMap);
    }

    private static Map<String, String> getOldValues(Contact oldContact){
        Map<String, String> oldContactMap = new Map<String, String>();
        oldContactMap.put('Membership_Status__c', oldContact.Membership_Status__c);
        oldContactMap.put('Opt_In__c', oldContact.Opt_In__c ? 'true' : 'false');
        oldContactMap.put('Phone_Opt_In__c', oldContact.Phone_Opt_In__c ? 'true' : 'false');
        oldContactMap.put('Mail_Opt_In__c', oldContact.Mail_Opt_In__c ? 'true' : 'false');
        oldContactMap.put('MailingStreet', oldContact.MailingStreet);
        oldContactMap.put('MailingState', oldContact.MailingState);
        oldContactMap.put('MailingCountry', oldContact.MailingCountry);
        oldContactMap.put('MailingCity', oldContact.MailingCity);
        oldContactMap.put('MailingPostalCode', oldContact.MailingPostalCode);
        oldContactMap.put('Phone', oldContact.Phone);

        return oldContactMap;
    }

    private static Contact updateNameFromWeb(Contact contactAfterSync, Contact contactBeforeSync) {
        if (String.isNotBlank(contactBeforeSync.FirstName) && contactBeforeSync.FirstName != contactAfterSync.FirstName) {
            contactAfterSync.FirstName = contactBeforeSync.FirstName;
        }

        if (String.isNotBlank(contactBeforeSync.LastName) && contactBeforeSync.LastName != contactAfterSync.LastName) {
            contactAfterSync.LastName = contactBeforeSync.LastName;
        }

        return contactAfterSync;
    }

    private static Contact updateMembership(Contact contact) {
        if (trac_ExternalCRM.MEMBERSHIP_BANNERS.contains(contact.Banner__c) && contact.External_ID__c != null) {
            List<Ext_cust_attribute__x> externalContactAttributes = new List<Ext_cust_attribute__x>();
            externalContactAttributes = trac_ExternalCustomerAttributeModel.findAllByExternalIds(new List<String>{contact.External_ID__c});
    
            Map<String, Ext_cust_attribute__x> loyaltyAttributes = new Map<String, Ext_cust_attribute__x>();
    
            for (Ext_cust_attribute__x externalContactAttribute : externalContactAttributes) {
                if (externalContactAttribute.attribute_grouping_code__c == trac_ExternalCRM.LOYALTY_CODE) {
                    loyaltyAttributes.put(externalContactAttribute.ExternalId, externalContactAttribute);
                }
            }
    
            if (loyaltyAttributes.containsKey(contact.External_ID__c)) {
                if (loyaltyAttributes.get(contact.External_ID__c).attribute_code__c != null) {
                    String membershipCode = contact.Banner__c + loyaltyAttributes.get(contact.External_ID__c).attribute_code__c;
                    if (trac_ExternalCRM.BANNER_CODE_TO_MEMBERSHIP.containsKey(membershipCode) && contact.Membership_Status__c != trac_ExternalCRM.BANNER_CODE_TO_MEMBERSHIP.get(membershipCode)) {
                        contact.Membership_Status__c = trac_ExternalCRM.BANNER_CODE_TO_MEMBERSHIP.get(membershipCode);
                    }
                }
            }
        }
        return contact;
    }
}