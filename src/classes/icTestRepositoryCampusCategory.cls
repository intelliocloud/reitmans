@isTest
public with sharing class icTestRepositoryCampusCategory {

	static void initTest() {
		icTestCampusObjectData.createCampusDataSet();
	}

	static testMethod void test_getAllActiveCampusCategory() {
		initTest();

		icRepositoryCampusCategory.IClass repo = (icRepositoryCampusCategory.IClass) icObjectFactory.GetSingletonInstance('icRepositoryCampusCategory');
		List<Campus_Category__c> result =  repo.getAllActiveCampusCategory();
	}
}