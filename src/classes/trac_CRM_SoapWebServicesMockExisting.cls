public class trac_CRM_SoapWebServicesMockExisting implements WebServiceMock {
   public void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        trac_CRM_SoapWebServices.SaveResponse_element responseElement = new trac_CRM_SoapWebServices.SaveResponse_element();

        response.put('response_x', responseElement); 
   }
}