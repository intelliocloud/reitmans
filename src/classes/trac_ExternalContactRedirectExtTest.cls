/**
 *  @description Tests for the trac_CustomerExtension and CustomerRedirect page
 *  @author      Graham Barnard, Traction on Demand.
 *  @date        2015-12-10
 */
@isTest
private class trac_ExternalContactRedirectExtTest {
    /**
     *  @description Test Converting an External Object with no data
     *  @author    Graham Barnard, Traction on Demand.
     *  @date        2015-12-10
     */
    @isTest static void testExternalObjectWithNoData() {
        Ext_contact__x customer = trac_TestUtils.setupCustomer();

        Test.startTest();
        ApexPages.StandardController stdCommercial = new ApexPages.StandardController(customer);
        trac_ExternalContactRedirectExt controller = new trac_ExternalContactRedirectExt(stdCommercial);

        PageReference pageReference = new PageReference('/ExternalContactRedirect');
        Test.setCurrentPage(pageReference);

        PageReference nextPageReference = controller.convertExternalObject();

        Test.stopTest();

        System.assertNotEquals(null,nextPageReference);

        List<Account> accounts = [SELECT Id, Name FROM Account];
        System.assertEquals(1, accounts.size());

        Account account = accounts[0];
        System.assertEquals('test first name test last name', account.Name);

        List<Contact> contacts = [SELECT Id, FirstName, LastName, Email, Banner__c FROM Contact];
        System.assertEquals(1, contacts.size());

        Contact contact = contacts[0];
        System.assertEquals('test first name', contact.FirstName.toLowerCase());
        System.assertEquals('test last name', contact.LastName.toLowerCase());
        System.assertEquals('test@email.com', contact.Email.toLowerCase());
        System.assertEquals('Reitmans', contact.Banner__c);

        System.assertEquals('/' + contact.Id, nextPageReference.getUrl());
    }

    /**
     *  @description Test Converting an External Object matching contact with same banner
     *  @author    Graham Barnard, Traction on Demand.
     *  @date        2015-12-10
     */
    @isTest static void testConvertWithMatchingContact() {
        Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        contact.Banner__c = 'Reitmans';
        contact.FirstName = 'First';
        contact.LastName = 'Last';
        insert contact;

        Ext_contact__x customer = trac_TestUtils.setupCustomer();

        Test.startTest();
        ApexPages.StandardController stdCommercial = new ApexPages.StandardController(customer);
        trac_ExternalContactRedirectExt controller = new trac_ExternalContactRedirectExt(stdCommercial);

        PageReference pageReference = new PageReference('/ExternalContactRedirect');
        Test.setCurrentPage(pageReference);

        PageReference nextPageReference = controller.convertExternalObject();

        Test.stopTest();

        System.assertNotEquals(null,nextPageReference);

        List<Account> accounts = [SELECT Id, Name FROM Account];
        System.assertEquals(1, accounts.size());

        account = accounts[0];
        System.assertEquals('Test Account', account.Name);

        List<Contact> contacts = [SELECT Id, FirstName, LastName, Email, Banner__c FROM Contact];
        System.assertEquals(1, contacts.size());

        contact = contacts[0];
        System.assertEquals('test first name', contact.FirstName.toLowerCase());
        System.assertEquals('test last name', contact.LastName.toLowerCase());
        System.assertEquals('test@email.com', contact.Email.toLowerCase());
        System.assertEquals('Reitmans', contact.Banner__c);
    }

    /**
     *  @description Test Converting an External Object matching contact with different banner
     *  @author    Graham Barnard, Traction on Demand.
     *  @date        2015-12-10
     */
    @isTest static void testConvertWithMatchingContactForDifferentBanner() {
        Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        contact.Banner__c = 'FAKE';
        insert contact;

        Ext_contact__x customer = trac_TestUtils.setupCustomer();

        Test.startTest();
        ApexPages.StandardController stdCommercial = new ApexPages.StandardController(customer);
        trac_ExternalContactRedirectExt controller = new trac_ExternalContactRedirectExt(stdCommercial);

        PageReference pageReference = new PageReference('/ExternalContactRedirect');
        Test.setCurrentPage(pageReference);

        PageReference nextPageReference = controller.convertExternalObject();

        Test.stopTest();

        System.assertNotEquals(null,nextPageReference);

        List<Account> accounts = [SELECT Id, Name FROM Account];
        System.assertEquals(1, accounts.size());

        account = accounts[0];
        System.assertEquals('Test Account', account.Name);

        List<Contact> contacts = [SELECT Id, FirstName, LastName, Email, Banner__c FROM Contact];
        System.assertEquals(2, contacts.size());

        contacts = [SELECT Id, FirstName, LastName, Email, Banner__c FROM Contact WHERE Banner__c = 'Reitmans'];
        System.assertEquals(1, contacts.size());

        contact = contacts[0];
        System.assertEquals('test first name', contact.FirstName.toLowerCase());
        System.assertEquals('test last name', contact.LastName.toLowerCase());
        System.assertEquals('test@email.com', contact.Email.toLowerCase());
        System.assertEquals('Reitmans', contact.Banner__c);

        System.assertEquals('/' + contact.Id, nextPageReference.getUrl());
   }

    /**
     *  @description Test Converting an External Object matching contact with different banner
     *  @author    Graham Barnard, Traction on Demand.
     *  @date        2015-12-10
     */
    @isTest static void testConvertWithMatchingContactWithNoParams() {
        Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        contact.Banner__c = 'FAKE';
        insert contact;

        Ext_contact__x customer = trac_TestUtils.setupCustomer();

        Test.startTest();

        Ext_Contact__x externalContact = trac_TestUtils.setupCustomer();
        trac_ExternalContactModel.mockExternalContacts.add(externalContact);

        PageReference pageReference = new PageReference('ExternalContactRedirect');
        Test.setCurrentPage(pageReference);

        trac_ExternalContactRedirectExt controller = new trac_ExternalContactRedirectExt();
        PageReference nextPageReference = controller.convertExternalObject();

        Test.stopTest();

        System.assertEquals(null, nextPageReference);

        List<Account> accounts = [SELECT Id, Name FROM Account];
        System.assertEquals(1, accounts.size());

        account = accounts[0];
        System.assertEquals('Test Account', account.Name);

        List<Contact> contacts = [SELECT Id, FirstName, LastName, Email, Banner__c FROM Contact];
        System.assertEquals(1, contacts.size());

        contacts = [SELECT Id, FirstName, LastName, Email, Banner__c FROM Contact WHERE Banner__c = 'Reitmans'];
        System.assertEquals(0, contacts.size());

        System.assertEquals(null, nextPageReference);
   } 

    /**
     *  @description Test Converting an External Object matching contact with different banner
     *  @author    Graham Barnard, Traction on Demand.
     *  @date        2015-12-10
     */
    @isTest static void testConvertWithMatchingContactWithParams() {
        Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        contact.Banner__c = 'FAKE';
        insert contact;

        Ext_contact__x customer = trac_TestUtils.setupCustomer();

        Test.startTest();

        Ext_Contact__x externalContact = trac_TestUtils.setupCustomer();
        trac_ExternalContactModel.mockExternalContacts.add(externalContact);

        PageReference pageReference = new PageReference('ExternalContactRedirect');
        pageReference.getParameters().put('customer_no', String.valueOf(customer.customer_no__c));
        pageReference.getParameters().put('brand_prefix', customer.brand_prefix__c);
        Test.setCurrentPage(pageReference);

        trac_ExternalContactRedirectExt controller = new trac_ExternalContactRedirectExt();
        PageReference nextPageReference = controller.convertExternalObject();

        Test.stopTest();

        System.assertNotEquals(null,nextPageReference);

        List<Account> accounts = [SELECT Id, Name FROM Account];
        System.assertEquals(1, accounts.size());

        account = accounts[0];
        System.assertEquals('Test Account', account.Name);

        List<Contact> contacts = [SELECT Id, FirstName, LastName, Email, Banner__c FROM Contact];
        System.assertEquals(2, contacts.size());

        contacts = [SELECT Id, FirstName, LastName, Email, Banner__c FROM Contact WHERE Banner__c = 'Reitmans'];
        System.assertEquals(1, contacts.size());

        contact = contacts[0];
        System.assertEquals('test first name', contact.FirstName.toLowerCase());
        System.assertEquals('test last name', contact.LastName.toLowerCase());
        System.assertEquals('test@email.com', contact.Email.toLowerCase());
        System.assertEquals('Reitmans', contact.Banner__c);

        System.assertNotEquals(null, nextPageReference);
        System.assertEquals('/' + contact.Id, nextPageReference.getUrl());
   }   
}