/**
 *  @description All SOQL calls for Account
 *  @author 	 Graham Barnard, Traction on Demand.
 *  @date        2016-01-14
 */
public with sharing class trac_AccountModel {
	private static Id accountCustomerServiceRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Service RCL').getRecordTypeId();

	/**
	 *  @description Return a single account by id
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-01-04
	 */
	public static Account findById(Id accountId) {
		List<Account> accounts = [
            SELECT Id, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, Name, Email_Address__c
            FROM Account
            WHERE Id =: accountId AND RecordTypeId =: accountCustomerServiceRecordTypeId
        ];

		return (accounts.size() != 0) ? accounts[0] : null;
	}

	/**
	 *  @description Return account list by email address
	 *  @author 	 Tanminder Rai, Traction on Demand.
	 *  @date        2016-01-04
	 */
	public static List<Account> findByEmailAddresses(Set<String> emailAddresses) {
		List<Account> accounts = [
            SELECT Id, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, Name, Email_Address__c
            FROM Account
            WHERE Email_Address__c IN :emailAddresses AND RecordTypeId =: accountCustomerServiceRecordTypeId
        ];

		return accounts;
	}

	/**
	  *  @description Return account list by email address
	  *  @author    Tanminder Rai, Traction on Demand.
	  *  @date        2016-01-04
	  */
	public static List<Account> findByEmailAddresses(List<String> emailAddresses) {
	    List<Account> accounts = [
	            SELECT Id, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, Name, Email_Address__c
	            FROM Account
	            WHERE Email_Address__c IN :emailAddresses AND RecordTypeId =: accountCustomerServiceRecordTypeId
	        ];

	    return accounts;
	}
}