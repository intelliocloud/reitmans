/**
 *  @description Controller for Urgent Message Lightning component
 *  @author 	 Jeremy Horan, Traction on Demand.
 *  @date        2016-08-04
 */
public with sharing class trac_UrgentMessageCtlr {

	private static Map<String, Profile_Banner_Community_Settings__c> profileBannerCommunitySettings;

	public trac_UrgentMessageCtlr() {

	}

	/**
	 *  @description Gets a list of Urgent Messages based on User Profile
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-08-04
	 */
	@AuraEnabled
	public static List<Urgent_Message__c> getUrgentMessages() {
		profileBannerCommunitySettings = Profile_Banner_Community_Settings__c.getall();
		Map<String, Profile_Banner_Community_Settings__c> profileBannerCommunitySettingsByProfile = new Map<String, Profile_Banner_Community_Settings__c>();
		for (Profile_Banner_Community_Settings__c profileBannerCommunitySetting : profileBannerCommunitySettings.values()) {
			profileBannerCommunitySettingsByProfile.put(profileBannerCommunitySetting.Profile__c, profileBannerCommunitySetting);
		}

		List<Urgent_Message__c> urgentMessages = new List<Urgent_Message__c>();

	    Profile currentUserProfile = trac_ProfileSelector.findById(UserInfo.getProfileId());
	    if (currentUserProfile != null && profileBannerCommunitySettingsByProfile.containsKey(currentUserProfile.Name)) {
	    	String banner = profileBannerCommunitySettingsByProfile.get(currentUserProfile.Name).Banner__c;
	        urgentMessages = trac_UrgentMessageSelector.findAllActiveByBanner(banner, Date.today());
	    }

	    return urgentMessages;
	}

	/**
	 *  @description Gets an Urgent Message with given Id
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-08-04
	 */
	@AuraEnabled
	public static Urgent_Message__c getUrgentMessage(Id urgentMessageId) {
	    return trac_UrgentMessageSelector.getUrgentMessageById(urgentMessageId);
	}
}