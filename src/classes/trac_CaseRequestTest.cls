/**
 *  @description Test for trac_CaseRequest
 *  @author 	 Marco Martel, Traction on Demand.
 *  @date        2016-02-03
 */

@isTest
private class trac_CaseRequestTest {
	
	public static final String IN_PROGRESS = 'In Progress';
	public static final String CLOSED = 'Completed';
	public static final String OPEN = 'Open';
	public static final String ASSIGNED = 'Assigned';

	@isTest static void trac_CaseRequestTest() {

		Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        contact.Banner__c = 'REIT';
        contact.FirstName = 'First';
        contact.LastName = 'Last';
        insert contact;
		
		Case case1 = trac_TestUtils.setupCase(contact.Id);
		insert case1;
		
		List<Case> caseTest = ([
			SELECT Id, Status
			FROM Case
			WHERE Id = :case1.Id
			]);

		System.assert(caseTest.size()==1);
		
		List<Request__c> reqsToInsert = new List<Request__c>();

		Request__c request1 = new Request__c();
		request1.Case__c = case1.Id;
		request1.Request_Status__c = IN_PROGRESS;
		request1.Request_Type__c = 'Insurance';
		request1.Request_Subject__c = 'this is a test';
		reqsToInsert.add(request1);

		Request__c request2 = new Request__c();
		request2.Case__c = case1.Id;
		request2.Request_Status__c = IN_PROGRESS;
		request2.Request_Type__c = 'Insurance';
		request2.Request_Subject__c = 'this is a test';
		reqsToInsert.add(request2);

		insert reqsToInsert;

		caseTest = ([
			SELECT Id, Status
			FROM Case
			WHERE Id = :case1.Id
			]);

		System.assert(caseTest.size()==1);
		System.assert(caseTest[0].Status.contains(IN_PROGRESS));

		for(Request__c requests:reqsToInsert){
			requests.Request_Status__c = CLOSED;
		}

		update reqsToInsert;

		caseTest = ([
			SELECT Id, Status
			FROM Case
			WHERE Id = :case1.Id
			]);

		System.assert(caseTest.size()==1);
		System.assert(caseTest[0].Status.contains(ASSIGNED));
	}
	
	@isTest static void trac_CaseRequestTestBulk(){

		Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        contact.Banner__c = 'REIT';
        contact.FirstName = 'First';
        contact.LastName = 'Last';
        insert contact;

        List<Case> listOfCases = new List<Case>();
        for(integer i=0; i<10; i++){
        	Case case1 = trac_TestUtils.setupCase(contact.Id);
			listOfCases.add(case1);
        }
        insert listOfCases;

        List<Case> caseTest = ([
			SELECT Id, Status
			FROM Case
			WHERE Id IN :listOfCases
			]);

        List<Request__c> reqsToInsert = new List<Request__c>();

		for(Case cases:caseTest){
			Request__c request1 = new Request__c();
			request1.Case__c = cases.Id;
			request1.Request_Status__c = IN_PROGRESS;
			request1.Request_Type__c = 'Insurance';
			request1.Request_Subject__c = 'this is a test';
			reqsToInsert.add(request1);

			Request__c request2 = new Request__c();
			request2.Case__c = cases.Id;
			request2.Request_Status__c = IN_PROGRESS;
			request2.Request_Type__c = 'Insurance';
			request2.Request_Subject__c = 'this is a test';
			reqsToInsert.add(request2);
		}
		
		insert reqsToInsert;

		caseTest = ([
			SELECT Id, Status
			FROM Case
			WHERE Id IN :listOfCases
			]);

		Boolean isCorrect = true;
		isCorrect = true;
        for(Case cases:caseTest){
        	if(!cases.Status.contains(IN_PROGRESS)){
        		isCorrect = false;
        	}
        }
        System.assert(isCorrect);

        for(Request__c requests:reqsToInsert){
        	requests.Request_Status__c = CLOSED;
        }

        update reqsToInsert;

		caseTest = ([
			SELECT Id, Status
			FROM Case
			WHERE Id = :listOfCases
			]);

		isCorrect = true;
        for(Case cases:caseTest){
        	if(!cases.Status.contains(ASSIGNED)){
        		isCorrect = false;
        	}
        }
        System.assert(isCorrect);
	}

	@isTest static void trac_CaseRequestTestReOpen(){

		Account account = trac_TestUtils.setupAccount();
        insert account;

        Contact contact = trac_TestUtils.setupContact(account.Id);
        contact.Banner__c = 'REIT';
        contact.FirstName = 'First';
        contact.LastName = 'Last';
        insert contact;
		
		Case case1 = trac_TestUtils.setupCase(contact.Id);
		insert case1;
		
		Request__c request1 = new Request__c();
		request1.Case__c = case1.Id;
		request1.Request_Status__c = OPEN;
		request1.Request_Type__c = 'Insurance';
		request1.Request_Subject__c = 'this is a test';

		insert request1;

		List<Case> caseTest = ([
			SELECT Id, Status
			FROM Case
			WHERE Id = :case1.Id
			]);

		request1.Request_Status__c = IN_PROGRESS;

		update request1;

		caseTest = ([
			SELECT Id, Status
			FROM Case
			WHERE Id = :case1.Id
			]);

		System.assert(caseTest[0].Status.contains(IN_PROGRESS));

		request1.Request_Status__c = CLOSED;

		update request1;

		caseTest = ([
			SELECT Id, Status
			FROM Case
			WHERE Id = :case1.Id
			]);

		System.assert(caseTest[0].Status.contains(ASSIGNED));

		request1.Request_Status__c = IN_PROGRESS;

		update request1;

		caseTest = ([
			SELECT Id, Status
			FROM Case
			WHERE Id = :case1.Id
			]);

		System.assert(caseTest[0].Status.contains(IN_PROGRESS));
	}
}