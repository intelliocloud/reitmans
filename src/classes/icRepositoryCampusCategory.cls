public class icRepositoryCampusCategory implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Campus_Category__c> getAllActiveCampusCategory();
	}

	public class Impl implements IClass {
		
		public List<Campus_Category__c> getAllActiveCampusCategory() {
			return [SELECT	Id
							,Name
							,Label_EN__c
							,Label_FR__c
							,Parent_Category__c
					FROM	Campus_Category__c 
					WHERE	Active__c = TRUE
					ORDER BY Sort_Order__c];
		}
	}
}