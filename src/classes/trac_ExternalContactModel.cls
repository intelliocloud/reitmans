/**
 *  @description Query calls for external contact object
 *  @author 	 Graham Barnard, Traction on Demand.
 *  @date        2015-12-29
 */
public with sharing class trac_ExternalContactModel {
	private static final String EMAIL_AT_SIGN = '@';
	private static final String EMAIL_PERIOD = '.';
	private static final String NUMBER_SIGN = '*';
	private static final String REGEX_NON_DIGITS = '[^0-9]';

	private static final Integer MINIMUM_PHONE_LENGTH = 7;

	@TestVisible private static List<Ext_Contact__x> mockExternalContacts = new List<Ext_Contact__x>();

	/**
	*  @description Get an External Contact By Id
	*  @author      Graham Barnard, Traction on Demand.
	*  @date        2015-12-17
	*/
	public static Ext_Contact__x findById(Id externalContactId) {
		List<Ext_Contact__x> externalContacts = new List<Ext_Contact__x>();

		if(Test.isRunningTest()) {
			externalContacts = mockExternalContacts;
		}
		else {
			externalContacts = [
			    SELECT  Id, apartment__c, birth_date__c, city__c, customer_id__c, customer_no__c, due_date__c, email_address__c,
			            email_indicator__c, email_opt_in__c, email_opt_in_date__c, first_name__c, gender__c, last_name__c,
			            last_updated__c, post_code__c, province__c, street_no__c, telephone_no__c, brand_prefix__c
			    FROM Ext_Contact__x
			    WHERE Id =: externalContactId
			];
		}

		return (externalContacts.size() > 0) ? externalContacts[0] : null;
	}

	/**
	*  @description Get an External Contact By Customer Number And Brand
	*  @author      Graham Barnard, Traction on Demand.
	*  @date        2015-12-17
	*/
	public static Ext_Contact__x findByCustomerNoAndBrand(Long customerNo, String brandPrefix) {
		List<Ext_Contact__x> externalContacts = new List<Ext_Contact__x>();

		if(Test.isRunningTest()) {
			externalContacts = mockExternalContacts;
		}
		else {
			externalContacts = [
			    SELECT  Id, address_id__c, address_modified__c, address_type_code__c, apartment__c, birth_date__c,
				    brand_prefix__c, city__c, country_code__c, create_date__c, create_source__c, customer_id__c,
				    customer_no__c, due_date__c, email_address__c, email_indicator__c, email_opt_in__c, email_opt_in_date__c,
				    first_name__c, gender__c, language_code__c, last_name__c, last_updated__c, mail_opt_in_flag__c,
				    membership_date__c, membership_type_code__c, mobile_phone_no__c, phone_opt_in_flag__c, post_code__c,
				    province__c, segmentation_flag_a__c, segmentation_flag_b__c, segmentation_flag_c__c, segmentation_flag_d__c,
				    segmentation_flag_e__c, segmentation_flag_f__c, store_no__c, street_no__c, telephone_no__c
			    FROM Ext_Contact__x
			    WHERE customer_no__c =: customerNo AND brand_prefix__c =: brandPrefix
			];
		}

		return (externalContacts.size() > 0) ? externalContacts[0] : null;
	}

	/**
	*  @description Get a list of External Contacts By Email
	*  @author      Graham Barnard, Traction on Demand.
	*  @date        2015-12-17
	*/
	public static List<Ext_Contact__x> findAllByEmail(String emailAddress) {
		List<Ext_Contact__x> externalContacts = new List<Ext_Contact__x>();
		
		if(Test.isRunningTest()) {
			externalContacts = mockExternalContacts;
		}
		else {
			externalContacts = [
				SELECT  Id, address_id__c, address_modified__c, address_type_code__c, apartment__c, birth_date__c,
				    brand_prefix__c, city__c, country_code__c, create_date__c, create_source__c, customer_id__c,
				    customer_no__c, due_date__c, email_address__c, email_indicator__c, email_opt_in__c, email_opt_in_date__c,
				    first_name__c, gender__c, language_code__c, last_name__c, last_updated__c, mail_opt_in_flag__c,
				    membership_date__c, membership_type_code__c, mobile_phone_no__c, phone_opt_in_flag__c, post_code__c,
				    province__c, segmentation_flag_a__c, segmentation_flag_b__c, segmentation_flag_c__c, segmentation_flag_d__c,
				    segmentation_flag_e__c, segmentation_flag_f__c, store_no__c, street_no__c, telephone_no__c
			    FROM Ext_Contact__x
			    WHERE email_address__c =: emailAddress
			];
		}
		return externalContacts;
	}

	/**
	*  @description Get a list of External Contacts By Phone
	*  @author      Graham Barnard, Traction on Demand.
	*  @date        2015-12-17
	*/
	public static List<Ext_Contact__x> findAllByPhone(String phoneNumber) {
		List<Ext_Contact__x> externalContacts = new List<Ext_Contact__x>();
		
		if(Test.isRunningTest()) {
			externalContacts = mockExternalContacts;
		}
		else {
			externalContacts = [
			    SELECT  Id, address_id__c, address_modified__c, address_type_code__c, apartment__c, birth_date__c,
				    brand_prefix__c, city__c, country_code__c, create_date__c, create_source__c, customer_id__c,
				    customer_no__c, due_date__c, email_address__c, email_indicator__c, email_opt_in__c, email_opt_in_date__c,
				    first_name__c, gender__c, language_code__c, last_name__c, last_updated__c, mail_opt_in_flag__c,
				    membership_date__c, membership_type_code__c, mobile_phone_no__c, phone_opt_in_flag__c, post_code__c,
				    province__c, segmentation_flag_a__c, segmentation_flag_b__c, segmentation_flag_c__c, segmentation_flag_d__c,
				    segmentation_flag_e__c, segmentation_flag_f__c, store_no__c, street_no__c, telephone_no__c
			    FROM Ext_Contact__x
			    WHERE telephone_no__c =: phoneNumber
			];
		}

		return externalContacts;
	}

	/**
	*  @description Get a list of External Contacts By Email and Banner
	*  @author      Tanminder Rai, Traction on Demand.
	*  @date        2016-04-02
	*/
	public static List<Ext_Contact__x> findAllByEmailAndBanner(String emailAddress, String brandPrefix) {
		List<Ext_Contact__x> externalContacts = new List<Ext_Contact__x>();
		
		if(Test.isRunningTest()) {
			externalContacts = mockExternalContacts;
		}
		else {
			externalContacts = [
			    SELECT  Id, address_id__c, address_modified__c, address_type_code__c, apartment__c, birth_date__c,
				    brand_prefix__c, city__c, country_code__c, create_date__c, create_source__c, customer_id__c,
				    customer_no__c, due_date__c, email_address__c, email_indicator__c, email_opt_in__c, email_opt_in_date__c,
				    first_name__c, gender__c, language_code__c, last_name__c, last_updated__c, mail_opt_in_flag__c,
				    membership_date__c, membership_type_code__c, mobile_phone_no__c, phone_opt_in_flag__c, post_code__c,
				    province__c, segmentation_flag_a__c, segmentation_flag_b__c, segmentation_flag_c__c, segmentation_flag_d__c,
				    segmentation_flag_e__c, segmentation_flag_f__c, store_no__c, street_no__c, telephone_no__c
			    FROM Ext_Contact__x
			    WHERE email_address__c =: emailAddress AND brand_prefix__c =: brandPrefix
			];
		}

		return externalContacts;
	}


	/**
	*  @description Get a list of External Contacts By Email
	*  @author      Graham Barnard, Traction on Demand.
	*  @date        2015-12-17
	*/
	public static List<Ext_Contact__x> findAllByPhoneOrEmail(String searchString) {
		List<Ext_Contact__x> externalContacts = new List<Ext_Contact__x>();

		Boolean isEmailAddress = searchString.contains(EMAIL_AT_SIGN) && searchString.contains(EMAIL_PERIOD);
		if(isEmailAddress) {
			externalContacts.addAll(findAllByEmail(searchString));
		}

		Boolean isCustomerNumber = searchString.startsWith(NUMBER_SIGN);
		if (isCustomerNumber) {
			String customerNumber = searchString.removeStart(NUMBER_SIGN);
			if (customerNumber.isNumeric()) {
				externalContacts.addAll(findAllByCustomerNumber(Long.valueOf(customerNumber)));
			}
		}

		Boolean isPhoneNumber = (searchString.replaceAll(REGEX_NON_DIGITS, '').length() >= MINIMUM_PHONE_LENGTH && !isCustomerNumber);
		if(isPhoneNumber) {
			externalContacts.addAll(findAllByPhone(searchString));
		}

		Boolean isOtherSearch = (!isEmailAddress && !isCustomerNumber && !isPhoneNumber);
		if (isOtherSearch) {
			externalContacts.addAll(findAllByLastName(searchString));
		}

		return externalContacts;
	}

	/**
	 *  @description Get a Map of External Contacts by Customer Number
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-09-25
	 */
	public static List<Ext_Contact__x> findAllByLastName(String customerNumber) {
		List<Ext_Contact__x> externalContacts = new List<Ext_Contact__x>();
		customerNumber = customerNumber.toUpperCase();
		if(Test.isRunningTest()) {
			externalContacts = mockExternalContacts;
		} else {
			externalContacts = [
				SELECT  Id, address_id__c, address_modified__c, address_type_code__c, apartment__c, birth_date__c,
				    brand_prefix__c, city__c, country_code__c, create_date__c, create_source__c, customer_id__c,
				    customer_no__c, due_date__c, email_address__c, email_indicator__c, email_opt_in__c, email_opt_in_date__c,
				    first_name__c, gender__c, language_code__c, last_name__c, last_updated__c, mail_opt_in_flag__c,
				    membership_date__c, membership_type_code__c, mobile_phone_no__c, phone_opt_in_flag__c, post_code__c,
				    province__c, segmentation_flag_a__c, segmentation_flag_b__c, segmentation_flag_c__c, segmentation_flag_d__c,
				    segmentation_flag_e__c, segmentation_flag_f__c, store_no__c, street_no__c, telephone_no__c
			    FROM Ext_Contact__x
			    WHERE last_name__c =: customerNumber
			    LIMIT 10000
			];
		}
		
		return externalContacts;
	}

	/**
	 *  @description Get a Map of External Contacts by Customer Number
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-09-25
	 */
	public static List<Ext_Contact__x> findAllByCustomerNumber(Long customerNumber) {
		List<Ext_Contact__x> externalContacts = new List<Ext_Contact__x>();
		
		if(Test.isRunningTest()) {
			externalContacts = mockExternalContacts;
		} else {
			externalContacts = [
				SELECT  Id, address_id__c, address_modified__c, address_type_code__c, apartment__c, birth_date__c,
				    brand_prefix__c, city__c, country_code__c, create_date__c, create_source__c, customer_id__c,
				    customer_no__c, due_date__c, email_address__c, email_indicator__c, email_opt_in__c, email_opt_in_date__c,
				    first_name__c, gender__c, language_code__c, last_name__c, last_updated__c, mail_opt_in_flag__c,
				    membership_date__c, membership_type_code__c, mobile_phone_no__c, phone_opt_in_flag__c, post_code__c,
				    province__c, segmentation_flag_a__c, segmentation_flag_b__c, segmentation_flag_c__c, segmentation_flag_d__c,
				    segmentation_flag_e__c, segmentation_flag_f__c, store_no__c, street_no__c, telephone_no__c
			    FROM Ext_Contact__x
			    WHERE customer_no__c =: customerNumber
			];
		}
		
		return externalContacts;
	}

	/**
	 *  @description Get a Map of External Contacts by Customer Number
	 *  @author 	 Jeremy Horan, Traction on Demand.
	 *  @date        2016-03-07
	 */
	public static List<Ext_Contact__x> findAllByCustomerNumbers(List<Long> newCustomerNumbers) {
		List<Ext_Contact__x> externalContacts = new List<Ext_Contact__x>();
		
		if(Test.isRunningTest()) {
			externalContacts = mockExternalContacts;
		} else {
			externalContacts = [
				SELECT  Id, address_id__c, address_modified__c, address_type_code__c, apartment__c, birth_date__c,
				    brand_prefix__c, city__c, country_code__c, create_date__c, create_source__c, customer_id__c,
				    customer_no__c, due_date__c, email_address__c, email_indicator__c, email_opt_in__c, email_opt_in_date__c,
				    first_name__c, gender__c, language_code__c, last_name__c, last_updated__c, mail_opt_in_flag__c,
				    membership_date__c, membership_type_code__c, mobile_phone_no__c, phone_opt_in_flag__c, post_code__c,
				    province__c, segmentation_flag_a__c, segmentation_flag_b__c, segmentation_flag_c__c, segmentation_flag_d__c,
				    segmentation_flag_e__c, segmentation_flag_f__c, store_no__c, street_no__c, telephone_no__c
			    FROM Ext_Contact__x
			    WHERE customer_no__c IN :newCustomerNumbers
			];
		}
		
		return externalContacts;
	}
}