/**
 *  @description  Wsdl2Apex Mock class from https://webservice.s6.exacttarget.com/etframework.wsdl
 *  @author      Zi Zhang, Traction on Demand.
 *  @date        2016-02-24
 */
@isTest
private class trac_EmailSendHistoryCtlrTest {
	private static Contact testContact1, testContact2, testContact3;
	private static Case testCase1, testCase2;
	private static Email_Send_Subscriber_Prefix__c setting;
	private static ET_SOAP_API_Login__c loginSetting;

	// Test Data Creations
	static {
		// Test Contacts (Banner__c and Email fields needed for Email Send)
		List<Contact> contacts = new List<Contact>();
		testContact1 = new Contact(LastName='Test Contact 1', FirstName='Test Contact 1', Banner__c='Reitmans', Customer_Number__c='123456', Email='test123@traction.com');
		testContact2 = new Contact(LastName='Test Contact 2', FirstName='Test Contact 1', Email='test123@traction.com');
		testContact3 = new Contact(LastName='Test Contact 3', FirstName='Test Contact 1', Banner__c='Test');
		contacts.add(testContact1);
		contacts.add(testContact2);
		contacts.add(testContact3);

		if(contacts.size() > 0) {
			insert contacts;
		}

		// Test Cases
		List<Case> cases = new List<Case>();
		testCase1 = trac_TestUtils.setupCase(contacts[0].Id);
		testCase2 = trac_TestUtils.setupCase(contacts[1].Id);
		cases.add(testCase1);
		cases.add(testCase2);

		if(cases.size() > 0) {
			insert cases;
		}

		// Test data for custom setting
		setting = new Email_Send_Subscriber_Prefix__c(Name='Reitmans', Prefix__c='RE_');
		insert setting;

		loginSetting = new ET_SOAP_API_Login__c(Name='Reitmans', Username__c='test', Password__c='testtest');
		insert loginSetting;
	}

    /**
	 * Successful response from webservice
	 * @author Zi Zhang, Traction on Demand
	 * @date   2016-02-24
	 */
    @isTest 
    private static void testControllerForContactSuccess() {
        ApexPages.currentPage().getParameters().put('id', testContact1.Id);

        Test.startTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(testContact1);
        trac_EmailSendHistoryCtlr controller = new trac_EmailSendHistoryCtlr(stdController);

        exacttargetComWsdlPartnerapiMockImpl mock = new exacttargetComWsdlPartnerapiMockImpl();
        exacttargetComWsdlPartnerapiMockImpl.isSuccess = true;

        Test.setMock(WebServiceMock.class, mock);

        controller.getEmailSendLogs();
        Test.stopTest();
    }

    /**
	 * Webservice returns error
	 * @author Zi Zhang, Traction on Demand
	 * @date   2016-02-24
	 */
    @isTest 
    private static void testControllerForContactErrorResponse() {
        ApexPages.currentPage().getParameters().put('id', testContact1.Id);

        Test.startTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(testContact1);
        trac_EmailSendHistoryCtlr controller = new trac_EmailSendHistoryCtlr(stdController);

        exacttargetComWsdlPartnerapiMockImpl mock = new exacttargetComWsdlPartnerapiMockImpl();
        exacttargetComWsdlPartnerapiMockImpl.isSuccess = false;

        Test.setMock(WebServiceMock.class, mock);

        controller.getEmailSendLogs();
        Test.stopTest();
    }

    /**
	 * Successful webservice call, no results returned
	 * @author Zi Zhang, Traction on Demand
	 * @date   2016-02-24
	 */
    @isTest 
    private static void testControllerForContactNoResults() {
        ApexPages.currentPage().getParameters().put('id', testContact1.Id);

        Test.startTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(testContact1);
        trac_EmailSendHistoryCtlr controller = new trac_EmailSendHistoryCtlr(stdController);

        exacttargetComWsdlPartnerapiMockImpl mock = new exacttargetComWsdlPartnerapiMockImpl();
        exacttargetComWsdlPartnerapiMockImpl.isSuccess = true;
        exacttargetComWsdlPartnerapiMockImpl.noResults = true;

        Test.setMock(WebServiceMock.class, mock);

        controller.getEmailSendLogs();
        Test.stopTest();
    }

    /**
	 * No banner, aka business unit specified on contact record
	 * @author Zi Zhang, Traction on Demand
	 * @date   2016-02-24
	 */
    @isTest 
    private static void testControllerForContactNoBanner() {
        ApexPages.currentPage().getParameters().put('id', testContact2.Id);

        Test.startTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(testContact2);
        trac_EmailSendHistoryCtlr controller = new trac_EmailSendHistoryCtlr(stdController);

        exacttargetComWsdlPartnerapiMockImpl mock = new exacttargetComWsdlPartnerapiMockImpl();
        exacttargetComWsdlPartnerapiMockImpl.isSuccess = true;

        Test.setMock(WebServiceMock.class, mock);

        controller.getEmailSendLogs();
        Test.stopTest();
    }

    /**
	 * Unit test controller constructor Case record
	 * @author Zi Zhang, Traction on Demand
	 * @date   2016-02-24
	 */
    @isTest 
    private static void testControllerForCase() {
        ApexPages.currentPage().getParameters().put('id', testCase1.Id);

        Test.startTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(testCase1);
        trac_EmailSendHistoryCtlr controller = new trac_EmailSendHistoryCtlr(stdController);

        exacttargetComWsdlPartnerapiMockImpl mock = new exacttargetComWsdlPartnerapiMockImpl();
        exacttargetComWsdlPartnerapiMockImpl.isSuccess = true;

        Test.setMock(WebServiceMock.class, mock);

        controller.getEmailSendLogs();
        Test.stopTest();
    }
}