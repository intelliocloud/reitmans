@isTest
public with sharing class icTestCTRLCampus {

	static void initTest() {
		icTestCampusObjectData.createCampusDataSet();
	}

	static testMethod void test_getNavigationCategories() {
		initTest();
		Map<String, List<icDTOCategory>> result =  icCTRLCampus.getNavigationCategories();
	}

	static testMethod void test_getCategoryContent() {
		initTest();
		Map<String, List<icDTOContent>> result =  icCTRLCampus.getCategoryContent(icTestCampusObjectData.contentCategory);
	}

	static testMethod void test_getWelcomeContent() {
		initTest();
		Map<String, List<icDTOContent>> result =  icCTRLCampus.getWelcomeContent(icTestCampusObjectData.contentCategory);
	}
}