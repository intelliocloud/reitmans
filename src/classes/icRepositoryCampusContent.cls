public class icRepositoryCampusContent implements icIClass {

	public Object GetInstance() {
		return new Impl();
	}

	public Interface IClass {
		List<Junction_Content_Category__c> getCampusContentJunctionByCategoryAndLanguage(String categoryId, String language);
		List<Junction_Content_Category__c> getCampusWelcomeContentJunctionByCategory(String categoryId);
	}

	public class Impl implements IClass {
		
		public List<Junction_Content_Category__c> getCampusContentJunctionByCategoryAndLanguage(String categoryId, String language) {
			return [SELECT	Id
							,Name
							,Campus_Content__c
							,Campus_Content__r.RecordTypeId
							,Campus_Content__r.RecordType.Name
							,Campus_Content__r.Name
							,Campus_Content__r.Image_Location__c
							,Campus_Content__r.Title__c
							,toLabel(Campus_Content__r.Type__c)
							,Campus_Content__r.Short_Description__c
							,Campus_Content__r.Description__c
							,Campus_Content__r.Cost__c
							,Campus_Content__r.Eligibility__c
							,Campus_Content__r.How_to_Register__c
							,Campus_Content__r.Location__c
							,Campus_Content__r.Partners__c
							,Campus_Content__r.Syllabus__c
							,Campus_Content__r.Target_Audience__c
							,Campus_Content__r.Time_Commitment__c
							,Campus_Content__r.Video__c
							,Campus_Content__r.Article__c
							,Campus_Content__r.Content_Details_EN__c
							,Campus_Content__r.Content_Details_FR__c
							,Campus_Content__r.Content_Heading_EN__c
							,Campus_Content__r.Content_Heading_FR__c
							,Campus_Content__r.Content_Sub_Heading_EN__c
							,Campus_Content__r.Content_Sub_Heading_FR__c
							,Campus_Content__r.Faculty_Name__c
							,Campus_Content__r.Faculty_Title_EN__c
							,Campus_Content__r.Faculty_Title_FR__c
							,Campus_Content__r.Faculty_Detail_EN__c
							,Campus_Content__r.Faculty_Detail_FR__c
							,Campus_Content__r.Sort_Order__c
					FROM 	Junction_Content_Category__c
					WHERE	Campus_Category__c = :categoryId
					AND		Campus_Content__r.Language__c = :language
					ORDER BY Campus_Content__r.Sort_Order__c];
		}

		public List<Junction_Content_Category__c> getCampusWelcomeContentJunctionByCategory(String categoryId) {
			return [SELECT	Id
							,Name
							,Campus_Content__c
							,Campus_Content__r.RecordTypeId
							,Campus_Content__r.RecordType.Name
							,Campus_Content__r.Name
							,Campus_Content__r.Image_Location__c
							,Campus_Content__r.Title__c
							,Campus_Content__r.Type__c
							,Campus_Content__r.Short_Description__c
							,Campus_Content__r.Description__c
							,Campus_Content__r.Cost__c
							,Campus_Content__r.Eligibility__c
							,Campus_Content__r.How_to_Register__c
							,Campus_Content__r.Location__c
							,Campus_Content__r.Partners__c
							,Campus_Content__r.Syllabus__c
							,Campus_Content__r.Target_Audience__c
							,Campus_Content__r.Time_Commitment__c
							,Campus_Content__r.Video__c
							,Campus_Content__r.Article__c
							,Campus_Content__r.Content_Details_EN__c
							,Campus_Content__r.Content_Details_FR__c
							,Campus_Content__r.Content_Heading_EN__c
							,Campus_Content__r.Content_Heading_FR__c
							,Campus_Content__r.Content_Sub_Heading_EN__c
							,Campus_Content__r.Content_Sub_Heading_FR__c
							,Campus_Content__r.Faculty_Name__c
							,Campus_Content__r.Faculty_Title_EN__c
							,Campus_Content__r.Faculty_Title_FR__c
							,Campus_Content__r.Faculty_Detail_EN__c
							,Campus_Content__r.Faculty_Detail_FR__c
							,Campus_Content__r.Sort_Order__c
					FROM 	Junction_Content_Category__c
					WHERE	Campus_Category__c = :categoryId
					ORDER BY Campus_Content__r.Sort_Order__c];
		}
	}
}