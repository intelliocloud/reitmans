/**
 *  @description This class will automatically assign an Entitlement to a Case based on criteria.
 *  @author 	 Graham Barnard, Traction on Demand.
 *  @date        2015-10-08
 */
public without sharing class trac_Entitlement {

	@TestVisible private static String ENTITLEMENT_NAME = 'Default';

	private List<Account> accounts;
	private List<Case> cases;

	private static Id customerServiceRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Service RCL').getRecordTypeId();

	/**
	 *  @description Constructor for accounts 
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-10
	 */
	public trac_Entitlement(List<Account> unfilteredAccounts) {
		accounts = filterEntitlementAccounts(unfilteredAccounts);
		cases = new List<Case>();
	}

	/**
	 *  @description Constructor for cases
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2016-02-10
	 */
	public trac_Entitlement(List<Case> unfilteredCases) {
		cases = filterEntitlementCases(unfilteredCases);
		accounts = new List<Account>();
	}

	/**
	 *  @description Associate the proper entitlement to the account
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2015-10-08
	 */
    public void associateEntitlementToAccount() {
    	if(accounts.size() == 0) return;

    	List<Entitlement> entitlementsToInsert = new List<Entitlement>();

    	Set<Id> accountsIds = trac_SObjectModel.getIds(accounts);
		SlaProcess slaProcess = findDefaultSlaProcess();

        if(slaProcess == null) return;

	  	for(Account account : accounts) {
	        Entitlement entitlement = buildEntitlement(account, slaProcess);
	        entitlementsToInsert.add(entitlement);
		}

	  	if(entitlementsToInsert.size() > 0) {
	  		insert entitlementsToInsert;
	  	}
    }

	/**
	 *  @description Associate the proper entitlement to the case
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2015-10-08
	 */
    public void associateEntitlementToCase() {
    	if(cases.size() == 0) return;

    	Map<Id, Entitlement> mappedAccountToEntitlement = new Map<Id, Entitlement>();
    	List<Case> casesToUpsert = new List<Case>();

    	Set<Id> accountsIds = new Set<Id>();
    	for(Case c : cases) {
    		accountsIds.add(c.AccountId);
    	}

    	List<Entitlement> entitlements = findAllEntitlementsByAccountIds(accountsIds);

		for(Entitlement entitlement : entitlements) {
    		if(!mappedAccountToEntitlement.containsKey(entitlement.AccountId)) {
    			mappedAccountToEntitlement.put(entitlement.AccountId, entitlement);
    		}
    	}

    	for(Case c : cases) {
    		Entitlement entitlement = mappedAccountToEntitlement.get(c.AccountId);
    		if(entitlement != null) {
    			c.EntitlementId = entitlement.Id;
    			casesToUpsert.add(c);
			}
    	}
    }

	/**
	 *  @description Create new Entitlement associated to the account
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2015-10-13
	 */
	private Entitlement buildEntitlement(Account account, SlaProcess slaProcess) {
        Entitlement entitlement = new Entitlement();
        entitlement.SlaProcessId = slaProcess.Id;
        entitlement.AccountId = account.Id;
        entitlement.StartDate = Date.today();
        entitlement.Name = account.Name + ' ' + slaProcess.Name;
        return entitlement;
	}

	/**
	 *  @description Filter Accounts that need to be associated to an entitlement
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2015-10-08
	 */
    private List<Account> filterEntitlementAccounts(List<Account> unfilteredAccounts) {
		List<Account> filteredAccounts = new List<Account>();
		for (Account accountToFilter : unfilteredAccounts) {
			if (accountToFilter.RecordTypeId == customerServiceRecordTypeId) {
				filteredAccounts.add(accountToFilter);
			}
		}
		return filteredAccounts;
    }

	/**
	 *  @description Filter Accounts that need to be associated to an entitlement
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2015-10-08
	 */
    private List<Case> filterEntitlementCases(List<Case> unfilteredCases) {
		List<Case> filteredCases = new List<Case>();
		for(Case c : unfilteredCases) {
			if(c.EntitlementId == null && c.AccountId != null) {
				filteredCases.add(c);
			}
		}
		return filteredCases;
    }

	/**
	 *  @description Return all Sla Processes by Name
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2015-10-13
	 */
	 private SlaProcess findDefaultSlaProcess() {
		List<SlaProcess> slaProcesses = [SELECT Id, Name FROM SlaProcess WHERE Name =: ENTITLEMENT_NAME AND IsActive = true];
		return (slaProcesses.size() != 0) ? slaProcesses[0] : null;
    }

	/**
	 *  @description Return all Sla Processes by Name
	 *  @author 	 Graham Barnard, Traction on Demand.
	 *  @date        2015-10-13
	 */
	 private List<Entitlement> findAllEntitlementsByAccountIds(Set<Id> accountIds) {
		List<Entitlement> entitlements = [SELECT Id, AccountId, Name, StartDate, SlaProcessId FROM Entitlement WHERE AccountId IN: accountIds];
		return entitlements;
    }
}